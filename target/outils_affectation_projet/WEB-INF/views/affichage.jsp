<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="com.google.gson.Gson"%>
<%@ page import="com.google.gson.JsonObject"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>


<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Affichage</title>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<link rel="stylesheet" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="webjars/jquery/3.1.1/jquery.min.js"></script>
<script src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
<script type="text/javascript" src="webjars/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="webjars/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" type="text/css"/>
		<link rel="stylesheet" href="resources/style.css">
<script type="text/javascript">
window.onload = function() { 
	
var chart = new CanvasJS.Chart("chartContainer", {
	
	animationEnabled: true,
	theme: "light2", // "light1", "dark1", "dark2"
	title: {
		text: "Affectation des étudiants aux sujets"
	},
	subtitles: [{
		text: "${nomEnseignement}",
		fontSize: 16
	},
	{
		text: "${solution}, pire affectation= ${pireAffectation}",
		fontSize: 16
	}
	],
	axisY: {
		scaleBreaks: {
			type: "wavy",
			autoCalculate: true
		}
	},
	data: [{
		type: "column",
		indexLabel: "{y}",
		dataPoints: ${dataPoints}
	}]
});
chart.render();
 
 
var chart2 = new CanvasJS.Chart("chartContainer2", {
	
	animationEnabled: true,
	theme: "light2", // "light1", "dark1", "dark2"
	title: {
		text: "Affectation des sujets aux encadrants"
	},
	subtitles: [{
		text: "${nomEnseignement}",
		fontSize: 16
	},
	{
		text: "${solution}, pire affectation= ${pireAffectation}",
		fontSize: 16
	}
	],
	axisY: {
		scaleBreaks: {
			type: "wavy",
			autoCalculate: true
		}
	},
	data: [{
		type: "column",
		indexLabel: "{y}",
		dataPoints: ${dataPoints2}
	}]
});
chart2.render();
}

$(document).ready(function(){
	$(".icon-input-btn").each(function(){
        var btnFont = $(this).find(".btn").css("font-size");
        var btnColor = $(this).find(".btn").css("color");
		$(this).find(".glyphicon").css("font-size", btnFont);
        $(this).find(".glyphicon").css("color", btnColor);
        if($(this).find(".btn-xs").length){
            $(this).find(".glyphicon").css("top", "20%");
        }
	}); 
});
</script>
<style type="text/css">
    .valider{
    	margin-left: 550px;
    }
    .icon-input-btn{
        display: inline-block;
        position: relative;
    }
    .retour .icon-input-btn input[type="submit"]{
        padding-left: 2em;
    }
    .icon-input-btn .glyphicon{
        display: inline-block;
        position: absolute;
        left: 0.65em;
        top: 30%;
    }
</style>
</head>
<body>
    
	<form action="optimisation" method="get" onsubmit="return confirm('Voulez vous vraiment retourner à la phase optimisation');" name="form" id="form">
	<div class="retour" >
		
	    <span class="icon-input-btn" ><span class="glyphicon glyphicon-arrow-left" ></span> <input type="submit" class="btn btn-default btn-lg" name="Retour" value="Retour"  ></span>
	    
	</div>        
	<input type="hidden" name="idEnseignement" value="${idEnseignement}">
    </form>
<div id="chartContainer" style="height: 370px; width: 100%; margin-bottom:5%">

</div>

<div id="chartContainer2" style="height: 370px; width: 100%; margin-bottom:5%">

</div>
<c:if test="${ok eq true}"> 
	<form action="validerOptimisation" method="get" onsubmit="return confirm('Voulez vous vraiment valider cette optimisation');" name="form" id="form">
		<div class="valider" >
			
		    <span class="icon-input-btn" ><input type="submit" class="btn btn-default btn-lg" name="valider" value="valider"  ></span>
		    
		</div>        
		<input type="hidden" name="idEnseignement" value="${idEnseignement}">
		<input type="hidden" name="listSolution" value="${listSolution}">
	</form>
</c:if>
</body>
</html>                              