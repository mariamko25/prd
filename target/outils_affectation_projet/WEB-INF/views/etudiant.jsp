<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
	    <title>Etudiant</title>
	    <link rel="stylesheet" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css">
	    <script src="webjars/jquery/3.1.1/jquery.min.js"></script>
		<script src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
		<script type="text/javascript" src="webjars/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
		<link rel="stylesheet" href="webjars/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" type="text/css"/>
		<link rel="stylesheet" href="resources/style.css">
		   <script type="text/javascript">
				function JSalert(){
				var alertstr = "Opération réussie";
					    alert(alertstr);
				 
				}
				
				function JSalertFail(){
					 
					var alertstr = "Désolé(e) l'opération a échoué";
				    alert(alertstr);
					 
					}
			</script>
	</head>
<body>
<c:if test="${success}"><script> JSalert();</script></c:if>
	<c:if test="${failure}"><script> JSalertFail();</script></c:if>
	<!--       Barre de navigation         -->
			<nav class="navbar navbar-default">
				  <div class="container-fluid">			    
					    <ul class="nav navbar-nav">
						      <li><a href="index">ACCUEIL</a></li>
							      
							      <li class="active" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">PROJET<span class="caret"></span></a>
								        <ul id="drop" class="dropdown-menu" >
								          <li ><a href="enseignant">ENSEIGNANT</a></li>
								          <li><a style="background-color:#00a99b !important"href="etudiant">ETUDIANT</a></li>
								        </ul>
							      </li>
						      
						      <li><a href="administrateur">ADMINISTRATEUR</a></li>
					    </ul>
				  </div>
			</nav>
	<!--      Tableau de recherche         -->
		  <div class="container" style="margin-top: 8%;">
					            
			        <div class="row">
			        <div class="col-md-8 col-md-offset-2">   
					<form action="cleEnseignement" method="get"  id="form-id" >
		            <table class="table table-list-search"  style="margin-bottom: 56%">
		                    <thead>
		                        <tr style="color:white; background-color:#00a99b">
		                            <th>Enseignement</th>
		                            <th>Année</th>
		                            <th>Intitulé</th>
		                            <th>Ouvrir</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    
		                    <c:forEach items="${enseignements}" var="current" varStatus="loop">
										<c:if test="${loop.index mod 2 eq 0 }">
									        
										        <tr>
													
														<td> ${current.nom} </td>
														<td> ${current.annee} </td>
														<td> ${current.description} </td>
														<td><input type="submit" id="ouvrir" name="ouvrir" value="ouvrir"></td>
														
												</tr> 
											
									    </c:if>
									    <c:if test="${loop.index mod 2 ne 0 }">
									        <tr style="background-color:#e6fff9">
													<td> ${current.nom} </td>
													<td> ${current.annee} </td>
													<td> ${current.description} </td>
													<td><input type="submit" id="ouvrir" name="ouvrir" value="ouvrir"></td>
																
											</tr>  
									    </c:if>
										<input type="hidden" name="idEnseignement" value="${current.idEnseignement}"> 
										
							</c:forEach>
		                    </tbody>
		                </table>  
		                </form>
		                </div> 
				</div>
				</div>
	</body>
	<div class="row">
		<div class="col-md-12">            
			<footer>
				<img src="resources/footer.png" class="img-fluid" alt="Responsive image" style="width:100%; height:auto">		
			</footer>
		</div>
	</div>
	
</html>