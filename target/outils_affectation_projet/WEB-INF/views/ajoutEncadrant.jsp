<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
	    <title>Ajout Encadrant</title>
	    <link rel="stylesheet" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css">
	    <script src="webjars/jquery/3.1.1/jquery.min.js"></script>
		<script src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
		<script type="text/javascript" src="webjars/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
		<link rel="stylesheet" href="webjars/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" type="text/css"/>
		<link rel="stylesheet" href="resources/style.css">
		   
	</head>
<body>
<!--       Barre de navigation         -->
			<nav class="navbar navbar-default">
				  <div class="container-fluid">			    
					    <ul class="nav navbar-nav">
						      <li><a href="index">ACCUEIL</a></li>
							      
							      <li  class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">PROJET<span class="caret"></span></a>
								        <ul id="drop" class="dropdown-menu" >
								          <li ><a  style="background-color:#00a99b !important" href="enseignant">ENSEIGNANT</a></li>
								          <li><a href="etudiant">ETUDIANT</a></li>
								        </ul>
							      </li>
						      
						      <li class="active"><a href="administrateur">ADMINISTRATEUR</a></li>
					    </ul>
				  </div>
			</nav>
			<div class="row">
					<div class="col-md-8 col-md-offset-3">
					<!--       Barre de navigation         -->
					<nav class="navbar navbar-default" id="navigator" style="background-color:white;border-color:white">
						  <div class="container-fluid" >			    
							    <ul  class="nav navbar-nav"   style="padding-left:6%">
								      <li ><a href="administrateur" >Liste Enseignements</a></li>								      
									  <li><a href="ajoutEnseignement">Ajout Enseignement</a></li>
									  <li><a href="gestionTables">Gestion Tables</a></li>
							    </ul>
						  </div>
					</nav>
					</div>
				</div>
		<!--      Tableau de recherche         -->
		  <div class="container" style="margin-top: 6%;">
		 	 <div id="signupbox" style=" margin-bottom:14%" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2" >
        		<div class="panel panel-info">
            <div class="panel-heading" style="background-color:#00a99b">
                <div class="panel-title" style="color:black">Ajouter Encadrant</div>
            </div>  
            <div class="panel-body" >
			  <form action="ajoutEncadrant" method="get" onsubmit="return confirm('Voulez vous vraiment ajouter cet encadrant?');" name="form" id="form">
				  
	              
	              <div class="row" style="padding-left:2.8%">
	              <div id="enseignant" class="form-group required" >
                       <label for="enseignant" class="control-label col-md-4  requiredField" style="margin-bottom:5%">Enseignant<span class="asteriskField">*</span> </label>
                       <div class="controls col-md-8 " >
                           <select name="enseignant" form="form" style="width:95%;" >
  							  <c:forEach items="${enseignants}" var="current">
									<option>${current.nom}-${current.idEnseignant}</option>
							</c:forEach>
							</select>
                       </div>
	              </div> 
	              </div>
	              
	              
	              
	             
	             
	              <div class="row" style="padding-left:2.8%">
	              <div id="sujet" class="form-group required" >
                       <label for="sujet" class="control-label col-md-4  requiredField" style="margin-bottom:5%">Sujet<span class="asteriskField">*</span> </label>
                       <div class="controls col-md-8 " >
                           <select name="sujet" form="form" style="width:95%;" >
  							  <c:forEach items="${sujets}" var="current">
									<option>${current.intitule}-${current.idSujet}</option>
							</c:forEach>
							</select>
                       </div>
	              </div>  
	              
	              </div>
	              <div class="form-group">
	              <div class="controls col-md-8 col-md-offset-4" style="margin-top: 2%">
            	              <input type="submit" name="enregistrer" id="enregistrer" value="enregistrer"/>           	                     	                            
	              </div>
	              </div>
		      </form>
		      </div>
		      </div>
		 </div> 
		 </div>
				
</body>
<div class="row">
		<div class="col-md-12">            
			<footer>
				<img src="resources/footer.png" class="img-fluid" alt="Responsive image" style="width:100%; height:auto">		
			</footer>
		</div>
	</div>
</html>