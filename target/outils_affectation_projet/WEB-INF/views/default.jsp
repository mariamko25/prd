<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
    <title>Default</title>
      <link rel="stylesheet" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css">
  

</head>
<body>


<nav class="navbar navbar-default">
  <div class="container-fluid">
    
    <ul class="nav navbar-nav">
      <li class="active"><a href="index">Accueil</a></li>
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Projet<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Enseignant</a></li>
          <li><a href="#">Etudiant</a></li>
        </ul>
      </li>
      <li><a href="#">Administrateur</a></li>
    </ul>
  </div>
</nav>
  
<script src="webjars/jquery/3.1.1/jquery.min.js"></script>
<script src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
</body>
</html>





