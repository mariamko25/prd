<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
	    <title>Liste sujets</title>
	    <link rel="stylesheet" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css">
	    <script src="webjars/jquery/3.1.1/jquery.min.js"></script>
		<script src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
		<script type="text/javascript" src="webjars/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
		<link rel="stylesheet" href="webjars/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" type="text/css"/>
		<link rel="stylesheet" href="resources/style.css">
		   <script type="text/javascript">
			function JSalert(){
			var alertstr = "Opération réussie";
				    alert(alertstr);
			 
			}
			
			function JSalertFail(){
				 
				var alertstr = "Désolé(e) l'opération a échoué";
			    alert(alertstr);
				 
				}
		</script>
	</head>
<body>
<c:if test="${success}"><script> JSalert();</script></c:if>
<c:if test="${failure}"><script> JSalertFail();</script></c:if>
<!--       Barre de navigation         -->
			<nav class="navbar navbar-default">
				  <div class="container-fluid">			    
					    <ul class="nav navbar-nav">
						      <li><a href="index">ACCUEIL</a></li>
							      
							      <li class="active" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">PROJET<span class="caret"></span></a>
								        <ul id="drop" class="dropdown-menu" >
								          <li ><a  style="background-color:#00a99b !important" href="enseignant">ENSEIGNANT</a></li>
								          <li><a href="etudiant">ETUDIANT</a></li>
								        </ul>
							      </li>
						      
						      <li><a href="administrateur">ADMINISTRATEUR</a></li>
					    </ul>
				  </div>
			</nav>
			
			
				<div class="row">
					<div class="col-md-4 col-md-offset-4">
					<!--       Barre de navigation         -->
					<nav class="navbar navbar-default" id="navigator" style="background-color:white;border-color:white">
						  <div class="container-fluid" >			    
							    <ul  class="nav navbar-nav"   >
								      <li ><a href="enseignementSujets" >Liste Sujets</a></li>
								      <c:if test="${formulaire ne true}"> 
									      <li><a href="ajoutSujet">Ajout Sujet</a></li>
									      <c:if test="${showMenu}"> 
									      	<li><a href="souhaits">Liste Souhaits</a></li>
									      </c:if>
								       </c:if>
							    </ul>
						  </div>
					</nav>
					</div>
				</div>
			
		<!--      Tableau de recherche         -->
		  <div class="container" style="margin-top: 4%;">
		  <c:if test="${showMenu}"> 
		     <div class="row" style="margin-bottom: 2%;">
			     	<div class="col-md-4 col-md-offset-2">
			     		<form action="activerFormulaire" method="get" >
			     			<c:if test="${formulaire eq true}"> 
			     				<input type="submit" name="desactiver" value="desactiver formulaire">
			     			</c:if>
			     			<c:if test="${formulaire ne true}"> 
			     				<input type="submit" name="activer" value="activer formulaire">
			     			</c:if>
			     			<input type="hidden" name="idEnseignement" value="${idEnseignement}"> 
							<input type="hidden" name="idEnseignant" value="${idEnseignant}"> 
			     		</form>
			     	</div>
			     	
			     	<div class="col-md-4 col-md-offset-2">
			     		<form action="modifierEnseignement" method="get" >
			     			
			     			<c:if test="${formulaire ne true}"> 
			     				<input type="submit" name="activer" value="ModifierEnseignement">
			     			</c:if>
			     			<input type="hidden" name="idEnseignement" value="${idEnseignement}"> 
							<input type="hidden" name="idEnseignant" value="${idEnseignant}"> 
			     		</form>
			     	</div>
			     	
		      </div>
		      
		       </c:if>
			  <div class="row">
				        <div class="col-md-8 col-md-offset-2">   
						
			            <table class="table table-list-search"  style="margin-bottom: 46%">
			                    <thead>
			                        <tr style="color:white; background-color:#00a99b">
			                            <th>Intitulé</th>
			                            <th>Description</th>
			                            <th>Action</th>
			                        </tr>
			                    </thead>
			                    <tbody>
		                    
		                    <c:forEach items="${sujets}" var="current" varStatus="loop">
										<c:if test="${loop.index mod 2 eq 0 }">
									        <form action="modifierSujet" method="get">
										        <tr>
													
														<td> ${current.intitule} </td>
														<td> ${current.description} </td>
														<td> <input type="submit" name="ouvrir" value="ouvrir"> </td>
												</tr> 
											<input type="hidden" name="idSujet" value="${current.idSujet}"> 
											<input type="hidden" name="idEnseignement" value="${idEnseignement}"> 
											<input type="hidden" name="idEnseignant" value="${idEnseignant}"> 
	
											</form>
									    </c:if>
									    <c:if test="${loop.index mod 2 ne 0 }">
									     <form action="modifierSujet" method="get" >
										        
										        <tr style="background-color:#e6fff9">
													
														<td> ${current.intitule} </td>
														<td> ${current.description} </td>
														<td> <input type="submit" name="ouvrir" value="ouvrir"> </td>
												</tr> 
											<input type="hidden" name="idSujet" value="${current.idSujet}"> 
											<input type="hidden" name="idEnseignement" value="${idEnseignement}"> 
											<input type="hidden" name="idEnseignant" value="${idEnseignant}"> 
	
											</form>
									       
									    </c:if>
									
							</c:forEach>
		                    </tbody>
		                </table>  
		                </div> 
				</div>
				</div>
</body>
<div class="row">
		<div class="col-md-12">            
			<footer>
				<img src="resources/footer.png" class="img-fluid" alt="Responsive image" style="width:100%; height:auto">		
			</footer>
		</div>
	</div>
</html>