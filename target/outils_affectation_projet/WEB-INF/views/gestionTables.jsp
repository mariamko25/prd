<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Gestion Tables</title>
<link rel="stylesheet"href="webjars/bootstrap/3.3.7/css/bootstrap.min.css"/>
<script src="webjars/jquery/3.1.1/jquery.min.js"></script>
<script src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="webjars/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
<script type="text/javascript" src="webjars/bootstrap-sweetalert/0.4.3/lib/sweet-alert.js"></script>
<link rel="stylesheet" href="webjars/bootstrap-sweetalert/0.4.3/lib/sweet-alert.css" type="text/css" />
<link rel="stylesheet" href="webjars/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" type="text/css" />
<link rel="stylesheet" href="resources/style.css"/>
<script type="text/javascript">
	function JSalert(){
	var alertstr = "Opération réussie";
		    alert(alertstr);
	 
	}
	
	function JSalertFail(){
		 
		var alertstr = "Désolé(e) l'opération a échoué";
	    alert(alertstr);
		 
		}
</script>
</head>
<body>
	<c:if test="${success}"><script> JSalert();</script></c:if>
	<c:if test="${failure}"><script> JSalertFail();</script></c:if>
	<!--       Barre de navigation         -->
	<nav class="navbar navbar-default">
	<div class="container-fluid">
		<ul class="nav navbar-nav">
			<li><a href="index">ACCUEIL</a></li>

			<li class="dropdown"><a class="dropdown-toggle"
				data-toggle="dropdown" href="#">PROJET<span class="caret"></span></a>
				<ul id="drop" class="dropdown-menu">
					<li><a href="enseignant">ENSEIGNANT</a></li>
					<li><a href="etudiant">ETUDIANT</a></li>
				</ul></li>

			<li class="active"><a href="administrateur">ADMINISTRATEUR</a></li>
		</ul>
	</div>
	</nav>


	<div class="row">
		<div class="col-md-8 col-md-offset-3">
			<!--       Barre de navigation         -->
			<nav class="navbar navbar-default" id="navigator"
				style="background-color:white;border-color:white">
			<div class="container-fluid">
				<ul class="nav navbar-nav" style="padding-left: 6%">
					<li><a href="administrateur">Liste Enseignements</a></li>
					<li><a href="ajoutEnseignement">Ajout Enseignement</a></li>
					<li><a href="gestionTables">Gestion Tables</a></li>
				</ul>
			</div>
			</nav>
		</div>
	</div>

	<!--      Tableau de recherche         -->
	<div class="container" style="margin-top: 4%;">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form action="gestionTablesAction" method="get">
					<table class="table table-list-search" style="margin-bottom: 46%">
						<thead>
							<tr style="color: white; background-color: #00a99b">
								<th>Table</th>
								<th>Ajouter</th>
								<th>Vider</th>
							</tr>
						</thead>
						<tbody>
							<tr>

								<td>Affectation</td>								
								<td><input type="submit" name="ouvrir" value="Ajouter Affectation"></td>
								<td><input type="submit" name="vider" value="Vider Affectation"></td>
								
							</tr>
							<tr>

								<td>Encadrant</td>								
								<td><input type="submit" name="ouvrir" value="Ajouter Encadrant"></td>
								<td><input type="submit" name="vider" value="Vider Encadrant"></td>
								
							</tr>
							<tr>

								<td>Enseignant</td>								
								<td><input type="submit" name="ouvrir" value="Ajouter Enseignant"></td>
								<td><input type="submit" name="vider" value="Vider Enseignant"></td>
								
							</tr>
							<tr>

								<td>Enseignement</td>								
								<td><input type="submit" name="ouvrir" value="Ajouter Enseignement"></td>
								<td><input type="submit" name="vider" value="Vider Enseignement"></td>
								
							</tr>
							<tr>

								<td>Etudiant</td>								
								<td><input type="submit" name="ouvrir" value="Ajouter Etudiant"></td>
								<td><input type="submit" name="vider" value="Vider Etudiant"></td>
								
							</tr>
							<tr>

								<td>Groupe</td>								
								<td><input type="submit" name="ouvrir" value="Ajouter Groupe"></td>
								<td><input type="submit" name="vider" value="Vider Groupe"></td>
								
							</tr>
							<tr>

								<td>MembreDuGroupe</td>								
								<td><input type="submit" name="ouvrir" value="Ajouter MembreDuGroupe"></td>
								<td><input type="submit" name="vider" value="Vider MembreDuGroupe"></td>
								
							</tr>
							<tr>

								<td>Souhait</td>								
								<td><input type="submit" name="ouvrir" value="Ajouter Souhait"></td>
								<td><input type="submit" name="vider" value="Vider Souhait"></td>
								
							</tr>
							<tr>

								<td>Sujet</td>								
								<td><input type="submit" name="ouvrir" value="Ajouter Sujet"></td>
								<td><input type="submit" name="vider" value="Vider Sujet"></td>
								
							</tr>
						</tbody>
					</table>
				</form>
			</div>
		</div>
	</div>
</body>
<div class="row">
	<div class="col-md-12">
		<footer> <img src="resources/footer.png" class="img-fluid"
			alt="Responsive image" style="width: 100%; height: auto"> </footer>
	</div>
</div>
</html>