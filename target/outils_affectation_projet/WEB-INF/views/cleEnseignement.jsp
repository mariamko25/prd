<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
	    <title>Clé Enseignement</title>
	    <link rel="stylesheet" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css">
	    <script src="webjars/jquery/3.1.1/jquery.min.js"></script>
		<script src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
		<script type="text/javascript" src="webjars/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
		<link rel="stylesheet" href="webjars/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" type="text/css"/>
		<link rel="stylesheet" href="resources/style.css">
		   <script type="text/javascript">
				function JSalert(){
				var alertstr = "Opération réussie";
					    alert(alertstr);
				 
				}
				
				function JSalertFail(){
					 
					var alertstr = "Désolé(e) l'opération a échoué";
				    alert(alertstr);
					 
					}
			</script>
	</head>
<body>
	<c:if test="${success}"><script> JSalert();</script></c:if>
	<c:if test="${failure}"><script> JSalertFail();</script></c:if>
	<!--       Barre de navigation         -->
			<nav class="navbar navbar-default">
				  <div class="container-fluid">			    
					    <ul class="nav navbar-nav">
						      <li><a href="index">ACCUEIL</a></li>
							      
							      <li class="active" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">PROJET<span class="caret"></span></a>
								        <ul id="drop" class="dropdown-menu" >
								          <li ><a href="enseignant">ENSEIGNANT</a></li>
								          <li><a style="background-color:#00a99b !important"href="etudiant">ETUDIANT</a></li>
								        </ul>
							      </li>
						      
						      <li><a href="administrateur">ADMINISTRATEUR</a></li>
					    </ul>
				  </div>
			</nav>
	<!--      Tableau de recherche         -->
		  <div class="container" style="margin-top: 10%; margin-bottom:28%">
					            
			        <div class="row">
				        <div class="col-md-8 col-md-offset-2">   
							<form action="etudiantEnseignement" method="post"  id="form-id" >
		                        <div id="cle" class="form-group required">
				                       <label for="cle" class="control-label col-md-4  requiredField"> Veuillez saisir une clé pour cet enseignement<span class="asteriskField">*</span> </label>
				                       <div class="controls col-md-8 ">
				                           <input class="input-md  textinput textInput form-control" id="cle"  name="cle" " style="margin-bottom: 10px" type="text" />
				                       </div>
					              </div>  
      							<input type="hidden" name="idEnseignement" value="${idEnseignement}"> 
				           		<div class="controls col-md-8 col-md-offset-4" style="margin-top: 2%">
            	              			<input type="submit" name="valider" id="valider" value="valider"/>           	              
	              				</div>
				            </form>
			            </div> 
					</div>
				</div>
	</body>
	<div class="row">
		<div class="col-md-12">            
			<footer>
				<img src="resources/footer.png" class="img-fluid" alt="Responsive image" style="width:100%; height:auto">		
			</footer>
		</div>
	</div>
	
</html>