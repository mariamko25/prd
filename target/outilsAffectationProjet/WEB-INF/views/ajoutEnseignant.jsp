<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
	    <title>Ajout Enseignant</title>
	    <link rel="stylesheet" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css">
	    <script src="webjars/jquery/3.1.1/jquery.min.js"></script>
		<script src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
		<script type="text/javascript" src="webjars/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
		<script type="text/javascript" src="webjars/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
	    <link rel="stylesheet" href="webjars/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker3.css" type="text/css"/>
		
		<link rel="stylesheet" href="webjars/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" type="text/css"/>
		<link rel="stylesheet" href="resources/style.css">
		   <script>
		    $(document).ready(function(){
		        var date_input=$('input[name="dateNaissance"]'); //our date input has the name "date"
		        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		        date_input.datepicker({
		            format: 'dd/mm/yyyy',
		            container: container,
		            todayHighlight: true,
		            autoclose: true,
		        })
		    })
		</script>
	</head>
<body>
<!--       Barre de navigation         -->
			<nav class="navbar navbar-default">
				  <div class="container-fluid">			    
					    <ul class="nav navbar-nav">
						      <li><a href="index">ACCUEIL</a></li>
							      
							      <li  class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">PROJET<span class="caret"></span></a>
								        <ul id="drop" class="dropdown-menu" >
								          <li ><a  style="background-color:#00a99b !important" href="authentificationEnseignant">ENSEIGNANT</a></li>
								          <li><a href="authentificationEtudiant">ETUDIANT</a></li>
								        </ul>
							      </li>
						      
						      <li class="active"><a href="authentificationAdministrateur">ADMINISTRATEUR</a></li>
					    </ul>
				  </div>
			</nav>
			<div class="row">
					<div class="col-md-8 col-md-offset-3">
					<!--       Barre de navigation         -->
					<nav class="navbar navbar-default" id="navigator" style="background-color:white;border-color:white">
						  <div class="container-fluid" >			    
							    <ul  class="nav navbar-nav"   style="padding-left:6%">
								      <li ><a href="administrateur" >Liste Enseignements</a></li>								      
									  <li><a href="AjoutEnseignements">Ajout Enseignement</a></li>
									  <li><a href="gestionTables">Gestion Tables</a></li>
							    </ul>
						  </div>
					</nav>
					</div>
				</div>
		<!--      Tableau de recherche         -->
		  <div class="container" style="margin-top: 6%;">
		 	 <div id="signupbox" style=" margin-bottom:5%" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2" >
        		<div class="panel panel-info">
            <div class="panel-heading" style="background-color:#00a99b">
                <div class="panel-title" style="color:black">Ajouter Enseignant</div>
            </div>  
            <div class="panel-body" >
			  <form action="ajoutEnseignant" method="get" onsubmit="return confirm('Voulez vous vraiment ajouter cet enseignant?');" name="form" id="form">
				  <div id="nom" class="form-group required">
                       <label for="nom" class="control-label col-md-4  requiredField"> Nom<span class="asteriskField">*</span> </label>
                       <div class="controls col-md-8 ">
                           <input class="input-md  textinput textInput form-control" id="nom"  name="nom"  value="" style="margin-bottom: 10px" type="text" />
                       </div>
	              </div>  
	              <div id="prenom" class="form-group required">
                       <label for="prenom" class="control-label col-md-4  requiredField"> Prenom<span class="asteriskField">*</span> </label>
                       <div class="controls col-md-8 ">
                           <input class="input-md  textinput textInput form-control" id="prenom"  name="prenom" value="" style="margin-bottom: 10px" type="text" />
                       </div>
	              </div>  
	             <div id="dateNaissance" class="form-group required">
                       <label for="dateNaissance" class="control-label col-md-4  requiredField"> Date Naissance<span class="asteriskField">*</span> </label>
                       <div class="controls col-md-8 ">
                           <input class="input-md  textinput textInput form-control" id="dateNaissance"  name="dateNaissance" placeholder="DD/MM/YYY" style="margin-bottom: 10px" type="text" />
                       </div>
	              </div> 
	             
	             
	              <div id="email" class="form-group required">
                       <label for="email" class="control-label col-md-4  requiredField"> Adresse mail<span class="asteriskField">*</span> </label>
                       <div class="controls col-md-8 ">
                           <input class="input-md  textinput textInput form-control" id="email"  name="email" value="" style="margin-bottom: 10px" type="email" placeholder="prenom.nom@univ-tour.fr"/>
                       </div>
	              </div>  
	              <div class="row" style="padding-left:2.8%">
	             
	              </div>
	              <div class="form-group">
	              <div class="controls col-md-8 col-md-offset-4" style="margin-top: 2%">
            	              <input type="submit" name="enregistrer" id="enregistrer" value="enregistrer"/>           	                     	                            
	              </div>
	              </div>
		      </form>
		      </div>
		      </div>
		 </div> 
		 </div>
				
</body>
<div class="row">
		<div class="col-md-12">            
			<footer>
				<img src="resources/footer.png" class="img-fluid" alt="Responsive image" style="width:100%; height:auto">		
			</footer>
		</div>
	</div>
</html>