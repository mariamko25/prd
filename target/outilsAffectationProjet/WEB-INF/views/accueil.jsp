<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
	    <title>Accueil</title>
	    <link rel="stylesheet" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css">
	    <script src="webjars/jquery/3.1.1/jquery.min.js"></script>
		<script src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
		<script type="text/javascript" src="webjars/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
		<link rel="stylesheet" href="webjars/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" type="text/css"/>
		<link rel="stylesheet" href="resources/style.css">		   
	</head>

	<body>
		
	    
		<!--       Barre de navigation         -->
		<nav class="navbar navbar-default">
			  <div class="container-fluid">			    
				    <ul class="nav navbar-nav">
					      <li class="active"><a href="index">ACCUEIL</a></li>
						      
						      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">PROJET<span class="caret"></span></a>
							        <ul id="drop" class="dropdown-menu">
							          <li><a href="authentificationEnseignant">ENSEIGNANT</a></li>
							          <li><a href="authentificationEtudiant">ETUDIANT</a></li>
							        </ul>
						      </li>
					      
					      <li><a href="authentificationAdministrateur">ADMINISTRATEUR</a></li>
				    </ul>
			  </div>
		</nav>
		
		<!--      Tableau de recherche         -->
		  <div class="container" style="margin-top: 8%;">
			
			            <form action="index" method="get" >
			            <div class="row" >
		        				<div class="col-md-8 col-md-offset-2">
					                <div class="input-group">
					                    <!-- USE TWITTER TYPEAHEAD JSON WITH API TO SEARCH -->
					                    <input class="form-control" id="system-search" name="research" placeholder="Thématique sujet"  value=" <c:out value="${param.research}" escapeXml="true"/> ">
					                    
										    
					                    <span class="input-group-btn" id="search-button">
					                        <button type="submit" id="searchDessin" class="btn btn-default"><i class="glyphicon glyphicon-search" ></i></button>
					                    </span>
					                    
					                </div>
			                		</div>
			                	</div>
			                <br>
			                	<div class="row" style="margin-bottom: 10%;">
			                	
					                	<div class="col-md-4 col-md-offset-2">
					                		<select class="form-control" name="enseignement" id="enseignement" >
					                			<option selected="selected">enseignement</option>
					                			
						                		<c:forEach items="${enseignements}" var="current">
											   <option>${current.nom}</option>
											</c:forEach>
								      	</select>
					                		
					                	</div>
					                		<div class="col-md-4 ">
						                		<select class="form-control" name="annee" id="annee" >
						                				<option selected="selected">annee</option>
										        		
										        		<c:forEach var="i" begin="${0}" end="${50}" varStatus="loop">
											        		<c:set var="decr" value="${currentYear-i}"/>
														   <option>${decr}</option>
													</c:forEach>
										     </select>			                		
					                		</div>
				                	</div>
			                		
			            </form>
			            
			            
			        <div class="row">
			        <div class="col-md-8 col-md-offset-2">    
		            <table class="table table-list-search" style="margin-bottom: 26.5%">
		                    <thead>
		                        <tr style="color:white; background-color:#00a99b">
		                            <th>Enseignement</th>
		                            <th>Intitulé</th>
		                            <th>Année</th>
		                            <th>Encadrant</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    
		                    <c:forEach items="${sujets}" var="current" varStatus="loop">
										<c:if test="${loop.index mod 2 eq 0 }">
									        
									        <tr>
												
													<td> ${current.nomEnseignement} </td>
													<td> ${current.intitule} </td>
													<td> ${current.annee} </td>
													<td> ${current.nomEnseignant} </td>
											</tr> 
											 
									    </c:if>
									    <c:if test="${loop.index mod 2 ne 0 }">
									        <tr style="background-color:#e6fff9">
													<td> ${current.nomEnseignement} </td>
													<td> ${current.intitule} </td>
													<td> ${current.annee} </td>
													<td> ${current.nomEnseignant} </td>
											</tr>   
									    </c:if>
										
							</c:forEach>
		                    </tbody>
		                </table>  
		                </div> 
				</div>
				</div>
	
	
	
	</body>

	<div class="row" style="margin-top:0.5%">
		<div class="col-md-12">            
			<footer>
				<img src="resources/footer.png" class="img-fluid" alt="Responsive image" style="width:100%; height:auto">		
			</footer>
		</div>
	</div>
</html>
