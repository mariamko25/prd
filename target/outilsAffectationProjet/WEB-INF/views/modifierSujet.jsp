<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
	    <title>Modifier Sujet</title>
	    <link rel="stylesheet" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css">
	    <script src="webjars/jquery/3.1.1/jquery.min.js"></script>
		<script src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
		<script type="text/javascript" src="webjars/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
		<link rel="stylesheet" href="webjars/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" type="text/css"/>
		<link rel="stylesheet" href="resources/style.css">
		   
	</head>
<body>
<!--       Barre de navigation         -->
			<nav class="navbar navbar-default">
				  <div class="container-fluid">			    
					    <ul class="nav navbar-nav">
						      <li><a href="index">ACCUEIL</a></li>
							      
							      <li class="active" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">PROJET<span class="caret"></span></a>
								        <ul id="drop" class="dropdown-menu" >
								          <li ><a  style="background-color:#00a99b !important" href="authentificationEnseignant">ENSEIGNANT</a></li>
								          <li><a href="authentificationEtudiant">ETUDIANT</a></li>
								        </ul>
							      </li>
						      
						      <li><a href="authentificationAdministrateur">ADMINISTRATEUR</a></li>
					    </ul>
				  </div>
			</nav>
			
		<!--      Tableau de recherche         -->
		  <div class="container" style="margin-top: 6%;">
		 	 <div id="signupbox" style=" margin-bottom:18%" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2" >
        		<div class="panel panel-info">
            <div class="panel-heading" style="background-color:#00a99b">
                <div class="panel-title" style="color:black">Modifier Sujet</div>
            </div>  
            <div class="panel-body" >
			  <form action="modifierSujet" method="get" onsubmit="return confirm('Voulez vous vraiment modifier/supprimer ce sujet?');">
				  <div id="intitule" class="form-group required">
                       <label for="intitule" class="control-label col-md-4  requiredField"> Intitulé<span class="asteriskField">*</span> </label>
                       <div class="controls col-md-8 ">
                           <input class="input-md  textinput textInput form-control" id="intitule"  name="intitule" value="${sujet.intitule}" style="margin-bottom: 10px" type="text" />
                       </div>
	              </div>  
	              <div id="description" class="form-group required">
                       <label for="description" class="control-label col-md-4  requiredField"> Description<span class="asteriskField">*</span> </label>
                       <div class="controls col-md-8 ">
                           <input class="input-md  textinput textInput form-control" id="description"  name="description" value="${sujet.description}" style="margin-bottom: 10px" type="text" />
                       </div>
	              </div>  
	              <div id="nbCopie" class="form-group required">
                       <label for="nbCopie" class="control-label col-md-4  requiredField"> Nombre de copies<span class="asteriskField">*</span> </label>
                       <div class="controls col-md-8 ">
                           <input class="input-md   form-control"  name="nbCopie" id="nbCopie" type="number"  value="${sujet.nbCopie}" min="0" max="100"/>
                       </div>
	              </div>  
	              <div class="form-group">
	              <div class="controls col-md-8 col-md-offset-4" style="margin-top: 2%">
	              	              <input type="submit" name="enregistrer" id="enregistrer" value="enregistrer"/>
	              	              <c:if test="${formulaire ne true}">
	              	             	 <input type="submit" name="supprimer" id="supprimer" value="supprimer"/>
	              	              </c:if>
	              	              <input type="hidden" name="idSujet" id="idSujet" value="${sujet.idSujet}"/>
	              	              <input type="hidden" name="idEnseignement" id="idEnseignement" value="${sujet.idEnseignement}"/>
	              	              <input type="hidden" name="idEnseignant" id="idEnseignant" value="${idEnseignant}"/>
	              	              
	              </div>
	              </div>
		      </form>
		      </div>
		      </div>
		 </div> 
		 </div>
				
</body>
<div class="row">
		<div class="col-md-12">            
			<footer>
				<img src="resources/footer.png" class="img-fluid" alt="Responsive image" style="width:100%; height:auto">		
			</footer>
		</div>
	</div>
</html>