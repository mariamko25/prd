<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
	    <title>Modifier Enseignement</title>
	    <link rel="stylesheet" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css">
	    <script src="webjars/jquery/3.1.1/jquery.min.js"></script>
		<script src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
		<script type="text/javascript" src="webjars/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
		<link rel="stylesheet" href="webjars/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" type="text/css"/>
		<link rel="stylesheet" href="resources/style.css">
		   
	</head>
<body>
<!--       Barre de navigation         -->
			<nav class="navbar navbar-default">
				  <div class="container-fluid">			    
					    <ul class="nav navbar-nav">
						      <li><a href="index">ACCUEIL</a></li>
							      
							      <li  class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">PROJET<span class="caret"></span></a>
								        <ul id="drop" class="dropdown-menu" >
								          <li ><a  style="background-color:#00a99b !important" href="authentificationEnseignant">ENSEIGNANT</a></li>
								          <li><a href="authentificationEtudiant">ETUDIANT</a></li>
								        </ul>
							      </li>
						      
						      <li class="active"><a href="authentificationAdministrateur">ADMINISTRATEUR</a></li>
					    </ul>
				  </div>
			</nav>
			<div class="row">
					<div class="col-md-8 col-md-offset-3">
					<!--       Barre de navigation         -->
					<nav class="navbar navbar-default" id="navigator" style="background-color:white;border-color:white">
						  <div class="container-fluid" >			    
							    <ul  class="nav navbar-nav"   style="padding-left:6%">
								      <li ><a href="administrateur" >Liste Enseignements</a></li>								      
									  <li><a href="ajoutEnseignement">Ajout Enseignement</a></li>
									  <li><a href="gestionTables">Gestion Tables</a></li>
							    </ul>
						  </div>
					</nav>
					</div>
				</div>
		<!--      Tableau de recherche         -->
		  <div class="container" style="margin-top: 6%;">
		 	 <div id="signupbox" style=" margin-bottom:7%" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2" >
        		<div class="panel panel-info">
            <div class="panel-heading" style="background-color:#00a99b">
                <div class="panel-title" style="color:black">Modifier Enseignement</div>
            </div>  
            <div class="panel-body" >
			  <form action="adminModifierEnseignement" method="get" onsubmit="return confirm('Voulez vous vraiment modifier cet enseignement?');" name="form" id="form">
				  <div id="nom" class="form-group required">
                       <label for="nom" class="control-label col-md-4  requiredField"> Nom<span class="asteriskField">*</span> </label>
                       <div class="controls col-md-8 ">
                           <input class="input-md  textinput textInput form-control" id="nom"  name="nom" value="${enseignement.nom}" style="margin-bottom: 10px" type="text" />
                       </div>
	              </div>  
	              <div id="description" class="form-group required">
                       <label for="description" class="control-label col-md-4  requiredField"> Description<span class="asteriskField">*</span> </label>
                       <div class="controls col-md-8 ">
                           <input class="input-md  textinput textInput form-control" id="description"  name="description" value="${enseignement.description}" style="margin-bottom: 10px" type="text" />
                       </div>
	              </div>  
	              <div class="row" style="padding-left:2.8%">
	              <div id="typeOptimisation" class="form-group required" >
                       <label for="typeOptimisation" class="control-label col-md-4  requiredField" style="margin-bottom:5%">Type d'optimisation<span class="asteriskField">*</span> </label>
                       <div class="controls col-md-8 " >
                           <select name="typeOptimisation" form="form" style="width:95%;" >
                           	  <option selected="selected">${enseignement.typeOptimisation}</option>
							  <option value="ProjetSI">ProjetSI</option>
							  <option value="Options">Options</option>
							  <option value="Specialite">Specialite</option>
							</select>
                       </div>
	              </div>  
	              </div>
	             <div id="nbMaxScore" class="form-group required" >
	                   <label for="nbMaxScore" class="control-label col-md-4 requiredField" > NbMaxScore<span class="asteriskField">*</span> </label>
                       <div class="controls col-md-8 ">
                           <input class="input-md   form-control"  name="nbMaxScore" id="nbMaxScore" type="number"  value="${enseignement.nbMaxScore}" min="0" max="100"/>
                       </div>
	              </div>  
	               <div id="nbMaxTotal" class="form-group required" >
	                   <label for="nbMaxTotal" class="control-label col-md-4 requiredField" > NbMaxTotal<span class="asteriskField">*</span> </label>
                       <div class="controls col-md-8 ">
                           <input class="input-md   form-control"  name="nbMaxTotal" id="nbMaxTotal" type="number"  value="${enseignement.nbMaxTotal}" min="0" max="100"/>
                       </div>
	              </div> 
	              <div id="formulaire" class="form-group required">
                       <label for="formulaire" class="control-label col-md-4  requiredField"> Formulaire<span class="asteriskField">*</span> </label>
                       <div class="controls col-md-8 ">
                           <input class="input-md   form-control"  name="formulaire" id="formulaire" type="number"  value="${enseignement.formulaire}" min="0" max="1"/>
                       </div>
	              </div>  
	              <div id="cle" class="form-group required">
                       <label for="cle" class="control-label col-md-4  requiredField"> Clé<span class="asteriskField">*</span> </label>
                       <div class="controls col-md-8 ">
                           <input class="input-md  textinput textInput form-control" id="cle"  name="cle" value="${enseignement.cle}" style="margin-bottom: 10px" type="text" />
                       </div>
	              </div>  
	              <div class="form-group">
	              <div class="controls col-md-8 col-md-offset-4" style="margin-top: 2%">
            	              <input type="submit" name="enregistrer" id="enregistrer" value="enregistrer"/>           	              
             	          <input type="submit" name="supprimer" id="supprimer" value="supprimer"/>           	              
            	             
            	              <input type="hidden" name="idEnseignement" id="idEnseignement" value="${enseignement.idEnseignement}"/>
            	              <input type="hidden" name="idEnseignant" id="idEnseignant" value="${enseignement.idEnseignant}"/>              	              
	              </div>
	              </div>
		      </form>
		      </div>
		      </div>
		 </div> 
		 </div>
				
</body>
<div class="row">
		<div class="col-md-12">            
			<footer>
				<img src="resources/footer.png" class="img-fluid" alt="Responsive image" style="width:100%; height:auto">		
			</footer>
		</div>
	</div>
</html>