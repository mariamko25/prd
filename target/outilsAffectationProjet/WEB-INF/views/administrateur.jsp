<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
	    <title>Administrateur</title>
	    <link rel="stylesheet" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css">
	    <script src="webjars/jquery/3.1.1/jquery.min.js"></script>
		<script src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
		<script type="text/javascript" src="webjars/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
		<link rel="stylesheet" href="webjars/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" type="text/css"/>
		<link rel="stylesheet" href="resources/style.css">
		   
	</head>
<body>
<!--       Barre de navigation         -->
			<nav class="navbar navbar-default">
				  <div class="container-fluid">			    
					    <ul class="nav navbar-nav">
						      <li><a href="index">ACCUEIL</a></li>
							      
							      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">PROJET<span class="caret"></span></a>
								        <ul id="drop" class="dropdown-menu" >
								          <li ><a href="authentificationEnseignant">ENSEIGNANT</a></li>
								          <li><a href="authentificationEtudiant">ETUDIANT</a></li>
								        </ul>
							      </li>
						      
						      <li class="active"><a href="authentificationAdministrateur">ADMINISTRATEUR</a></li>
					    </ul>
				  </div>
			</nav>
			
			
			<form action="administrateurDeconnexion" method="get" style="margin-left:70%; ">
			
				<input type="submit" name="deconnexion" value="Se deconnecter">
			
			</form>
				<div class="row">
					<div class="col-md-8 col-md-offset-3">
					<!--       Barre de navigation         -->
					<nav class="navbar navbar-default" id="navigator" style="background-color:white;border-color:white">
						  <div class="container-fluid" >			    
							    <ul  class="nav navbar-nav"   style="padding-left:6%">
								      <li ><a href="administrateur" >Liste Enseignements</a></li>								      
									  <li><a href="ajoutEnseignement">Ajout Enseignement</a></li>
									  <li><a href="gestionTables">Gestion Tables</a></li>
							    </ul>
						  </div>
					</nav>
					</div>
				</div>
			
		<!--      Tableau de recherche         -->
		  <div class="container" style="margin-top: 4%;">
  		       <form action="enseignantRecherche" method="get" >
		            <div class="row" >
	        				<div class="col-md-8 col-md-offset-2">
				                <div class="input-group">
				                    <!-- USE TWITTER TYPEAHEAD JSON WITH API TO SEARCH -->
				                    <input class="form-control" id="system-search" name="research" placeholder="Thématique sujet"  value=" <c:out value="${param.research}" escapeXml="true"/> ">
				                    
									    
				                    <span class="input-group-btn" id="search-button">
				                        <button type="submit" id="searchDessin" class="btn btn-default"><i class="glyphicon glyphicon-search" ></i></button>
				                    </span>
				                    
				                </div>
		                		</div>
		                	</div>			                
	            </form>
	              <div class="row">
			        <div class="col-md-8 col-md-offset-2">   
		            <table class="table table-list-search"  style="margin-bottom: 46%">
		                    <thead>
		                        <tr style="color:white; background-color:#00a99b">
		                            <th>Enseignement</th>
		                            <th>Année</th>
		                            <th>Intitulé</th>
		                            <th>Ouvrir</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    
		                    <c:forEach items="${enseignements}" var="current" varStatus="loop">
										
										<c:if test="${loop.index mod 2 eq 0 }">
											<form action="adminModifierEnseignement" method="get" >
										        <tr>
													
														<td> ${current.nom} </td>
														<td> ${current.annee} </td>
														<td> ${current.description} </td>
														<td> <input type="submit" name="ouvrir" value="Ouvrir" > </td>
														
												</tr> 
												<input type="hidden" name="idEnseignement" value="${current.idEnseignement}" >
											</form>
									    </c:if>
									    <c:if test="${loop.index mod 2 ne 0 }">
									    <form action="adminModifierEnseignement" method="get" >
									        <tr style="background-color:#e6fff9">
													<td> ${current.nom} </td>
													<td> ${current.annee} </td>
													<td> ${current.description} </td>
													<td> <input type="submit" name="ouvrir" value="Ouvrir"> </td>
													
											</tr> 
												<input type="hidden" name="idEnseignement" value="${current.idEnseignement}" >
										</form>
											
									    </c:if>
										
							</c:forEach>
		                    </tbody>
		                </table>  
		                		
		                
		                </div> 
				</div>
			 </div>
</body>
<div class="row">
		<div class="col-md-12">            
			<footer>
				<img src="resources/footer.png" class="img-fluid" alt="Responsive image" style="width:100%; height:auto">		
			</footer>
		</div>
	</div>
</html>