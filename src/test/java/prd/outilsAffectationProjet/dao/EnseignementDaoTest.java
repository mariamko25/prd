package prd.outilsAffectationProjet.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import prd.outilsAffectationProjet.config.HibernateConfiguration;
import prd.outilsAffectationProjet.model.*;

/**
 * Class de test pour EnseignementDao
 * @author MariamKonate
 */
public class EnseignementDaoTest {
	
	/**
	 * Test Enregistrer enseignement dans la base
	 */
	@Test
	public void testEnregistrerEnseignement() {
				//Je crée la session
				Session session = HibernateConfiguration.getSessionFactory().openSession();
				// J'obtient le nombre d'enseignement actuel
				int size= session.createQuery("from Enseignement", Enseignement.class).list().size();
				// J'enregiste un Enseignement
				List<Integer> listIdEnseignant=session.createQuery("Select Max(idEnseignant) from Enseignant", Integer.class).list();
				if(listIdEnseignant.get(0)!=null && !listIdEnseignant.isEmpty())
				{
					// J'enregiste un enseignement
					Enseignement enseignement= new Enseignement();
					enseignement.setAnnee(0);
					enseignement.setTypeOptimisation("PEIP");
					enseignement.setIdEnseignant(listIdEnseignant.get(0));
					enseignement.setNbMaxScore(3);
					enseignement.setNbMaxTotal(12);
					enseignement.setFormulaire(0);

				
					new EnseignementDao().enregistrerEnseignement(enseignement);
					// Je verifie que la liste des enseignants a augmenté
					int sizeVerification=session.createQuery("from Enseignement", Enseignement.class).list().size();				
					assertTrue(size<sizeVerification);
					// Je supprime le dernier Enseignement enregistré
					List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();
					if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
					{
						List<Enseignement> enseignements=session.createQuery("from Enseignement where idEnseignement = "+listIdEnseignement.get(0), Enseignement.class).list();
						if(!enseignements.isEmpty()) {
						Transaction transaction = null;
						transaction = session.beginTransaction();
						session.delete(enseignements.get(0));
						transaction.commit();
						}
					}
				}

	}

	/**
	 * Test Supprimer un enseignement portant l'id idEnseignement
	 */
	@Test
	public void testSupprimerEnseignement() {
		//Je crée la session
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		
		List<Integer> listIdEnseignant=session.createQuery("Select Max(idEnseignant) from Enseignant", Integer.class).list();
		if(listIdEnseignant.get(0)!=null && !listIdEnseignant.isEmpty())
		{
			// J'enregiste un enseignement
			Enseignement enseignement= new Enseignement();
			enseignement.setAnnee(0);
			enseignement.setTypeOptimisation("PEIP");
			enseignement.setIdEnseignant(listIdEnseignant.get(0));
			enseignement.setNbMaxScore(3);
			enseignement.setNbMaxTotal(12);
			enseignement.setFormulaire(0);

			/** Enregistrement du sujet*/
			Transaction transaction = null;
			// start a transaction
			transaction = session.beginTransaction();
			// save the sujet object
			session.save(enseignement);
			// commit transaction
			transaction.commit();			
			List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();

			if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
			{
				new EnseignementDao().supprimerEnseignement(listIdEnseignement.get(0));
				List<Enseignement> enseignements=session.createQuery("from Enseignement where idEnseignement = "+listIdEnseignement.get(0), Enseignement.class).list();
				assertTrue(enseignements.isEmpty());
			}
		}
				
	}

	/**
	 * Test mette à jour un enseignement
	 */
	@Test
	public void testUpdateEnseignement() {
		/** J'ouvre une session*/
		Session session = HibernateConfiguration.getSessionFactory().openSession();

		/** Je crée un enseignement*/
		List<Integer> listIdEnseignant=session.createQuery("Select Max(idEnseignant) from Enseignant", Integer.class).list();
		if(listIdEnseignant.get(0)!=null && !listIdEnseignant.isEmpty())
		{
			// J'enregiste un enseignement
			Enseignement enseignement= new Enseignement();
			enseignement.setAnnee(0);
			enseignement.setTypeOptimisation("PEIP");
			enseignement.setIdEnseignant(listIdEnseignant.get(0));
			enseignement.setNbMaxScore(3);
			enseignement.setNbMaxTotal(12);
			enseignement.setFormulaire(0);

			
			/** Enregistrement du sujet*/
			Transaction transaction = null;
			// start a transaction
			transaction = session.beginTransaction();
			// save the sujet object
			session.save(enseignement);
			// commit transaction
			transaction.commit();
			
			/**Je teste la méthode update*/
			int idEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list().get(0);
			enseignement.setAnnee(2019);
			new EnseignementDao().updateEnseignement(idEnseignement, enseignement);
			
			List<Enseignement> listEnseignements=session.createQuery("from Enseignement where idEnseignement = " + idEnseignement, Enseignement.class).list();
			assertEquals(listEnseignements.size(),1);
			Enseignement Verification=listEnseignements.get(0);
			assertEquals(enseignement.getAnnee(),Verification.getAnnee());
			
			/** Je supprime le sujet inséré*/
			transaction = null;
			transaction = session.beginTransaction();
			Enseignement ens = session.load(Enseignement.class, idEnseignement);
			session.delete(ens);
			transaction.commit();
		}
	}

	/**
	 * Test Obtenir un enseignement à partir de son identifiant
	 */
	@Test
	public void testGetEnseignement() {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();

		if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
		{
			List<Enseignement> enseignement=session.createQuery("from Enseignement where idEnseignement = "+listIdEnseignement.get(0), Enseignement.class).list();
			assertNotNull(enseignement.get(0));
		}
	}

	/**
	 * Test Récupérer la liste des enseignements
	 */
	@Test
	public void testGetEnseignements() {

		Session session = HibernateConfiguration.getSessionFactory().openSession();
	    List<Enseignement> enseignements= session.createQuery("from Enseignement", Enseignement.class).list();
	
	    List<Enseignement>enseignementsVerification= new EnseignementDao().getEnseignements();
	
	    assertEquals(enseignements,enseignementsVerification);

	}

	/**
	 * Test Récupérer la liste des enseignements de l'enseignant dont la description est
	 * comme "like"
	 */
	@Test
	public void testGetEnseignementsLike() {
		/** J'ouvre une session*/
		Session session = HibernateConfiguration.getSessionFactory().openSession();

		/** Je crée des enseignements*/
		
		
		ArrayList<String> like= new ArrayList<String>();
		like.add("pokemon");
		like.add("aimer les pokemon");
		like.add("pokemon dangereux");
		
		List<Integer> listIdEnseignant=session.createQuery("Select Max(idEnseignant) from Enseignant", Integer.class).list();
		if(listIdEnseignant.get(0)!=null && !listIdEnseignant.isEmpty())
		{
			for(String nom:like)
			{
				// J'enregiste un enseignement
				Enseignement enseignement= new Enseignement();
				enseignement.setNom(nom);
				enseignement.setAnnee(0);
				enseignement.setTypeOptimisation("PEIP");
				enseignement.setIdEnseignant(listIdEnseignant.get(0));
				enseignement.setNbMaxScore(3);
				enseignement.setNbMaxTotal(12);
				enseignement.setFormulaire(0);
				
				/** Enregistrement du sujet*/
				Transaction transaction = null;
				// start a transaction
				transaction = session.beginTransaction();
				// save the sujet object
				session.save(enseignement);
				// commit transaction
				transaction.commit();
				
			}
			
			/** tester la methode getEnseignements(String like)*/
			List<Enseignement> liste=new EnseignementDao().getEnseignementsLike("pokemon");
			assertTrue(liste.size()>=3);
			Transaction transaction = null;

			for(Enseignement ens:liste)
			{
				transaction = session.beginTransaction();
				Enseignement enseignement = session.load(Enseignement.class, ens.getIdEnseignement());
				session.delete(enseignement);

				transaction.commit();
			}
			
			
			
		}
		

	}

	/**
	 * Récupérer la liste des enseignements d'un enseignant
	 */
	@Test
	public  void testGetEnseignementsEnseignant() {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdEnseignant=session.createQuery("Select Max(idEnseignant) from Enseignant", Integer.class).list();
		if(listIdEnseignant.get(0)!=null && !listIdEnseignant.isEmpty())
		{
			List<Enseignement> ens=session.createQuery(
					"from Enseignement E where E.idEnseignant = " + listIdEnseignant.get(0) + " order by idEnseignement desc",
					Enseignement.class).list();
			List<Enseignement> ensVerif= new EnseignementDao().getEnseignementsEnseignant(listIdEnseignant.get(0));
			
			assertEquals(ens,ensVerif);
		}
		 

	}

	/**
	 * Récupérer la liste des enseignements limités
	 */
	@Test
	public  void testGetEnseignementsLimitee() {

		int limite=3;
		Session session = HibernateConfiguration.getSessionFactory().openSession();
	    List<Enseignement> enseignements= session.createQuery("from Enseignement order by idEnseignement desc", Enseignement.class)
				.setMaxResults(limite).list();
	    List<Enseignement>enseignementsVerification= new EnseignementDao().getEnseignements(limite);
	
	    assertEquals(enseignements,enseignementsVerification);
	    assertTrue(enseignementsVerification.size()<=3);

	}

	/**
	 * Récupérer l'id de l'enseignant qui encadre l'enseignement portant comme
	 * identifiant idEnseignement
	 */
	@Test
	public void testGetIdEnseignant() {
		
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();
		if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
		{
			int id = session.createQuery("select E.idEnseignant from Enseignement E where E.idEnseignement = " + listIdEnseignement.get(0),
							Integer.class)
					.list().get(0);
			int idVerif= new EnseignementDao().getIdEnseignant(listIdEnseignement.get(0));
			assertEquals(id,idVerif);
		}
		
		
	}

	/**
	 * Récupérer l'id de l'enseignement qui se nomme nomEnseignement et qui a été
	 * créé en annee
	 */
	@Test
	public void testGetIdEnseignementNomAnnee() {
		//Je crée la session
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdEnseignant=session.createQuery("Select Max(idEnseignant) from Enseignant", Integer.class).list();
		if(listIdEnseignant.get(0)!=null && !listIdEnseignant.isEmpty())
		{
			// J'enregiste un enseignement
			Enseignement enseignement= new Enseignement();
			enseignement.setAnnee(20004);
			enseignement.setNom("Pokemon");
			enseignement.setTypeOptimisation("PEIP");
			enseignement.setIdEnseignant(listIdEnseignant.get(0));
			enseignement.setNbMaxScore(3);
			enseignement.setNbMaxTotal(12);
			enseignement.setFormulaire(0);
			
			/** Enregistrement du sujet*/
			Transaction transaction = null;
			// start a transaction
			transaction = session.beginTransaction();
			// save the sujet object
			session.save(enseignement);
			// commit transaction
			transaction.commit();	
			
			List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();
			int id=listIdEnseignement.get(0);
			int idverif=new EnseignementDao().getIdEnseignement(enseignement.getNom(), enseignement.getAnnee());
			assertEquals(id,idverif);
			
		    	transaction = session.beginTransaction();
		    	Enseignement ensSup=session.load(Enseignement.class, id);
		    	session.delete(ensSup);
		    	transaction.commit();
		}

	}

	/**
	 * Récupérer la liste des enseignements de l'annee
	 */
	@Test
	public void testGetIdEnseignement() {
				//Je crée la session
				Session session = HibernateConfiguration.getSessionFactory().openSession();
				List<Integer> listIdEnseignant=session.createQuery("Select Max(idEnseignant) from Enseignant", Integer.class).list();
				if(listIdEnseignant.get(0)!=null && !listIdEnseignant.isEmpty())
				{
					// J'enregiste un enseignement
					Enseignement enseignement= new Enseignement();
					enseignement.setAnnee(20004);
					enseignement.setNom("Pokemon");
					enseignement.setTypeOptimisation("PEIP");
					enseignement.setIdEnseignant(listIdEnseignant.get(0));
					enseignement.setNbMaxScore(3);
					enseignement.setNbMaxTotal(12);
					enseignement.setFormulaire(0);
					
					/** Enregistrement du sujet*/
					Transaction transaction = null;
					// start a transaction
					transaction = session.beginTransaction();
					// save the sujet object
					session.save(enseignement);
					// commit transaction
					transaction.commit();	
					List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();

					List<Integer> liste= new EnseignementDao().getIdEnseignement(20004);
					int id=listIdEnseignement.get(0);
					int idverif=liste.get(0);
					assertEquals(id,idverif);
					
					transaction = session.beginTransaction();
				    	Enseignement ensSup=session.load(Enseignement.class, id);
				    	session.delete(ensSup);
				    	transaction.commit();
				}
	}

	/**
	 * Récupérer le nom de l'enseignant ayant pour identifiant idEnseignant
	 */
	@Test
	public void testGetNomEncadrant() {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdEnseignant=session.createQuery("Select Max(idEnseignant) from Enseignant", Integer.class).list();
		if(listIdEnseignant.get(0)!=null && !listIdEnseignant.isEmpty())
		{
			String nom = session.createQuery("select E.nom from Enseignant E where E.id = " + listIdEnseignant.get(0), String.class)
					.list().get(0);
			
			String nomVerif= new EnseignementDao().getNomEncadrant(listIdEnseignant.get(0));
			
			assertEquals(nom,nomVerif);
		}
		
		
	}

	/**
	 * Liste Enseignement ayant un formulaire ouvert
	 */
	@Test
	public void getEnseignementsFormulaire() {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Enseignement> ens=session.createQuery("from Enseignement where formulaire = 1", Enseignement.class).list();
		List<Enseignement> ensVerif= new EnseignementDao().getEnseignementsFormulaire();		
		assertEquals(ens,ensVerif);
	}

	/**
	 * Supprimer la liste des enseignements
	 */
	@Test
    public void deleteEnseignements() {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();

		if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
		{
			List<Enseignement> enseignements=session.createQuery("from Enseignement where idEnseignement = "+listIdEnseignement.get(0), Enseignement.class).list();
			assertNotNull(enseignements.get(0));
		}  
    }

}
