package prd.outilsAffectationProjet.dao;

import org.junit.*;



import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import prd.outilsAffectationProjet.config.HibernateConfiguration;
import prd.outilsAffectationProjet.model.*;

/**
 * Class de test pour le SujetDao
 * @author MariamKonate
 */
public class SujetDaoTest {
	
	/**
	 * Test de l'enregistrement d'un sujet
	 */
	@Test
	public void testEnregistrerSujet()
	{
		/** J'ouvre une session*/
		Session session = HibernateConfiguration.getSessionFactory().openSession();

		/** Je crée un sujet*/
		int idSujet=0;
		List<Integer> listId=session.createQuery("Select Max(idSujet) from Sujet", Integer.class).list();
		if(listId.get(0)==null || listId.isEmpty())
		{
			idSujet=idSujet+1;
		}
		else
		{
			idSujet= listId.get(0)+1;
		}
		

		Sujet sujetInitial= new Sujet();
		sujetInitial.setIdSujet(idSujet);
		sujetInitial.setIntitule("sujetTest1");
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();
		
		if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
		{
			sujetInitial.setIdEnseignement(listIdEnseignement.get(0));
			sujetInitial.setAnnee(2019);
			sujetInitial.setDescription("Sujet de test");
			sujetInitial.setNbCopie(1);
			
			/** J'enregistre le sujet*/
			new SujetDao().enregistrerSujet(sujetInitial);
			/** Je verifie que le sujet que j'ai créé est bien celui qui est enregistré*/
			idSujet=session.createQuery("Select Max(idSujet) from Sujet", Integer.class).list().get(0);
			List<Sujet> listSujet=session.createQuery("from Sujet where idSujet = " + idSujet, Sujet.class).list();
			assertEquals(listSujet.size(),1);
			
			Sujet sujetVerification=listSujet.get(0);
			assertEquals(sujetVerification.getAnnee(),sujetInitial.getAnnee());
			assertEquals(sujetVerification.getIntitule(),sujetInitial.getIntitule());
			assertEquals(sujetVerification.getIdEnseignement(),sujetInitial.getIdEnseignement());
			assertEquals(sujetVerification.getDescription(),sujetInitial.getDescription());
			assertEquals(sujetVerification.getNbCopie(),sujetInitial.getNbCopie());
			
			/** Je supprime le sujet inséré*/
			Transaction transaction = null;
			transaction = session.beginTransaction();
			Sujet sujet = session.load(Sujet.class, idSujet);
			session.delete(sujet);

			transaction.commit();	
		}
		
	}
	
	/**
	 * Test de l'obtention d'un sujet
	 */
	@Test
	public void testGetSujet(){
		/** J'ouvre une session*/
		Session session = HibernateConfiguration.getSessionFactory().openSession();

		/** Je crée un sujet*/
		int idSujet=0;
		List<Integer> listId=session.createQuery("Select Max(idSujet) from Sujet", Integer.class).list();
		if(listId.get(0)==null || listId.isEmpty())
		{
			idSujet=idSujet+1;
		}
		else
		{
			idSujet= listId.get(0)+1;
		}
		
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();
		
		if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
		{
			Sujet sujetInitial= new Sujet();
			sujetInitial.setIdSujet(idSujet);
			sujetInitial.setIntitule("sujetTest1");
			sujetInitial.setIdEnseignement(listIdEnseignement.get(0));
			sujetInitial.setAnnee(2019);
			sujetInitial.setDescription("Sujet de test");
			sujetInitial.setNbCopie(1);
			
			/** Enregistrement du sujet*/
			Transaction transaction = null;
			// start a transaction
			transaction = session.beginTransaction();
			// save the sujet object
			session.save(sujetInitial);
			// commit transaction
			transaction.commit();
			
			
			/** Je teste la methode get */
			idSujet=session.createQuery("Select Max(idSujet) from Sujet", Integer.class).list().get(0);

			Sujet sujetVerification=new SujetDao().getSujet(idSujet);
			assertEquals(sujetVerification.getAnnee(),sujetInitial.getAnnee());
			assertEquals(sujetVerification.getIntitule(),sujetInitial.getIntitule());
			assertEquals(sujetVerification.getIdEnseignement(),sujetInitial.getIdEnseignement());
			assertEquals(sujetVerification.getDescription(),sujetInitial.getDescription());
			assertEquals(sujetVerification.getNbCopie(),sujetInitial.getNbCopie());
			
			/** Je supprime le sujet inséré*/
			transaction = null;
			transaction = session.beginTransaction();
			Sujet sujet = session.load(Sujet.class, idSujet);
			session.delete(sujet);

			transaction.commit();	
		}
		
		
	}
	
	/**
	 * Test de la suppression d'un sujet
	 */
	@Test
	public void testDeleteSujet() {
		
		/** J'ouvre une session*/
		Session session = HibernateConfiguration.getSessionFactory().openSession();

		/** Je crée un sujet*/
		int idSujet=0;
		List<Integer> listId=session.createQuery("Select Max(idSujet) from Sujet", Integer.class).list();
		if(listId.get(0)==null || listId.isEmpty())
		{
			idSujet=idSujet+1;
		}
		else
		{
			idSujet= listId.get(0)+1;
		}
		
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();
		
		if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
		{
			Sujet sujetInitial= new Sujet();
			sujetInitial.setIdSujet(idSujet);
			sujetInitial.setIntitule("sujetTest1");
			sujetInitial.setIdEnseignement(listIdEnseignement.get(0));
			sujetInitial.setAnnee(2019);
			sujetInitial.setDescription("Sujet de test");
			sujetInitial.setNbCopie(1);
			
			/** Enregistrement du sujet*/
			Transaction transaction = null;
			// start a transaction
			transaction = session.beginTransaction();
			// save the sujet object
			session.save(sujetInitial);
			// commit transaction
			transaction.commit();
			
			/** Je teste la méthode delete*/
			idSujet=session.createQuery("Select Max(idSujet) from Sujet", Integer.class).list().get(0);
			new SujetDao().deleteSujet(idSujet);
			List<Sujet> listSujet=session.createQuery("from Sujet where idSujet = " + idSujet, Sujet.class).list();

			assertEquals(listSujet.size(),0);
		}
		
		
		
	}
	
	/**
	 * Test de la mise a jour d'un sujet
	 */
	@Test
	public void testUpdateSujet() {
		/** J'ouvre une session*/
		Session session = HibernateConfiguration.getSessionFactory().openSession();

		/** Je crée un sujet*/
		int idSujet=0;
		List<Integer> listId=session.createQuery("Select Max(idSujet) from Sujet", Integer.class).list();
		if(listId.get(0)==null || listId.isEmpty())
		{
			idSujet=idSujet+1;
		}
		else
		{
			idSujet= listId.get(0)+1;
		}
		
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();
		
		if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
		{
			Sujet sujetInitial= new Sujet();
			sujetInitial.setIdSujet(idSujet);
			sujetInitial.setIntitule("sujetTest1");
			sujetInitial.setIdEnseignement(listIdEnseignement.get(0));
			sujetInitial.setAnnee(2019);
			sujetInitial.setDescription("Sujet de test");
			sujetInitial.setNbCopie(1);
			
			/** Enregistrement du sujet*/
			Transaction transaction = null;
			// start a transaction
			transaction = session.beginTransaction();
			// save the sujet object
			session.save(sujetInitial);
			// commit transaction
			transaction.commit();
			
			/**Je teste la méthode update*/
			idSujet=session.createQuery("Select Max(idSujet) from Sujet", Integer.class).list().get(0);
			sujetInitial.setIntitule("Verification");
			new SujetDao().updateSujet(idSujet, sujetInitial);
			List<Sujet> listSujet=session.createQuery("from Sujet where idSujet = " + idSujet, Sujet.class).list();
			assertEquals(listSujet.size(),1);
			Sujet sujetVerification=listSujet.get(0);
			assertEquals(sujetVerification.getAnnee(),sujetInitial.getAnnee());
			assertEquals(sujetVerification.getIntitule(),sujetInitial.getIntitule());
			assertEquals(sujetVerification.getIdEnseignement(),sujetInitial.getIdEnseignement());
			assertEquals(sujetVerification.getDescription(),sujetInitial.getDescription());
			assertEquals(sujetVerification.getNbCopie(),sujetInitial.getNbCopie());
			/** Je supprime le sujet inséré*/
			transaction = null;
			transaction = session.beginTransaction();
			Sujet sujet = session.load(Sujet.class, idSujet);
			session.delete(sujet);

			transaction.commit();
		}
		
		
		
	}
	
	/**
	 * Test de l'obtention de tous les  sujets
	 */
	@Test
	public void testGetSujets() {
		/** On obtient la liste des sujets*/
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Sujet> listeInitiale= session.createQuery("from Sujet", Sujet.class).list();
		
		/** On teste notre methode getSujets */
		List<Sujet> listeVerification= new SujetDao().getSujets();
		assertEquals(listeInitiale.size(),listeVerification.size());
			
	}
	
	/**
	 * Test de la suppression de tous les sujets 
	 */
	@Test
	public void testDeleteSujets() {
		
		/** J'ouvre une session*/
		Session session = HibernateConfiguration.getSessionFactory().openSession();

		/** Je crée un sujet*/
		int idSujet=0;
		List<Integer> listId=session.createQuery("Select Max(idSujet) from Sujet", Integer.class).list();
		if(listId.get(0)==null || listId.isEmpty())
		{
			idSujet=idSujet+1;
		}
		else
		{
			idSujet= listId.get(0)+1;
		}
		
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();
		
		if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
		{
			Sujet sujetInitial= new Sujet();
			sujetInitial.setIdSujet(idSujet);
			sujetInitial.setIntitule("sujetTest1");
			sujetInitial.setIdEnseignement(listIdEnseignement.get(0));
			sujetInitial.setAnnee(2019);
			sujetInitial.setDescription("Sujet de test");
			sujetInitial.setNbCopie(1);
			
			/** Enregistrement du sujet*/
			Transaction transaction = null;
			// start a transaction
			transaction = session.beginTransaction();
			// save the sujet object
			session.save(sujetInitial);
			// commit transaction
			transaction.commit();
			
			
			/** Je teste la methode deleteSujets */
			new SujetDao().deleteSujets();
			List<Sujet> liste= session.createQuery("from Sujet", Sujet.class).list();
			
			assertEquals(liste.size(),0);
		}
			
		
		
	}
	
	/**
	 * Test de l'obtention des sujets en fonction d'une chaine
	 * de caractères
	 */
	@Test
	public void testGetSujetsStr() {
		
		/** J'ouvre une session*/
		Session session = HibernateConfiguration.getSessionFactory().openSession();

		/** Je crée un sujet*/
		int idSujet=0;
		List<Integer> listId=session.createQuery("Select Max(idSujet) from Sujet", Integer.class).list();
		if(listId.get(0)==null || listId.isEmpty())
		{
			idSujet=idSujet+1;
		}
		else
		{
			idSujet= listId.get(0)+1;
		}
		
		ArrayList<String> like= new ArrayList<String>();
		like.add("pokemon");
		like.add("aimer les pokemon");
		like.add("pokemon dangereux");
		int indice= idSujet;
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();
		
		if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
		{
			for(String intitule:like)
			{
				Sujet sujetInitial= new Sujet();
				sujetInitial.setIdSujet(indice);
				sujetInitial.setIntitule(intitule);
				sujetInitial.setIdEnseignement(listIdEnseignement.get(0));
				sujetInitial.setAnnee(2019);
				sujetInitial.setDescription("sujet de test");
				sujetInitial.setNbCopie(1);
				
				/** Enregistrement du sujet*/
				Transaction transaction = null;
				// start a transaction
				transaction = session.beginTransaction();
				// save the sujet object
				session.save(sujetInitial);
				// commit transaction
				transaction.commit();
				indice++;
				
			}
			
			/** tester la methode getSujets(String like)*/
			List<Sujet> liste=new SujetDao().getSujets("pokemon");
			assertTrue(liste.size()>=3);
			Transaction transaction = null;

			for(Sujet s:liste)
			{
				transaction = session.beginTransaction();
				Sujet sujet = session.load(Sujet.class, s.getIdSujet());
				session.delete(sujet);

				transaction.commit();
			}
			
			indice++;
			for(String description:like)
			{
				Sujet sujetInitial= new Sujet();
				sujetInitial.setIdSujet(indice);
				sujetInitial.setIntitule("sujet de test"+indice);
				sujetInitial.setIdEnseignement(listIdEnseignement.get(0));
				sujetInitial.setAnnee(2019);
				sujetInitial.setDescription(description);
				sujetInitial.setNbCopie(1);
				
				/** Enregistrement du sujet*/
				// start a transaction
				transaction = session.beginTransaction();
				// save the sujet object
				session.save(sujetInitial);
				// commit transaction
				transaction.commit();
				indice++;
			}
			
			
			/** tester la methode getSujets(String like)*/
			 liste=new SujetDao().getSujets("pokemon");
			 assertTrue(liste.size()>=3);
			 
			 for(Sujet s:liste)
				{
					transaction = session.beginTransaction();
					Sujet sujet = session.load(Sujet.class, s.getIdSujet());
					session.delete(sujet);

					transaction.commit();
				}
		}
		
	}
	
	/**
	 * Test de l'obtention d'une liste de sujet limitée à 3
	 */
	@Test
	public void testGetSujetsLimit() {
		List<Sujet> liste= new SujetDao().getSujets(3);
		assertTrue(liste.size()<=3);
	}
	
	/**
	 * Test de l'obtention des sujets d'un enseignement
	 */
	@Test
	public void testGetSujetsEnseignement() {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();
		
		if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
		{
			List<Sujet> sujets= session.createQuery("from Sujet S where S.idEnseignement = " + listIdEnseignement.get(0), Sujet.class).list();
			List<Sujet> sujetsVerification= new SujetDao().getSujetsEnseignement(listIdEnseignement.get(0));
			assertEquals(sujets.size(),sujetsVerification.size());
		}
		
	}
	
	/**
	 * Test des sujets en fonction d'un enseignant et  d'un enseignement
	 */
	@Test
	public void testGetSujetsEnseignementEnseignant() {
		
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();
		List<Integer> listIdEnseignant=session.createQuery("Select Max(idEnseignant) from Enseignant", Integer.class).list();

		if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
		{
			if(listIdEnseignant.get(0)!=null && !listIdEnseignant.isEmpty())
			{
				List<Sujet> sujets= session.createQuery("from Sujet as S where S.idEnseignement = " + listIdEnseignement.get(0)
						+ " and S.idSujet in (select E.idSujet from Encadrant as E where E.idEnseignant = " + listIdEnseignant.get(0)
						+ " )", Sujet.class).list();
				
				List<Sujet> sujetsVerification= new SujetDao().getSujetsEnseignementEnseignant(listIdEnseignement.get(0), listIdEnseignant.get(0));

				assertEquals(sujets.size(),sujetsVerification.size());
			}
		}
		

	}
	
	/**
	 * Test des sujets en fonction d'un enseignement et  d'une description
	 */
	@Test
	public void testGetSujetsEnseignementDescription() {
		
		String description="";
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();

		if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
		{
			List<Sujet> sujets= session
					.createQuery(
							"from Sujet S where S.idEnseignement = " + listIdEnseignement.get(0) + "and lower(S.description) like '%"
									+ description.toLowerCase().trim() + "%' order by idSujet desc",
							Sujet.class).list();
			
			List<Sujet> sujetsVerification= new SujetDao().getSujetsEnseignementDescription(listIdEnseignement.get(0), description);

			assertEquals(sujets.size(),sujetsVerification.size());
		}
		
		
	}
	
	/**
	 * Test des noms de l'enseignement auquel appartient un sujet
	 */
	@Test
	public void testGetNomEnseignementSujet() {
		
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();

		if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
		{
			String nom = session.createQuery("select E.nom from Enseignement E where E.idEnseignement = " + listIdEnseignement.get(0),
					String.class).list().get(0);
			String nomVerification= new SujetDao().getNomEnseignementSujet(listIdEnseignement.get(0));
			assertEquals(nom,nomVerification);
		}
		
		
	}
	
	/**
	 * Test Enseignement auquel appartient un sujet
	 */
	@Test
	public void testGetEnseignementSujet() {
		
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();

		if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
		{
			Enseignement enseignement = session
					.createQuery(" from Enseignement E where E.idEnseignement = " + listIdEnseignement.get(0), Enseignement.class)
					.list().get(0);
			Enseignement enseignementVerification= new SujetDao().getEnseignementSujet(listIdEnseignement.get(0));
			
			assertEquals(enseignement.getNom(),enseignementVerification.getNom());
		}
		
	}
	
	/**
	 * Test Annee de l'enseignement auquel appartient le sujet
	 */
	@Test
	public void testGetAnneeEnseignementSujet() {
		
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();

		if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
		{
			int annee = -1;
			if (!session.createQuery("select E.annee from Enseignement E where E.idEnseignement = " + listIdEnseignement.get(0),
					Integer.class).list().isEmpty()) {
				annee = session.createQuery("select E.annee from Enseignement E where E.idEnseignement = " + listIdEnseignement.get(0),
						Integer.class).list().get(0);
				int anneeVerification= new SujetDao().getAnneeEnseignementSujet(listIdEnseignement.get(0));
				
				assertEquals(annee,anneeVerification);
			}
		}
		
	}
	
	/**
	 * Test Obtenir l'enseignement en fonction du nom et de l'annee
	 */
	@Test
	public void testGetNomEnseignementSujetAnnee() {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();

		if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
		{
			String nom = session.createQuery(
					"select E.nom from Enseignement E where E.idEnseignement = " + listIdEnseignement.get(0) + "and E.annee=" + 2019,
					String.class).list().get(0);
			
			String nomVerification= new SujetDao().getNomEnseignementSujet(listIdEnseignement.get(0), 2019);
			assertEquals(nom,nomVerification);
		}
		
	}
	
	/**
	 * Test Obtenir un sujet en fonction de son intitule
	 */
	@Test
	public void testGetSujetIntitule() {
		
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Sujet> sujets = session
				.createQuery("from Sujet S where lower(S.intitule) = lower('" + "" + "') order by S.idSujet desc",
						Sujet.class)
				.list();
		Sujet sujetVerification=  new SujetDao().getSujetIntitule("");
		
		if(sujets.isEmpty())
		{
			assertNull(sujetVerification);
		}
		else
		{
			assertEquals(sujets.get(0).getIntitule(),sujetVerification.getIntitule());
		}
	}
	
	/**
	 *  Test Obtenir le sujet a partir de l'intitule et de l'idEnseignement
	 */
	@Test
	public void testGetSujetIntituleEnseignement() {
		
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();

		if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
		{
			List<Sujet> sujets = session.createQuery("from Sujet S where lower(S.intitule) = lower('" + ""
					+ "') and idEnseignement = " + listIdEnseignement.get(0) + " order by S.idSujet desc", Sujet.class).list();
			
			Sujet sujetVerification=  new SujetDao().getSujetIntituleEnseignement("",listIdEnseignement.get(0));

			if(sujets.isEmpty())
			{
				assertNull(sujetVerification);
			}
			else
			{
				assertEquals(sujets.get(0).getIntitule(),sujetVerification.getIntitule());

			}
		}
		
		
	}
	
}
