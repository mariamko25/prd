package prd.outilsAffectationProjet.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import prd.outilsAffectationProjet.config.HibernateConfiguration;
import prd.outilsAffectationProjet.model.Groupe;

/**
 * Class de test pour le GroupeDao
 * @author MariamKonate
 */
public class GroupeDaoTest {
	/**
	 * Test Enregistrer un groupe dans la base
	 */
	@Test
	public void testEnregistrerGroupe() {
		//Je crée la session
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		// J'obtient le nombre de groupe actuel
		int size= session.createQuery("from Groupe", Groupe.class).list().size();
		// J'enregiste un groupe
		Groupe groupe= new Groupe();
		new GroupeDao().enregistrerGroupe(groupe);
		// Je verifie que la liste des groupes a augmenté
		int sizeVerification= session.createQuery("from Groupe", Groupe.class).list().size();				
		assertTrue(size<sizeVerification);
		// Je supprime le dernier groupe enregistré
		List<Integer> listIdGroupe=session.createQuery("Select Max(idGroupe) from Groupe", Integer.class).list();
		if(listIdGroupe.get(0)!=null && !listIdGroupe.isEmpty())
		{
			List<Groupe> group=session.createQuery("from Groupe where idGroupe = "+listIdGroupe.get(0), Groupe.class).list();
			if(!group.isEmpty()) {
			Transaction transaction = null;
			transaction = session.beginTransaction();
			session.delete(group.get(0));
			transaction.commit();
			}
		}
	}

	/**
	 * Test Obtenir la liste des groupes
	 */
	@Test
	public void testGetGroupes() {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Groupe> groupes= session.createQuery("from Groupe", Groupe.class).list();
		
		List<Groupe> groupesVerification= new GroupeDao().getGroupes();
		
		assertEquals(groupes,groupesVerification);
	}

	/**
	 * Test Obtenir un groupe a partir de son identifiant
	 */
	@Test
	public void testGetGroupe() {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdGroupe=session.createQuery("Select Max(idGroupe) from Groupe", Integer.class).list();

		if(listIdGroupe.get(0)!=null && !listIdGroupe.isEmpty())
		{
			List<Groupe> group=session.createQuery("from Groupe where idGroupe = "+listIdGroupe.get(0), Groupe.class).list();
			assertNotNull(group.get(0));
		}
		
	}

	/**
	 * Test Supprimer le groupe avec id idGroupe
	 */
	@Test
	public void testDeleteGroup() {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		
		Groupe groupe= new Groupe();
		Transaction transaction = null;

		// start a transaction
		transaction = session.beginTransaction();
		// save the groupe object

		session.save(groupe);
		// commit transaction
		transaction.commit();

		List<Integer> listIdGroupe=session.createQuery("Select Max(idGroupe) from Groupe", Integer.class).list();

		if(listIdGroupe.get(0)!=null && !listIdGroupe.isEmpty())
		{
			new GroupeDao().deleteGroup(listIdGroupe.get(0));
			List<Groupe> group=session.createQuery("from Groupe where idGroupe = "+listIdGroupe.get(0), Groupe.class).list();
			assertTrue(group.isEmpty());
		}
		
	}

		/**
		 * Test Supprimer la liste des Groupes
		 */
		@Test
	    public void testDeleteGroupes() {
	    	new GroupeDao().deleteGroupes();
    		Session session=HibernateConfiguration.getSessionFactory().openSession();
        	List<Groupe> groupes= session.createQuery("from Groupe", Groupe.class).list();
        	assertTrue(groupes.isEmpty());
	    }
}
