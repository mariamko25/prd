package prd.outilsAffectationProjet.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import prd.outilsAffectationProjet.config.HibernateConfiguration;
import prd.outilsAffectationProjet.model.Affectation;

/**
 * Class de test pour AffectationDao
 */
public class AffectationDaoTest {
	
	/**
	 * Test Enregistrer une affectation dans la base
	 */
	@Test
	public void testEnregistrerAffectation() {
		/** J'ouvre une session*/
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdSujet=session.createQuery("Select Max(idSujet) from Sujet", Integer.class).list();
		List<Integer> listIdGroupe=session.createQuery("Select Max(idGroupe) from Groupe", Integer.class).list();
		
		/** Je crée une affectation*/
		if(listIdSujet.get(0)!=null && !listIdSujet.isEmpty())
		{
			if(listIdGroupe.get(0)!=null && !listIdGroupe.isEmpty())
			{
				List<Affectation> listAff=session.createQuery("from Affectation where idSujet = " + listIdSujet.get(0)+" and idGroupe = "+listIdGroupe.get(0), Affectation.class).list();
				if(listAff.isEmpty())
				{
					Affectation aff= new Affectation();
					aff.setIdGroupe(listIdGroupe.get(0));
					aff.setIdSujet(listIdSujet.get(0));
					/** J'enregistre l'affectation*/

					new AffectationDao().enregistrerAffectation(aff);
					/** Je verifie que l' affectation a bien été créée*/
					 listAff=session.createQuery("from Affectation where idSujet = " + listIdSujet.get(0)+" and idGroupe = "+listIdGroupe.get(0), Affectation.class).list();

					assertTrue(listAff.size()>0);
					
					/** Je supprime le souhait inséré*/
					Transaction transaction = null;
					transaction = session.beginTransaction();
					session.delete(listAff.get(0));
					transaction.commit();	
				}
			}
				
			
		}
    }
	/**
	 * Supprimer la liste des affectations
	 */
	@Test
    public void testDeleteAffectations() {
	     	new AffectationDao().deleteAffectations();
			/** On obtient la session*/
			Session session = HibernateConfiguration.getSessionFactory().openSession();
			List<Affectation> affectations = session.createQuery("from Affectation", Affectation.class).list();
			assertTrue(affectations.isEmpty());  
    }
    
    
    /**
	 * Obtenir la liste des affectations
	 */
	@Test
    public void testGetAffectations() {
		Session session=HibernateConfiguration.getSessionFactory().openSession();
		
		List<Affectation> liste=session.createQuery("from Affectation", Affectation.class).list();    		
		List<Affectation> listeVerif= new AffectationDao().getAffectations();
		
		assertEquals(liste,listeVerif);
    }
    
    /**
   	 * Obtenir la liste des affectations
   	 * pour un enseignement
   	 */
	@Test
    public void testGetAffectationsEnseignement() {
		Session session=HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();
		
		/** Je crée une affectation*/
		if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
		{
	   	 	List<Affectation> list=session.createQuery("from Affectation where idSujet in(select idSujet from Sujet where idEnseignement = "+listIdEnseignement.get(0)+")", Affectation.class).list();        
	   	 	List<Affectation> listVerif=new AffectationDao().getAffectations(listIdEnseignement.get(0));
	   	 	assertEquals(list,listVerif);
		}
    }
}
