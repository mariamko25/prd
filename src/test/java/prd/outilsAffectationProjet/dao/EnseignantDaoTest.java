package prd.outilsAffectationProjet.dao;

import static org.junit.Assert.*;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import prd.outilsAffectationProjet.config.HibernateConfiguration;
import prd.outilsAffectationProjet.model.Encadrant;
import prd.outilsAffectationProjet.model.Enseignant;
import prd.outilsAffectationProjet.model.Etudiant;
import prd.outilsAffectationProjet.model.Sujet;

/**
 * Class de test pour EnseignantDao
 * @author MariamKonate
 */
public class EnseignantDaoTest {
	/**
	 * Test Enregistrer enseignant dans la base
	 */
	@Test
	public void testEnregistrerEnseignant() {
				//Je crée la session
				Session session = HibernateConfiguration.getSessionFactory().openSession();
				// J'obtient le nombre d'enseignant actuel
				int size= session.createQuery("from Enseignant", Enseignant.class).list().size();
				// J'enregiste un enseignant
				Enseignant enseignant= new Enseignant();
				new EnseignantDao().enregistrerEnseignant(enseignant);
				// Je verifie que la liste des enseignants a augmenté
				int sizeVerification= session.createQuery("from Enseignant", Enseignant.class).list().size();				
				assertTrue(size<sizeVerification);
				// Je supprime le dernier enseignant enregistré
				List<Integer> listIdEnseignant=session.createQuery("Select Max(idEnseignant) from Enseignant", Integer.class).list();
				if(listIdEnseignant.get(0)!=null && !listIdEnseignant.isEmpty())
				{
					List<Enseignant> ens=session.createQuery("from Enseignant where idEnseignant = "+listIdEnseignant.get(0), Enseignant.class).list();
					if(!ens.isEmpty()) {
					Transaction transaction = null;
					transaction = session.beginTransaction();
					session.delete(ens.get(0));
					transaction.commit();
					}
				}
        
    }
	
	/**
	 * Test Obtenir l'identifiant d'un enseignant a partir de son adresse
	 * mail
	 */
	@Test
	public void testGetIdEnseignant()
	{
		//Je crée la session
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		// J'enregiste un enseignant
		Enseignant enseignant= new Enseignant();
		enseignant.setEmail("pokemonGame");
		Transaction transaction=null;
		 transaction = session.beginTransaction();
         // save the enseignant object
         session.save(enseignant);
         // commit transaction
         transaction.commit();
		
		
		
		// Je verifie que c'est le mm enseignant avec getIdEnseignant(email)
 		List<Integer> listIdEnseignant=session.createQuery("Select Max(idEnseignant) from Enseignant", Integer.class).list();
 		int id=listIdEnseignant.get(0);
 		int idVerif=new EnseignantDao().getIdEnseignant("pokemonGame");
		assertEquals(id,idVerif);
		
		
		
		// Je supprime le dernier enseignant enregistré
		if(listIdEnseignant.get(0)!=null && !listIdEnseignant.isEmpty())
		{
			List<Enseignant> ens=session.createQuery("from Enseignant where idEnseignant = "+listIdEnseignant.get(0), Enseignant.class).list();
			if(!ens.isEmpty()) {
			transaction = null;
			transaction = session.beginTransaction();
			session.delete(ens.get(0));
			transaction.commit();
			}
		}
	}
	
	/**
	 * Test Obtenir la liste des enseignants
	 */
	@Test
    public void testGetEnseignants() {
    	
	Session session = HibernateConfiguration.getSessionFactory().openSession();
    List<Enseignant> enseignants= session.createQuery("from Enseignant", Enseignant.class).list();

    List<Enseignant>enseignantsVerification= new EnseignantDao().getEnseignants();

    assertEquals(enseignants,enseignantsVerification);
        
    }
    
    /**
     * Test Obtenir la liste des enseignants en limitant 
     * par l'entier limit
     */
	@Test
	public void testGetEnseignantsLimit() {
	    	int limite=3;
	    	Session session = HibernateConfiguration.getSessionFactory().openSession();
        List<Enseignant> enseignants= session.createQuery("from Enseignant", Enseignant.class).setMaxResults(limite).list();

        List<Enseignant>enseignantsVerification= new EnseignantDao().getEnseignants(limite);

        assertEquals(enseignants,enseignantsVerification);
        assertTrue(enseignantsVerification.size()<=3);
	        
	    }
	
	 /**
     * Test Obtenir un enseignant a partir de son id
     */
	@Test
	public  void  testGetEnseignant() {
	    	
				//Je crée la session
				Session session = HibernateConfiguration.getSessionFactory().openSession();
				// J'enregiste un enseignant
				Enseignant enseignant= new Enseignant();
				enseignant.setEmail("pokemonGame");
				Transaction transaction=null;
				 transaction = session.beginTransaction();
		         // save the enseignant object
		         session.save(enseignant);
		         // commit transaction
		         transaction.commit();
				
				
				
				// Je verifie que c'est le mm enseignant avec getEnseignant(id)
		 		List<Integer> listIdEnseignant=session.createQuery("Select Max(idEnseignant) from Enseignant", Integer.class).list();
		 		Enseignant enseignantVerif=new EnseignantDao().getEnseignant(listIdEnseignant.get(0));
				assertEquals(enseignant.getEmail(),enseignantVerif.getEmail());
				
				
				
				// Je supprime le dernier enseignant enregistré
				if(listIdEnseignant.get(0)!=null && !listIdEnseignant.isEmpty())
				{
					List<Enseignant> ens=session.createQuery("from Enseignant where idEnseignant = "+listIdEnseignant.get(0), Enseignant.class).list();
					if(!ens.isEmpty()) {
					transaction = null;
					transaction = session.beginTransaction();
					session.delete(ens.get(0));
					transaction.commit();
					}
				}
	}
	
	/**
	 * Obtenir l'enseignant responsable du sujet dont l'id est idSujet
	 */
	@Test
	public void testGetEnseignantSujet()
    {
		//Je crée la session
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		// J'enregiste un enseignant
		Enseignant enseignant= new Enseignant();
		enseignant.setEmail("pokemonGame");
		Transaction transaction=null;
		 transaction = session.beginTransaction();
         // save the enseignant object
         session.save(enseignant);
         // commit transaction
         transaction.commit();
		//Je cree un sujet dont il est responsable
         List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();
 		
 		if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
 		{
 			Sujet sujet=new Sujet();
 			sujet.setIdEnseignement(listIdEnseignement.get(0));
 			transaction = session.beginTransaction();
 	        // save the enseignant object
 	        session.save(sujet);
 	        // commit transaction
 	        transaction.commit();
  	 		List<Integer> listIdEnseignant=session.createQuery("Select Max(idEnseignant) from Enseignant", Integer.class).list();
  	 		List<Integer> listIdSujet=session.createQuery("Select Max(idSujet) from Sujet", Integer.class).list();

 	        Encadrant encadrant= new Encadrant();
 	         
 	        encadrant.setIdEnseignant(listIdEnseignant.get(0));
 	        encadrant.setIdSujet(listIdSujet.get(0));
 	        transaction = session.beginTransaction();
	        // save the enseignant object
	        session.save(encadrant);
	        // commit transaction
	        transaction.commit();
 	        
 			
 			// Je verifie que c'est le mm enseignant avec getEnseignantSujet(id)
 	 		Enseignant enseignantVerif=new EnseignantDao().getEnseignantSujet(listIdSujet.get(0));
 			assertEquals(enseignant.getEmail(),enseignantVerif.getEmail());
 			
 			
 			
 			// Je supprime le dernier enseignant enregistré le dernier sujet enregistré et l'encadrant
 			if(listIdEnseignant.get(0)!=null && !listIdEnseignant.isEmpty())
 			{
 				List<Enseignant> ens=session.createQuery("from Enseignant where idEnseignant = "+listIdEnseignant.get(0), Enseignant.class).list();
 				List<Sujet> suj=session.createQuery("from Sujet where idSujet = "+listIdSujet.get(0), Sujet.class).list();
 				List<Encadrant> enc=session.createQuery("from Encadrant where idSujet = "+listIdSujet.get(0) +" and idEnseignant = "+listIdEnseignant.get(0), Encadrant.class).list();
 				
 				if(!enc.isEmpty())
 				{
 					transaction = null;
 	 				transaction = session.beginTransaction();
 	 				session.delete(enc.get(0));
 	 				transaction.commit();
 				}
 				if(!suj.isEmpty())
 				{
 					transaction = null;
 	 				transaction = session.beginTransaction();
 	 				session.delete(suj.get(0));
 	 				transaction.commit();
 				}
 				if(!ens.isEmpty()) {
 				transaction = null;
 				transaction = session.beginTransaction();
 				session.delete(ens.get(0));
 				transaction.commit();
 				}
 			}
 		}
		
    }
	/**
	 * Supprimer la liste des enseignants
	 */
	@Test
    public void testDeleteEnseignants() {
    		new EnseignantDao().deleteEnseignants();
		Session session=HibernateConfiguration.getSessionFactory().openSession();
	    	List<Enseignant> enseignants= session.createQuery("from Enseignant", Enseignant.class).list();
	    	assertTrue(enseignants.isEmpty());
    
    }

}
