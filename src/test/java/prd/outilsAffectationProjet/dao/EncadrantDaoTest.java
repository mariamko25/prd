package prd.outilsAffectationProjet.dao;

import static org.junit.Assert.*;


import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import prd.outilsAffectationProjet.config.HibernateConfiguration;
import prd.outilsAffectationProjet.model.Encadrant;

/**
 * Class de test pour EncadrantDao
 */
public class EncadrantDaoTest {

	/**
	 * Enregistrer un encadrant dans la base
	 */
	@Test
	public void testEnregistrerEncadrant() {
		/** J'ouvre une session*/
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdEnseignant=session.createQuery("Select Max(idEnseignant) from Enseignant", Integer.class).list();
		List<Integer> listIdSujet=session.createQuery("Select Max(idSujet) from Sujet", Integer.class).list();
		if(listIdEnseignant.get(0)!=null && !listIdEnseignant.isEmpty())
		{
			if(listIdSujet.get(0)!=null && !listIdSujet.isEmpty())
			{
				
				
				List<Encadrant> listMembre=session.createQuery(" from Encadrant where idSujet = "+listIdSujet.get(0)+" and idEnseignant= "+listIdEnseignant.get(0), Encadrant.class).list();
				if(listMembre.isEmpty())
				{
					Encadrant encadrant= new Encadrant();
					encadrant.setIdEnseignant(listIdEnseignant.get(0));
					encadrant.setIdSujet(listIdSujet.get(0));
					new EncadrantDao().enregistrerEncadrant(encadrant);
					listMembre=session.createQuery(" from Encadrant where idSujet = "+listIdSujet.get(0)+" and idEnseignant= "+listIdEnseignant.get(0), Encadrant.class).list();
					assertTrue(listMembre.size()>0);

					if(!listMembre.isEmpty())
					{
						Transaction transaction = null;
						transaction = session.beginTransaction();
						session.delete(listMembre.get(0));
						transaction.commit();
					}
					
					
				}
			}
		}

	}

	/**
	 * Obtenir l'identifiant de l'encadrant du sujet ayant pour id idSujet
	 */
	@Test
	public void testGetIdEncadrant() {
		/** J'ouvre une session*/
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Encadrant> listEncadrant= session.createQuery("from Encadrant order by idEnseignant desc", Encadrant.class)
				.list();
		if(!listEncadrant.isEmpty()) {
			int idVerif= new EncadrantDao().getIdEncadrant(listEncadrant.get(0).getIdSujet());
			assertEquals(listEncadrant.get(0).getIdEnseignant(),idVerif);
		}
	}

	/**
	 * Obtenir la liste des encadrants
	 */
	@Test
	public void testGetEncadrant() {

		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Encadrant> list= session.createQuery("from Encadrant", Encadrant.class).list();
		List<Encadrant> listVerif= new EncadrantDao().getEncadrant();	
		assertEquals(list,listVerif);

	}
	
	/**
	 * Obtenir la liste des encadrants pour un enseignement
	 * ayant pour id idEnseignement
	 */
	@Test
	public void testGetEncadrantEnseignement() {

		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();
		if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
		{
			List<Encadrant>list= session.createQuery("from Encadrant where idSujet in (Select S.idSujet from Sujet S where idEnseignement = "+listIdEnseignement.get(0)+")", Encadrant.class).list();
			List<Encadrant>listVerif= new EncadrantDao().getEncadrantEnseignement(listIdEnseignement.get(0));
			assertEquals(list,listVerif);
		}

	}

	/**
	 * Obtenir encadrant a partir de l'id sujet
	 */
	@Test
	public void testGetEncadrantId() {

		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdSujet=session.createQuery("Select Max(idSujet) from Sujet", Integer.class).list();
		if(listIdSujet.get(0)!=null && !listIdSujet.isEmpty())
		{
			List<Encadrant> encadrants = session.createQuery("from Encadrant where idSujet = " + listIdSujet.get(0), Encadrant.class)
					.list();
			Encadrant encadrantsVerif= new EncadrantDao().getEncadrant(listIdSujet.get(0));
			
			assertEquals(encadrants.get(0),encadrantsVerif);
		}
		

	}

	/**
	 * Obtenir la liste des encadrants en limitant par l'entier limit
	 */
	@Test
	public void testGetEncadrants() {

		int limite=3;
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Encadrant> list= session.createQuery("from Encadrant", Encadrant.class).setMaxResults(limite).list();
		List<Encadrant> listVerif= new EncadrantDao().getEncadrants(limite);	
		assertEquals(list,listVerif);
		assertTrue(listVerif.size()<=3);
	}

	/**
	 * Supprimer la liste des encadrants
	 */
	@Test
	public void testDeleteEncadrants() {
		new EncadrantDao().deleteEncadrants();
		Session session=HibernateConfiguration.getSessionFactory().openSession();
	    	List<Encadrant> encadrants= session.createQuery("from Encadrant", Encadrant.class).list();
	    	assertTrue(encadrants.isEmpty());
	}

}
