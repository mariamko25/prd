package prd.outilsAffectationProjet.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import prd.outilsAffectationProjet.config.HibernateConfiguration;
import prd.outilsAffectationProjet.model.MembreDuGroupe;

/**
 * Class de test pour le MembreDuGroupeDao
 * @author MariamKonate
 */
public class MembreDuGroupeDaoTest {
	/**
	 * Test Enregistrer un membre du groupe dans la base
	 */
	@Test
	public void testEnregistrerMembreDuGroupe() {
		/** J'ouvre une session*/
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdEtudiant=session.createQuery("Select Max(idEtudiant) from Etudiant", Integer.class).list();
		List<Integer> listIdGroupe=session.createQuery("Select Max(idGroupe) from Groupe", Integer.class).list();
		if(listIdGroupe.get(0)!=null && !listIdGroupe.isEmpty())
		{
			if(listIdEtudiant.get(0)!=null && !listIdEtudiant.isEmpty())
			{
				MembreDuGroupe membreDuGroupe= new MembreDuGroupe();
				membreDuGroupe.setIdEtudiant(listIdEtudiant.get(0));
				membreDuGroupe.setIdGroupe(listIdGroupe.get(0));
				
				List<Integer> listMembre=session.createQuery("select idEtudiant from MembreDuGroupe where idGroupe = "+listIdGroupe.get(0), Integer.class).list();
				if(!listMembre.contains(listIdEtudiant.get(0)))
				{
					new MembreDuGroupeDao().enregistrerMembreDuGroupe(membreDuGroupe);
					listMembre=session.createQuery("select idEtudiant from MembreDuGroupe where idGroupe = "+listIdGroupe.get(0), Integer.class).list();
					assertTrue(listMembre.contains(listIdEtudiant.get(0)));
					List<MembreDuGroupe> membres= session.createQuery("from MembreDuGroupe where idGroupe = "+listIdGroupe.get(0)+" and idEtudiant = "+listIdEtudiant.get(0), MembreDuGroupe.class).list();  

					if(!membres.isEmpty())
					{
						Transaction transaction = null;
						transaction = session.beginTransaction();
						session.delete(membres.get(0));
						transaction.commit();
					}
					
					
				}
			}
		}
        
    }
	
	
	/**
	 * Test Obtenir tous les membres de tous les groupes
	 */
	@Test
    public void testGetMembreDesGroupes() {
    	Session session=HibernateConfiguration.getSessionFactory().openSession();
    	List<MembreDuGroupe> membres= session.createQuery("from MembreDuGroupe", MembreDuGroupe.class).list();
    	List<MembreDuGroupe> membresVerif= new MembreDuGroupeDao().getMembreDesGroupes();
    	assertEquals(membres,membresVerif);
    }
    
    /**
	 * Test Obtenir  la taille d'un groupe
	 */
	@Test
    public void testGetNbMembreGroupe() {
	    	Session session=HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdGroupe=session.createQuery("Select Max(idGroupe) from Groupe", Integer.class).list();
		if(listIdGroupe.get(0)!=null && !listIdGroupe.isEmpty())
		{
			List<MembreDuGroupe>membres= session.createQuery("from MembreDuGroupe where idGroupe = "+listIdGroupe.get(0), MembreDuGroupe.class).list();  
		    int size= new MembreDuGroupeDao().getNbMembreGroupe(listIdGroupe.get(0));
		    assertEquals(membres.size(),size);
		}
    
    }
    
    /**
     * Test Etudiants membres d'un groupe
     */
    public void testEtudiantsGroupe()
    {
    		Session session=HibernateConfiguration.getSessionFactory().openSession();
    		List<Integer> listIdGroupe=session.createQuery("Select Max(idGroupe) from Groupe", Integer.class).list();
    		if(listIdGroupe.get(0)!=null && !listIdGroupe.isEmpty())
    		{
        		List<Integer> idEtudiants= session.createQuery("select idEtudiant from MembreDuGroupe where idGroupe = "+listIdGroupe.get(0), Integer.class).list();  
        		List<Integer> idEtudiantsVerif= new MembreDuGroupeDao().etudiantsGroupe(listIdGroupe.get(0));
        		assertEquals(idEtudiants, idEtudiantsVerif);
    		}
    }
    /**
	 * Test Supprimer la liste des MembreDuGroupe
	 */
    @Test
    public void testDeleteMembreDuGroupes() {
    	
    		new MembreDuGroupeDao().deleteMembreDuGroupes();
    		Session session=HibernateConfiguration.getSessionFactory().openSession();
        	List<MembreDuGroupe> membres= session.createQuery("from MembreDuGroupe", MembreDuGroupe.class).list();
        	assertTrue(membres.isEmpty());
    		
    }
    
}
