package prd.outilsAffectationProjet.dao;
import org.junit.*;



import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import prd.outilsAffectationProjet.config.HibernateConfiguration;
import prd.outilsAffectationProjet.model.*;

/**
 * Class de test pour le SouhaitDao
 * @author MariamKonate
 */
public class SouhaitDaoTest {
	
	
	/**
	 * Test de l'enregistrement d'un souhait
	 */
	@Test
	public void testEnregistrerSouhait()
	{
		/** J'ouvre une session*/
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdSujet=session.createQuery("Select Max(idSujet) from Sujet", Integer.class).list();
		List<Integer> listIdGroupe=session.createQuery("Select Max(idGroupe) from Groupe", Integer.class).list();
		
		/** Je crée un souhait*/
		if(listIdSujet.get(0)!=null && !listIdSujet.isEmpty())
		{
			if(listIdGroupe.get(0)!=null && !listIdGroupe.isEmpty())
			{
				Souhait souhait= new Souhait();
				souhait.setIdGroupe(listIdGroupe.get(0));
				souhait.setIdSujet(listIdSujet.get(0));
				souhait.setScore(3);
				/** J'enregistre le souhait*/

				new SouhaitDao().enregistrerSouhait(souhait);
				/** Je verifie que le souhait que j'ai créé est bien celui qui est enregistré*/
				List<Souhait> listSouhait=session.createQuery("from Souhait where idSujet = " + listIdSujet.get(0)+" and idGroupe = "+listIdGroupe.get(0), Souhait.class).list();
				
				Souhait souhaitVerification=listSouhait.get(0);
				assertEquals(souhaitVerification.getScore(),3);
				
				/** Je supprime le souhait inséré*/
				Transaction transaction = null;
				transaction = session.beginTransaction();
				session.delete(listSouhait.get(0));
				transaction.commit();	
			}
			
		}
		
	}
	
	
	/**
	 * Test de l'enregistrement d'un souhait
	 */
	@Test
	public void testEnregistrerSouhaits()
	{
		/** J'ouvre une session*/
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdSujet=session.createQuery("Select Max(idSujet) from Sujet", Integer.class).list();
		List<Integer> listIdGroupe=session.createQuery("Select Max(idGroupe) from Groupe", Integer.class).list();
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();

		/** Je crée un souhait*/
		if(listIdSujet.get(0)!=null && !listIdSujet.isEmpty())
		{
			if(listIdGroupe.get(0)!=null && !listIdGroupe.isEmpty())
			{
				if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
				{
					Souhait souhait= new Souhait();
					souhait.setIdGroupe(listIdGroupe.get(0));
					souhait.setIdSujet(listIdSujet.get(0));
					souhait.setScore(3);
					List<Souhait> souhaits= new ArrayList<Souhait>();
					souhaits.add(souhait);
					/** J'enregistre le souhait*/

					new SouhaitDao().enregistrerSouhaits(souhaits,listIdEnseignement.get(0));
					/** Je verifie que le souhait que j'ai créé est bien celui qui est enregistré*/
					List<Souhait> listSouhait=session.createQuery("from Souhait where idSujet = " + listIdSujet.get(0)+" and idGroupe = "+listIdGroupe.get(0), Souhait.class).list();
					
					Souhait souhaitVerification=listSouhait.get(0);
					assertEquals(souhaitVerification.getScore(),3);
					
					/** Je supprime le souhait inséré*/
					Transaction transaction = null;
					transaction = session.beginTransaction();
					session.delete(listSouhait.get(0));
					transaction.commit();	
				}
				
			}
			
		}
	}
	
	/**
	 * Test de l'obtention de tous les  souhaits
	 */
	@Test
	public void testGetSouhaits() {
		/** On obtient la liste des souhaits*/
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Souhait> listeInitiale= session.createQuery("from Souhait", Souhait.class).list();
		
		/** On teste notre methode getSouhaits */
		List<Souhait> listeVerification= new SouhaitDao().getSouhaits();
		assertEquals(listeInitiale,listeVerification);
			
	}
	
	/**
	 * Test de l'obtention d'un souhait pour un enseignement
	 */
	@Test
	public void testGetSouhaitsEnseignement() {
		/** On obtient la liste des souhaits*/
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();
		if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
		{
			List<Souhait> listeInitiale= session.createQuery("from Souhait where idSujet in(select idSujet from Sujet where idEnseignement = "+listIdEnseignement.get(0)+")", Souhait.class).list();        

			
			/** On teste notre methode getSouhaits */
			List<Souhait> listeVerification= new SouhaitDao().getSouhaits(listIdEnseignement.get(0));
			assertEquals(listeInitiale,listeVerification);
		}
		
		
			
	}
	
	/**
	 * Test Obtenir la liste des groupes des souhaits
	 * pour un enseignement
	 */
	@Test
	public void testGetSouhaitsGroupe() {
		
		/** On obtient la liste des souhaits*/
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();
		if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
		{
			List<Integer> listeInitiale= session.createQuery("select distinct idGroupe from Souhait where idSujet in(select idSujet from Sujet where idEnseignement = "+listIdEnseignement.get(0)+")", Integer.class).list();        

			
			/** On teste notre methode getSouhaitsGroupe */
			List<Integer> listeVerification= new SouhaitDao().getSouhaitsGroupe(listIdEnseignement.get(0));
			assertEquals(listeInitiale,listeVerification);
		}
	}
	
	/**
	 * Test Obtenir la liste des sujets des souhaits
	 * pour un enseignement
	 */
	@Test
	public void testGetSouhaitsSujet() {
		
		/** On obtient la session*/
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();
		if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
		{
			List<Integer> listeInitiale= session.createQuery("select distinct idSujet from Souhait where idSujet in(select idSujet from Sujet where idEnseignement = "+listIdEnseignement.get(0)+")", Integer.class).list();        			
			/** On teste notre methode getSouhaits */
			List<Integer> listeVerification= new SouhaitDao().getSouhaitsSujet(listIdEnseignement.get(0));
			assertEquals(listeInitiale,listeVerification);
		}
	}
	 /**
		 * Test Obtenir la liste des souhaits d'un groupe 
		 * pour un enseignement
		 */
	@Test
	public void testgetSouhaitsEnseignementGroupe() {
		
		/** On obtient la session*/
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();
		List<Integer> listIdGroupe=session.createQuery("Select Max(idGroupe) from Groupe", Integer.class).list();
		if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
		{
			if(listIdGroupe.get(0)!=null && !listIdGroupe.isEmpty())
			{
		    		List<Souhait>listeInitiale= session.createQuery("from Souhait where idSujet in(select idSujet from Sujet where idEnseignement = "+listIdEnseignement.get(0)+") and idGroupe = "+listIdGroupe.get(0), Souhait.class).list();        
		    		/** On teste notre methode getSouhaits */
				List<Souhait> listeVerification= new SouhaitDao().getSouhaits(listIdEnseignement.get(0),listIdGroupe.get(0));
				assertEquals(listeInitiale,listeVerification);
			}
		}
		
		
	}
	
	/**
	 * Test Supprimer la liste des souhaits
	 * 
	 */
	@Test
	public void testDeleteSouhaits() {
		
		new SouhaitDao().deleteSouhaits();
		/** On obtient la session*/
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Souhait> souhaits = session.createQuery("from Souhait", Souhait.class).list();
		assertTrue(souhaits.isEmpty());
		
		
	}

}
