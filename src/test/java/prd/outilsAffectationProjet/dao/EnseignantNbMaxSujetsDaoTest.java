package prd.outilsAffectationProjet.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import prd.outilsAffectationProjet.config.HibernateConfiguration;
import prd.outilsAffectationProjet.model.EnseignantNbMaxSujets;

/**
 * Class de test pour EnseignantNbMaxSujetsDao
 * @author MariamKonate
 */
public class EnseignantNbMaxSujetsDaoTest {
	
	/**
	 * Enregistrer un EnseignantNbMaxSujets dans la base
	 */
	@Test
	public void testEnregistrerEnseignantNbMaxSujets() {
		
		
		//Je crée la session
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		
		// J'enregiste un Enseignement
		List<Integer> listIdEnseignant=session.createQuery("Select Max(idEnseignant) from Enseignant", Integer.class).list();
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();

		if(listIdEnseignant.get(0)!=null && !listIdEnseignant.isEmpty())
		{
			if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
			{
				List<EnseignantNbMaxSujets> ensNbMaxList=session.createQuery("from EnseignantNbMaxSujets", EnseignantNbMaxSujets.class).list();
				boolean exists=false;
				for(EnseignantNbMaxSujets max:ensNbMaxList)
				{
					if(max.getIdEnseignant()==listIdEnseignant.get(0) && max.getIdEnseignement()==listIdEnseignant.get(0)) {
						exists=true;
					}
				}
				if(!exists)
				{
					// J'enregiste un enseignantNbMaxSujets
					EnseignantNbMaxSujets enbmSujet= new EnseignantNbMaxSujets();
					enbmSujet.setIdEnseignant(listIdEnseignant.get(0));
					enbmSujet.setIdEnseignement(listIdEnseignement.get(0));
					enbmSujet.setNombreSujetMin(0);
					enbmSujet.setNombreSujetMax(1);
					
					new EnseignantNbMaxSujetsDao().enregistrerEnseignantNbMaxSujets(enbmSujet);

					// Je verifie qu'il a été enregistré
					ensNbMaxList=session.createQuery("from EnseignantNbMaxSujets", EnseignantNbMaxSujets.class).list();
					for(EnseignantNbMaxSujets max:ensNbMaxList)
					{
						if(max.getIdEnseignant()==listIdEnseignant.get(0) && max.getIdEnseignement()==listIdEnseignant.get(0)) {
							exists=true;
						}
					}
					assertTrue(exists);
					
					List<EnseignantNbMaxSujets> enseignements=session.createQuery("from EnseignantNbMaxSujets where idEnseignement = "+listIdEnseignement.get(0)+" and idEnseignant = "+listIdEnseignant.get(0), EnseignantNbMaxSujets.class).list();
					if(!enseignements.isEmpty()) {
					Transaction transaction = null;
					transaction = session.beginTransaction();
					session.delete(enseignements.get(0));
					transaction.commit();
					}
					
				}
				
			}
			
		
		}
			
				

		
			

	}
	
	
	/**
	 * Obtenir EnseignantNbMaxSujets a partir de l'id enseignant 
	 * et l'id enseignement
	 */
	@Test
	public void testGetEnseignantNbMaxSujets() {

				//Je crée la session
				Session session = HibernateConfiguration.getSessionFactory().openSession();
				// J'enregiste un Enseignement
				List<Integer> listIdEnseignant=session.createQuery("Select Max(idEnseignant) from Enseignant", Integer.class).list();
				List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();

				if(listIdEnseignant.get(0)!=null && !listIdEnseignant.isEmpty())
				{
					if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
					{
						List<EnseignantNbMaxSujets> ensNbMaxList=session.createQuery("from EnseignantNbMaxSujets", EnseignantNbMaxSujets.class).list();
						boolean exists=false;
						for(EnseignantNbMaxSujets max:ensNbMaxList)
						{
							if(max.getIdEnseignant()==listIdEnseignant.get(0) && max.getIdEnseignement()==listIdEnseignant.get(0)) {
								exists=true;
							}
						}
						if(!exists)
						{
							// J'enregiste un enseignantNbMaxSujets
							EnseignantNbMaxSujets enbmSujet= new EnseignantNbMaxSujets();
							enbmSujet.setIdEnseignant(listIdEnseignant.get(0));
							enbmSujet.setIdEnseignement(listIdEnseignement.get(0));
							enbmSujet.setNombreSujetMin(0);
							enbmSujet.setNombreSujetMax(1);
							
							Transaction transaction = null;
							// start a transaction
							transaction = session.beginTransaction();
							// save the enseignant object
							session.save(enbmSujet);
							// commit transaction
							transaction.commit();
							
							// Je test ma methode get
							EnseignantNbMaxSujets test=new EnseignantNbMaxSujetsDao().getEnseignantNbMaxSujets(listIdEnseignant.get(0),listIdEnseignement.get(0));
							assertNotNull(test);
							
							List<EnseignantNbMaxSujets> enseignements=session.createQuery("from EnseignantNbMaxSujets where idEnseignement = "+listIdEnseignement.get(0)+" and idEnseignant = "+listIdEnseignant.get(0), EnseignantNbMaxSujets.class).list();
							if(!enseignements.isEmpty()) {
							transaction = null;
							transaction = session.beginTransaction();
							session.delete(enseignements.get(0));
							transaction.commit();
							}
							
						}
						
					}
				}

	}
	
	/**
	 * Obtenir EnseignantNbMaxSujets a partir de l'id enseignement
	 */
	@Test
	public void testGetEnseignantNbMaxSujets2() {

		//Je crée la session
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		// J'enregiste un Enseignement
		List<Integer> listIdEnseignant=session.createQuery("Select Max(idEnseignant) from Enseignant", Integer.class).list();
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();

		if(listIdEnseignant.get(0)!=null && !listIdEnseignant.isEmpty())
		{
			if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
			{
				List<EnseignantNbMaxSujets> ensNbMaxList=session.createQuery("from EnseignantNbMaxSujets", EnseignantNbMaxSujets.class).list();
				boolean exists=false;
				for(EnseignantNbMaxSujets max:ensNbMaxList)
				{
					if(max.getIdEnseignant()==listIdEnseignant.get(0) && max.getIdEnseignement()==listIdEnseignant.get(0)) {
						exists=true;
					}
				}
				if(!exists)
				{
					// J'enregiste un enseignantNbMaxSujets
					EnseignantNbMaxSujets enbmSujet= new EnseignantNbMaxSujets();
					enbmSujet.setIdEnseignant(listIdEnseignant.get(0));
					enbmSujet.setIdEnseignement(listIdEnseignement.get(0));
					enbmSujet.setNombreSujetMin(0);
					enbmSujet.setNombreSujetMax(1);
					
					Transaction transaction = null;
					// start a transaction
					transaction = session.beginTransaction();
					// save the enseignant object
					session.save(enbmSujet);
					// commit transaction
					transaction.commit();
					
					// Je test ma methode get
					List<EnseignantNbMaxSujets> test=new EnseignantNbMaxSujetsDao().getEnseignantNbMaxSujets(listIdEnseignement.get(0));
					assertNotNull(test.get(0));
					
					List<EnseignantNbMaxSujets> enseignements=session.createQuery("from EnseignantNbMaxSujets where idEnseignement = "+listIdEnseignement.get(0), EnseignantNbMaxSujets.class).list();
					if(!enseignements.isEmpty()) {
					transaction = null;
					transaction = session.beginTransaction();
					session.delete(enseignements.get(0));
					transaction.commit();
					}
					
				}
				
			}
		}

	}
	
	/**
	 * mette à jour un EnseignantNbMaxSujets
	 */
	@Test
	public void testUpdateEnseignantNbMaxSujets() {
		//Je crée la session
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		// J'enregiste un Enseignement
		List<Integer> listIdEnseignant=session.createQuery("Select Max(idEnseignant) from Enseignant", Integer.class).list();
		List<Integer> listIdEnseignement=session.createQuery("Select Max(idEnseignement) from Enseignement", Integer.class).list();

		if(listIdEnseignant.get(0)!=null && !listIdEnseignant.isEmpty())
		{
			if(listIdEnseignement.get(0)!=null && !listIdEnseignement.isEmpty())
			{
				List<EnseignantNbMaxSujets> ensNbMaxList=session.createQuery("from EnseignantNbMaxSujets", EnseignantNbMaxSujets.class).list();
				boolean exists=false;
				for(EnseignantNbMaxSujets max:ensNbMaxList)
				{
					if(max.getIdEnseignant()==listIdEnseignant.get(0) && max.getIdEnseignement()==listIdEnseignant.get(0)) {
						exists=true;
					}
				}
				if(!exists)
				{
					// J'enregiste un enseignantNbMaxSujets
					EnseignantNbMaxSujets enbmSujet= new EnseignantNbMaxSujets();
					enbmSujet.setIdEnseignant(listIdEnseignant.get(0));
					enbmSujet.setIdEnseignement(listIdEnseignement.get(0));
					enbmSujet.setNombreSujetMin(0);
					enbmSujet.setNombreSujetMax(1);
					
					Transaction transaction = null;
					// start a transaction
					transaction = session.beginTransaction();
					// save the enseignant object
					session.save(enbmSujet);
					// commit transaction
					transaction.commit();
					
					// Je test ma methode update
					enbmSujet.setNombreSujetMax(10001);
					new EnseignantNbMaxSujetsDao().updateEnseignantNbMaxSujets(enbmSujet);
					
					
					List<EnseignantNbMaxSujets> enseignements=session.createQuery("from EnseignantNbMaxSujets where idEnseignement = "+listIdEnseignement.get(0)+" and idEnseignant = "+listIdEnseignant.get(0), EnseignantNbMaxSujets.class).list();
					EnseignantNbMaxSujets test=enseignements.get(0);
					assertEquals(test.getNombreSujetMax(),10001);
					
					if(!enseignements.isEmpty()) {
					transaction = null;
					transaction = session.beginTransaction();
					session.delete(enseignements.get(0));
					transaction.commit();
					}
					
				}
				
			}
		}
	}


}
