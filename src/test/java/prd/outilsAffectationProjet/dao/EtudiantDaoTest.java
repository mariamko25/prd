package prd.outilsAffectationProjet.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import prd.outilsAffectationProjet.config.HibernateConfiguration;
import prd.outilsAffectationProjet.model.Etudiant;

/**
 * Class de test pour  EtudiantDao
 * @author MariamKonate
 */
public class EtudiantDaoTest {
	
	/**
	 * Test Enregistrer etudiant dans la base
	 */
	@Test
	public void testEnregistrerEtudiant() {
		//Je crée la session
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		// J'obtient le nombre d'etudiants actuel
		int size= session.createQuery("from Etudiant", Etudiant.class).list().size();
		// J'enregiste un etudiant
		Etudiant etudiant= new Etudiant();
		new EtudiantDao().enregistrerEtudiant(etudiant);
		// Je verifie que la liste des etudiant a augmenté
		int sizeVerification=session.createQuery("from Etudiant", Etudiant.class).list().size();				
		assertTrue(size<sizeVerification);
		// Je supprime le dernier etudiant enregistré
		List<Integer> listIdEtudiant=session.createQuery("Select Max(idEtudiant) from Etudiant", Integer.class).list();
		if(listIdEtudiant.get(0)!=null && !listIdEtudiant.isEmpty())
		{
			List<Etudiant> etudiants=session.createQuery("from Etudiant where idEtudiant = "+listIdEtudiant.get(0), Etudiant.class).list();
			if(!etudiants.isEmpty()) {
			Transaction transaction = null;
			transaction = session.beginTransaction();
			session.delete(etudiants.get(0));
			transaction.commit();
			}
		}
        
    }
	
	/**
	 * Test Obtenir etudiant a partir de son adresse mail
	 */
	@Test
	public void testGetEtudiantMail()
	{
		//Je crée la session
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		
		// J'enregiste un etudiant
		Etudiant etudiant= new Etudiant();
		etudiant.setEmail("testtesttest");
		/** Enregistrement du sujet*/
		Transaction transaction = null;
		// start a transaction
		transaction = session.beginTransaction();
		// save the sujet object
		session.save(etudiant);
		// commit transaction
		transaction.commit();		//Je verifie que c'est le meme etudiant
		Etudiant verification= new EtudiantDao().getEtudiant(etudiant.getEmail());
		assertEquals(etudiant.getEmail(),verification.getEmail());
		List<Integer> listIdEtudiant=session.createQuery("Select Max(idEtudiant) from Etudiant", Integer.class).list();
		// Je supprime l'etudiant inséré
		verification=session.load(Etudiant.class, listIdEtudiant.get(0));
		transaction = null;
		transaction = session.beginTransaction();
		session.delete(verification);
		transaction.commit();
	}
	
	/**
	 * Test Obtenir la liste des etudiants
	 */
	@Test
    public void testGetStudents() {
    	
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Etudiant> etudiants= session.createQuery("from Etudiant", Etudiant.class).list();
		
		List<Etudiant> etudiantsVerification= new EtudiantDao().getStudents();
		
		assertEquals(etudiants,etudiantsVerification);
        
    }
	
    /**
   	 * Obtenir la liste des etudiants
   	 */
	@Test
    public void  testGetEtudiants() {
   	
	    Session session = HibernateConfiguration.getSessionFactory().openSession();
	    List<Etudiant> etudiants= session.createQuery("from Etudiant", Etudiant.class).list();
	
	    List<Etudiant> etudiantsVerification= new EtudiantDao().getEtudiants();
	
	    assertEquals(etudiants,etudiantsVerification);
       
    }
	
    /**
     * Test Obtenir la liste des etudiants limitée par limit
     */
	@Test
    public void testGetStudentsLimit() {
    	
    		int limit=3;
    		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Etudiant> etudiants= session.createQuery("from Etudiant order by idEtudiant desc", Etudiant.class).setMaxResults(limit).list();
		
		List<Etudiant> etudiantsVerification= new EtudiantDao().getStudents(limit);
		
		assertEquals(etudiants,etudiantsVerification);
		assertTrue(etudiants.size()<=limit);
        
    }
    
    /**
	 * Test Supprimer la liste des etudiants
	 */
	@Test
    public void testDeleteEtudiants() {
	    	new EtudiantDao().deleteEtudiants();
		Session session=HibernateConfiguration.getSessionFactory().openSession();
	    	List<Etudiant> etudiants= session.createQuery("from Etudiant", Etudiant.class).list();
	    	assertTrue(etudiants.isEmpty());
    }

	/**
	 * Test Obtenir etudiant a partir de son idEtudiant
	 */
	@Test
	public void testGetEtudiantId()
	{
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> listIdEtudiant=session.createQuery("Select Max(idEtudiant) from Etudiant", Integer.class).list();

		if(listIdEtudiant.get(0)!=null && !listIdEtudiant.isEmpty())
		{
			List<Etudiant> etudiants=session.createQuery("from Etudiant where idEtudiant = "+listIdEtudiant.get(0), Etudiant.class).list();
			assertNotNull(etudiants.get(0));
		}
	}
}
