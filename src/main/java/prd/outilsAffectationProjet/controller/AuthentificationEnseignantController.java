package prd.outilsAffectationProjet.controller;


import java.io.IOException;
import java.util.Hashtable;


import javax.naming.AuthenticationException;
import javax.naming.CommunicationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.*;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.HtmlUtils;

import prd.outilsAffectationProjet.dao.EnseignantDao;
import prd.outilsAffectationProjet.model.Enseignant;
/**
 * Controller pour la vue authentification enseignant 
 * Elle met en place tout ce qui connection de l'enseignant 
 * @author MariamKonate
 *
 */
@Controller
public class AuthentificationEnseignantController {
	
	
    		
	/**
	 * Méthode pour afficher la page de connexion enseignant
	 * @param request requete http
	 * @return vue connexionEnseignant
	 * @throws IOException une exception
	 */	
	@RequestMapping("/authentificationEnseignant")
	public ModelAndView authentificationEnseignant(HttpServletRequest request) throws IOException {
		if(request.getParameter("email")==null && request.getParameter("motDePasse")==null && request.getSession().getAttribute("enseignantConnecte")==null)
		{
			ModelAndView view= new ModelAndView("connexionEnseignant");
			return view;
		}
		else
		{
			if(request.getSession().getAttribute("enseignantConnecte")!=null && request.getSession().getAttribute("enseignantConnecte").equals(true)) {
				return new EnseignantController().enseignant(request);
			}
			else if(authentification(request.getParameter("email"),request.getParameter("motDePasse")))
			{
				Enseignant ens=new EnseignantDao().getEnseignant( HtmlUtils.htmlEscape(request.getParameter("email").trim()));
				if(ens==null) {
					ens=new Enseignant();
					ens.setEmail( HtmlUtils.htmlEscape(request.getParameter("email").trim()));
					new EnseignantDao().enregistrerEnseignant(ens);
				}
				return new EnseignantController().enseignant(request).addObject("success", true);
			}
			else
			{
				ModelAndView view= new ModelAndView("connexionEnseignant");
				return view.addObject("failure", true);
			}
		}
		
	}
	
	/**
	 * Méthode pour s'authentifier avec l'active directory
	 * @param email l'email de l'enseignant
	 * @param motDePasse mot de passe de l'enseignant
	 * @return vrai ou faux
	 */
	public boolean authentification(String email, String motDePasse)
	{
		if(email.equals("ens1") && motDePasse.equals("Pop2212!"))
		{
			return true;
		}
		else if(email.equals("ens2") && motDePasse.equals("Pop2212!"))
		{
			return true;
		}
		else if(email.equals("ens1@univ-tours.fr") && motDePasse.equals("Pop2212!"))
		{
			return true;
		}
		else if(email.equals("ens2@univ-tours.fr") && motDePasse.equals("Pop2212!"))
		{
			return true;
		}
		return false;
		
	}
}
	

