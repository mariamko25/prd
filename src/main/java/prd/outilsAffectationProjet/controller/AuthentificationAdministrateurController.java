package prd.outilsAffectationProjet.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.HtmlUtils;

import prd.outilsAffectationProjet.dao.EnseignantDao;
import prd.outilsAffectationProjet.model.Enseignant;

/**
 * Controller pour la vue authentification administrateur 
 * Elle met en place tout ce qui connection de l'administrateur 
 * @author MariamKonate
 *
 */
@Controller
public class AuthentificationAdministrateurController {
	/**
	 * Méthode pour afficher la page de connexion administrateur
	 * @param request requete http
	 * @return vue connexionAdministrateur
	 * @throws IOException une exception
	 */	
	@RequestMapping("/authentificationAdministrateur")
	public ModelAndView authentificationAdministrateur(HttpServletRequest request) throws IOException {
		if(request.getParameter("email")==null && request.getParameter("motDePasse")==null && request.getSession().getAttribute("administrateurConnecte")==null)
		{
			ModelAndView view= new ModelAndView("connexionAdministrateur");
			return view;
		}
		else
		{
			if(request.getSession().getAttribute("administrateurConnecte")!=null && request.getSession().getAttribute("administrateurConnecte").equals(true)) {
				return new AdministrateurController().administrateur(request);
			}
			else if(authentification(request.getParameter("email"),request.getParameter("motDePasse")))
			{
				Enseignant ens=new EnseignantDao().getEnseignant( HtmlUtils.htmlEscape(request.getParameter("email").trim()));
				if(ens==null) {
					ens=new Enseignant();
					ens.setEmail( HtmlUtils.htmlEscape(request.getParameter("email").trim()));
					new EnseignantDao().enregistrerEnseignant(ens);
				}
				return new AdministrateurController().administrateur(request).addObject("success", true);
			}
			else
			{
				ModelAndView view= new ModelAndView("connexionAdministrateur");
				return view.addObject("failure", true);
			}
		}
		
	}
	
	/**
	 * Méthode pour s'authentifier avec l'active directory
	 * @param email l'adresseMail de l'enseignant
	 * @param motDePasse mot de passe de l'enseignant
	 * @return  vrai ou faux
	 */
	public boolean authentification(String email, String motDePasse)
	{
		if(email.equals("ens1") && motDePasse.equals("Pop2212!"))
		{
			return true;
		}
		else if(email.equals("ens2") && motDePasse.equals("Pop2212!"))
		{
			return true;
		}
		else if(email.equals("ens1@univ-tours.fr") && motDePasse.equals("Pop2212!"))
		{
			return true;
		}
		else if(email.equals("ens2@univ-tours.fr") && motDePasse.equals("Pop2212!"))
		{
			return true;
		}
		return false;
		
	}
}
