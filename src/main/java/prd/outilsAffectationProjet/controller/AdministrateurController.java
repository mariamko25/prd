package prd.outilsAffectationProjet.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.HtmlUtils;

import prd.outilsAffectationProjet.dao.*;
import prd.outilsAffectationProjet.model.*;

/**
 * Controller pour la vue administrateur 
 * Elle met en place tout ce qui concerne l'admin c'est à dire tout
 * modifier et supprimer tout
 * avoir la main sur l'affichage ou non du formulaire etc..
 * @author MariamKonate
 *
 */
@Controller
public class AdministrateurController {
	
	/**
	 * Deconnexion de la page  pour l'administrateur
	 * @param request la requete http
	 * @return la vue accueil
	 * @throws IOException une exception
	 */
	@RequestMapping("/administrateurDeconnexion")
	public ModelAndView administrateurDeconnexion(HttpServletRequest request) throws IOException {
		
		
		request.getSession().setAttribute("administrateurConnecte", null); 
		request.getSession().setAttribute("email", null); 

		
		return new AccueilController().acceuil(request);
	}
	
	
	
	/**
	 * Methode qui montre la page administrateur
	 * @param request la requete http
	 * @return vue administrateur
	 * @throws IOException une exception
	 */
	@RequestMapping("/administrateur")
	public ModelAndView administrateur(HttpServletRequest request) throws IOException {

		String email = "";
		if (request.getParameter("email") == null) {
			email = "";
		} else {
			email = HtmlUtils.htmlEscape(request.getParameter("email").trim());

		}

		if (request.getSession().getAttribute("email") != null) {

			email =  HtmlUtils.htmlEscape(((String) request.getSession().getAttribute("email")).trim());
		}
		request.getSession().setAttribute("administrateurConnecte", true); 
		/** On envoie la liste des enseignements a la page admin*/
		List<Enseignement> enseignements = new EnseignementDao().getEnseignements();
		ModelAndView view = new ModelAndView("administrateur");
		view.addObject("enseignements", enseignements);
		return view;
	}

	/**
	 * Methode qui permet a l'admin de modifier un enseignement
	 * @param request la requete http 
	 * @return vue modification enseignement
	 * @throws IOException une exception
	 */
	@RequestMapping("/adminModifierEnseignement")
	public ModelAndView adminModifierEnseignement(HttpServletRequest request) throws IOException {
		/**
		 * Si l'on accede a la page pour la premiere fois
		 */
		if (request.getParameter("enregistrer") == null && request.getParameter("supprimer") == null) {
			ModelAndView view = new ModelAndView("adminModifierEnseignement");
			/**
			 * On recupere l'identfiant 
			 */
			int idEnseignement = 0;
			if (request.getParameter("ouvrir") != null) {
				String str = request.getParameter("idEnseignement");
				idEnseignement = Integer.valueOf(str);

				/** On enregistre l'identifiant de l'enseignement en session **/
				request.getSession().setAttribute("idEnseignement", idEnseignement);
			} else {
				/** On recupere l'identifiant de l'enseignement depuis la session **/
				idEnseignement = (Integer) request.getSession().getAttribute("idEnseignement");
			}
			EnseignementDao enseignementDao = new EnseignementDao();
			Enseignement enseignement = enseignementDao.getEnseignement(idEnseignement);
			/**
			 * On renvoie les informations de l'enseignement a la vue
			 */
			view.addObject("enseignement", enseignement);
			return view;
		} else if (request.getParameter("enregistrer") != null) {
			/**
			 * Si les informations sont indiquées on retourne sur la page enseignant
			 */
			ModelAndView view = new ModelAndView("administrateur");
			/**
			 * On recupere l'enseignement en sesssion
			 */
			int idEnseignement = (Integer) request.getSession().getAttribute("idEnseignement");
			EnseignementDao enseignementDao = new EnseignementDao();
			Enseignement enseignementA = enseignementDao.getEnseignement(idEnseignement);

			/**
			 * On le modifie
			 */
			enseignementA.setCle(HtmlUtils.htmlEscape(request.getParameter("cle").trim()));
			enseignementA.setDescription(HtmlUtils.htmlEscape(request.getParameter("description").trim()));
			enseignementA.setFormulaire(Integer.valueOf(request.getParameter("formulaire")));
			enseignementA.setNbMaxScore(Integer.valueOf(request.getParameter("nbMaxScore")));
			enseignementA.setNbMaxTotal(Integer.valueOf(request.getParameter("nbMaxTotal")));
			enseignementA.setNom(HtmlUtils.htmlEscape(request.getParameter("nom").trim()));
			enseignementA.setTypeOptimisation(HtmlUtils.htmlEscape(request.getParameter("typeOptimisation").trim()));
			enseignementDao.updateEnseignement(idEnseignement, enseignementA);
			String email = "";
			if (request.getSession().getAttribute("email") != null) {

				email = HtmlUtils.htmlEscape(((String) request.getSession().getAttribute("email")).trim());
			}

			/** Enregistrement de la session de l'utilisateur */
			HttpSession session = request.getSession();
			session.setAttribute("email", email);

			/** Obtenir la liste de ses enseignements */
			EnseignantDao enseignantDao = new EnseignantDao();
			int idEnseignant = enseignantDao.getIdEnseignant(email);
			List<Enseignement> enseignements = enseignementDao.getEnseignements();

			/** Envoyer cette liste à la vue */
			view.addObject("enseignements", enseignements);
			view.addObject("idEnseignant", idEnseignant);
			return view;
		}
		else
		{
			/**
			 * Si les informations sont indiquées on retourne sur la page enseignant
			 */
			List<Enseignement> enseignements = new EnseignementDao().getEnseignements();
			ModelAndView view = new ModelAndView("administrateur");
			view.addObject("enseignements", enseignements);
			/**
			 * On recupere l'enseignement en sesssion
			 */
			int idEnseignement = (Integer) request.getSession().getAttribute("idEnseignement");
			new EnseignementDao().supprimerEnseignement(idEnseignement);
			return view;
			
		}

	}
	/**
	 * Page qui permet a l'admin de rajouter un enseignement
	 * @param request requete http
	 * @return vue ajout Enseignement
	 * @throws IOException une exception 
	 */
	@RequestMapping("/ajoutEnseignement")
	public ModelAndView ajoutEnseignement(HttpServletRequest request) throws IOException {
		
		/** S'il atterit sur la page pour la premiere fois */
		if(request.getParameter("enregistrer")==null)
		{
			/** On envoie la liste des enseignants a la vue */
			List<Enseignant> enseignants= new EnseignantDao().getEnseignants();
			return new ModelAndView("ajoutEnseignement").addObject("enseignants", enseignants);

		}
		else
		{
			/** S'il souhaite enregistrer on recupere les informations */
			String encadrant=HtmlUtils.htmlEscape(request.getParameter("encadrant").trim());
			int idEnseignant=Integer.valueOf(encadrant.split("-")[1].trim());
			Enseignement enseignementA= new Enseignement();
		    enseignementA.setCle(HtmlUtils.htmlEscape(request.getParameter("cle").trim()));
			enseignementA.setDescription(HtmlUtils.htmlEscape(request.getParameter("description").trim()));
			enseignementA.setFormulaire(Integer.valueOf(request.getParameter("formulaire")));
			enseignementA.setNbMaxScore(Integer.valueOf(request.getParameter("nbMaxScore")));
			enseignementA.setNbMaxTotal(Integer.valueOf(request.getParameter("nbMaxTotal")));
			enseignementA.setNom(HtmlUtils.htmlEscape(request.getParameter("nom").trim()));
			enseignementA.setTypeOptimisation(HtmlUtils.htmlEscape(request.getParameter("typeOptimisation").trim()));
			enseignementA.setIdEnseignant(idEnseignant);
			enseignementA.setAnnee(Calendar.getInstance().get(Calendar.YEAR));
			new EnseignementDao().enregistrerEnseignement(enseignementA);
			return administrateur(request);
		}
	}
	
	/**
	 * Methode permettant a l'admin de rajouter un enseignant
	 * @param request requete http 
	 * @return vue ajout Enseignant
	 * @throws IOException une exception
	 */
	@RequestMapping("/ajoutEnseignant")
	public ModelAndView ajoutEnseignant(HttpServletRequest request) throws IOException {
		
		/** Apres l'ajout on renvoie la page gestionTables*/
		ModelAndView view=new ModelAndView("gestionTables");
		Enseignant enseignant=new Enseignant();
		enseignant.setNom(HtmlUtils.htmlEscape(request.getParameter("nom").trim()));
		enseignant.setPrenom(HtmlUtils.htmlEscape(request.getParameter("prenom").trim()));
		enseignant.setEmail(HtmlUtils.htmlEscape(request.getParameter("email").trim()));
		Date sf;
		try {
			sf = new SimpleDateFormat("dd/MM/yyyy").parse(request.getParameter("dateNaissance"));
			enseignant.setDateNaissance(sf);
			new EnseignantDao().enregistrerEnseignant(enseignant);
			 view.addObject("success", true);
        	     view.addObject("failure", false);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			 view.addObject("success", false);
        		view.addObject("failure", true);
			e.printStackTrace();
		}
		
		return view;
		
	}
	
	/**
	 * Methode permettant a l'admin de rajouter un souhait
	 * @param request requete http
	 * @return vue ajout Souhait
	 * @throws IOException une exception
	 */
	@RequestMapping("/ajoutSouhait")
	public ModelAndView ajoutSouhait(HttpServletRequest request) throws IOException {
		
		/** Apres l'ajout on renvoie la page gestionTables */
		ModelAndView view=new ModelAndView("gestionTables");
		Souhait souhait=new Souhait();
		
		try {
			 souhait.setIdGroupe(Integer.valueOf(request.getParameter("groupe")));
			 int idSujet=Integer.valueOf(request.getParameter("sujet").split("-")[1].trim());
			 souhait.setIdSujet(idSujet);
			 souhait.setScore(Integer.valueOf(request.getParameter("score")));
			 new SouhaitDao().enregistrerSouhait(souhait);
			 view.addObject("success", true);
        	     view.addObject("failure", false);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			 view.addObject("success", false);
        		view.addObject("failure", true);
			e.printStackTrace();
		}
		
		return view;
	}
	
	/**
	 * Methode permettant a l'admin de rajouter une affectation
	 * @param request requete http 
	 * @return vue ajout Affectation
	 * @throws IOException une exception
	 */
	@RequestMapping("/ajoutAffectation")
	public ModelAndView ajoutAffectation(HttpServletRequest request) throws IOException {
		/** Après l'ajout on renvoie la page gestionTables*/
		ModelAndView view=new ModelAndView("gestionTables");
		Affectation affectation=new Affectation();
		
		try {
			 affectation.setIdGroupe(Integer.valueOf(request.getParameter("groupe")));
			 int idSujet=Integer.valueOf(request.getParameter("sujet").split("-")[1].trim());
			 affectation.setIdSujet(idSujet);
			 new AffectationDao().enregistrerAffectation(affectation);
			 view.addObject("success", true);
        	     view.addObject("failure", false);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			 view.addObject("success", false);
        		view.addObject("failure", true);
			e.printStackTrace();
		}
		
		return view;
	}
	
	/**
	 * Methode permettant a l'admin de rajouter un encadrant 
	 * @param request requete http 
	 * @return ajout Encadrant
	 * @throws IOException une exception
	 */
	@RequestMapping("/ajoutEncadrant")
	public ModelAndView ajoutEncadrant(HttpServletRequest request) throws IOException {
		/**Après l'ajout on renvoie la page gestionTables */
		ModelAndView view=new ModelAndView("gestionTables");
		Encadrant encadrant=new Encadrant();
		
		try {
			 int idEnseignant=Integer.valueOf(request.getParameter("enseignant").split("-")[1].trim());
			 int idSujet=Integer.valueOf(request.getParameter("sujet").split("-")[1].trim());
			 encadrant.setIdEnseignant(idEnseignant);
			 encadrant.setIdSujet(idSujet);			
			 new EncadrantDao().enregistrerEncadrant(encadrant);
			 view.addObject("success", true);
        	     view.addObject("failure", false);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			 view.addObject("success", false);
        		view.addObject("failure", true);
			e.printStackTrace();
		}
		
		return view;
	}
	
	/**
	 * Methode permettant a l'admin de rajouter un MembreDuGroupe
	 * @param request requete http 
	 * @return vue ajout MembreDuGroupe 
	 * @throws IOException une exception
	 */
	@RequestMapping("/ajoutMembreDuGroupe")
	public ModelAndView ajoutMembreDuGroupe(HttpServletRequest request) throws IOException {
		/**Après l'ajout on renvoie la page gestionTables */
		ModelAndView view=new ModelAndView("gestionTables");
		MembreDuGroupe membreDuGroupe=new MembreDuGroupe();
		
		try {
			 membreDuGroupe.setIdGroupe(Integer.valueOf(request.getParameter("groupe")));
			 int idEtudiant=Integer.valueOf(request.getParameter("etudiant").split("-")[1].trim());
			 membreDuGroupe.setIdEtudiant(idEtudiant);
			 
			 new MembreDuGroupeDao().enregistrerMembreDuGroupe(membreDuGroupe);
			 view.addObject("success", true);
        	     view.addObject("failure", false);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			 view.addObject("success", false);
        		view.addObject("failure", true);
			e.printStackTrace();
		}
		
		return view;
	}
	
	/**
	 * Methode permettant a l'admin de rajouter un etudiant
	 * @param request requete http 
	 * @return vue ajout Etudiant
	 * @throws IOException une exception
	 */
	@RequestMapping("/ajoutEtudiant")
	public ModelAndView ajoutEtudiant(HttpServletRequest request) throws IOException {
		/**Après l'ajout on renvoie la page gestionTables */
		ModelAndView view=new ModelAndView("gestionTables");
		Etudiant etudiant=new Etudiant();
		etudiant.setNom(HtmlUtils.htmlEscape(request.getParameter("nom").trim()));
		etudiant.setPrenom(HtmlUtils.htmlEscape(request.getParameter("prenom").trim()));
		etudiant.setEmail(HtmlUtils.htmlEscape(request.getParameter("email").trim()));
		Date sf;
		try {
			sf = new SimpleDateFormat("dd/MM/yyyy").parse(request.getParameter("dateNaissance"));
			etudiant.setDateNaissance(sf);
			new EtudiantDao().enregistrerEtudiant(etudiant);
			 view.addObject("success", true);
        	     view.addObject("failure", false);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			 view.addObject("success", false);
        		view.addObject("failure", true);
			e.printStackTrace();
		}
		
		return view;
		
	}
	
	/**
	 * Methode permettant a l'admin de rajouter un groupe
	 * @param request http requete
	 * @return vue ajout Groupe
	 * @throws IOException une exception
	 */
	@RequestMapping("/ajoutGroupe")
	public ModelAndView ajoutGroupe(HttpServletRequest request) throws IOException {
		/**Après l'ajout on renvoie la page gestionTables */
		ModelAndView view=new ModelAndView("gestionTables");
		Groupe groupe=new Groupe();
		
		try {
			
			new GroupeDao().enregistrerGroupe(groupe);
			 view.addObject("success", true);
        	     view.addObject("failure", false);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			 view.addObject("success", false);
        		view.addObject("failure", true);
			e.printStackTrace();
		}
		
		return view;
		
	}
	
	/**
	 * Methode permettant a l'admin de rajouter un sujet
	 * @param request requete http 
	 * @return vue ajout sujet
	 * @throws IOException une exception
	 */
	@RequestMapping("/adminAjoutSujet")
	public ModelAndView adminAjoutSujet(HttpServletRequest request) throws IOException {
		/**Après l'ajout on renvoie la page gestionTables */
		ModelAndView view=new ModelAndView("gestionTables");
		Sujet sujet=new Sujet();
		
		try {
			sujet.setDescription(HtmlUtils.htmlEscape(request.getParameter("description").trim()));
			sujet.setIntitule(HtmlUtils.htmlEscape(request.getParameter("intitule").trim()));
			sujet.setNbCopie(Integer.valueOf(request.getParameter("nbCopie")));
			String enseignement=HtmlUtils.htmlEscape(request.getParameter("enseignement").trim());
			String enseignant=HtmlUtils.htmlEscape(request.getParameter("enseignant").trim());
			int idEnseignement=Integer.valueOf(enseignement.split("-")[1].trim());
			int idEnseignant=Integer.valueOf(enseignant.split("-")[1].trim());
			sujet.setIdEnseignement(idEnseignement);
			new SujetDao().enregistrerSujet(sujet);
			List<Sujet> sujets = new SujetDao().getSujets();
			Encadrant encadrant = new Encadrant();
			encadrant.setIdEnseignant(idEnseignant);
			encadrant.setIdSujet(sujets.get(sujets.size() - 1).getIdSujet());
			EncadrantDao endao = new EncadrantDao();
			endao.enregistrerEncadrant(encadrant);
			 view.addObject("success", true);
        	     view.addObject("failure", false);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			 view.addObject("success", false);
        		view.addObject("failure", true);
			e.printStackTrace();
		}
		
		return view;
	}
	
	/**
	 * Methode permettant la gestion des tables
	 * @param request requete http 
	 * @return vue gestionTables
	 * @throws IOException une exception
	 */
	@RequestMapping("/gestionTables")
	public ModelAndView gestionTables(HttpServletRequest request) throws IOException {
		return new ModelAndView("gestionTables");
	}
	
	/**
	 * La page gestion table
	 * @param request requete http
	 * @return vue gestion table
	 * @throws IOException une exception
	 */
	@RequestMapping("/gestionTablesAction")
	public ModelAndView gestionTablesAction(HttpServletRequest request) throws IOException {
		/** Si l'on clique sur ouvrir */
		if(request.getParameter("ouvrir")!=null)
		{
			String action=HtmlUtils.htmlEscape(((String) request.getParameter("ouvrir")).trim());
			String table=action.split(" ")[1].trim();
    			List<Groupe> groupes=new GroupeDao().getGroupes();
    			List<Enseignant> enseignants=new EnseignantDao().getEnseignants();
        	 	List<Sujet> sujets=new SujetDao().getSujets();
        	 	List<Enseignement> enseignements=new EnseignementDao().getEnseignements();
        	 	List<Etudiant> etudiants=new EtudiantDao().getEtudiants();
        	 	/** On essaie de savoir de quelle table il s'agit et on fait l'action correspondant*/
			 switch (table) {
	         case "Affectation":
	        	 	return new ModelAndView("ajoutAffectation").addObject("groupes", groupes).addObject("sujets", sujets);
			case "Encadrant":
        	 		return new ModelAndView("ajoutEncadrant").addObject("enseignants", enseignants).addObject("sujets", sujets);
	         case "Enseignant":
	        	 	return new ModelAndView("ajoutEnseignant");
	         case "Enseignement":
	        	 	return ajoutEnseignement(request);
	         case "Etudiant":          
	        	 	return new ModelAndView("ajoutEtudiant");
	         case "Groupe":
	        	 	return new ModelAndView("ajoutGroupe");
	         case "MembreDuGroupe":
	        	 	return new ModelAndView("ajoutMembreDuGroupe").addObject("etudiants", etudiants).addObject("groupes", groupes);
	         case "Sujet":	        	 	
	        	 	return new ModelAndView("adminAjoutSujet").addObject("enseignements", enseignements).addObject("enseignants", enseignants);
	         case "Souhait":
	        	 	return new ModelAndView("ajoutSouhait").addObject("sujets", sujets).addObject("groupes", groupes);

	     }
			
		}
		/**Si l'action c'est vidé */
		else if(request.getParameter("vider")!=null)
		{
			try
			{
					 String action=HtmlUtils.htmlEscape(((String) request.getParameter("vider")).trim());
					 String table=action.split(" ")[1].trim();
					 /** On essaie de savoir de quelle table il s'agit puis on l'ajoute */
					 switch (table) {
			         case "Affectation":
			        	 new AffectationDao().deleteAffectations();
			        	 break;
			         case "Encadrant":
			        	 new EncadrantDao().deleteEncadrants();
			        	 break;
			         case "Enseignant":
			        	 new EnseignantDao().deleteEnseignants();
			        	 break;
			         case "Enseignement":
			        	 new EnseignementDao().deleteEnseignements();
			        	 break;
			         case "Etudiant":          
			        	 new EtudiantDao().deleteEtudiants();
			        	 break;
			         case "Groupe":
			        	 new GroupeDao().deleteGroupes();
			        	 break;
			         case "MembreDuGroupe":
			        	 new MembreDuGroupeDao().deleteMembreDuGroupes();
			        	 break;
			         case "Sujet":
			        	 new SujetDao().deleteSujets();
			        	 break;
			         case "Souhait":
			        	 new SouhaitDao().deleteSouhaits();
			        	 break;
			        	 
			        	 
	
				}
				 ModelAndView view=new ModelAndView("gestionTables");
		        	 view.addObject("success", true);
		        	 view.addObject("failure", false);
		        	 return view;
			}
			catch(Exception e)
			{
				 System.out.println(e.getMessage());
				 e.printStackTrace();
				 ModelAndView view=new ModelAndView("gestionTables");
		        	 view.addObject("success", false);
		        	 view.addObject("failure", true);
		        	 return view;
			}
			 	 
	     }
		else
		{
			return new ModelAndView("gestionTables");
		}
		return null;
		
		
	}
}
