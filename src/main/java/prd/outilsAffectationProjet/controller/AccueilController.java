package prd.outilsAffectationProjet.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import prd.outilsAffectationProjet.dao.*;
import prd.outilsAffectationProjet.model.*;
import org.springframework.web.util.HtmlUtils;

/**
 * Controller pour la vue accueil 
 * Elle met en place tout ce qui est recherche dans la base 
 * En fonction de l'année pour du nom de l'enseignement  et 
 * de la description du sujet
 * Elle affiche au maximum 20 sujets
 * @author MariamKonate
 *
 */
@Controller
public class AccueilController {

	/**
	 * Méthode pour afficher la page d'accueil
	 * @param request la requete http
	 * @return la vue accueil
	 * @throws IOException une exception
	 */
	@RequestMapping(value={"", "/", "index"})
	public ModelAndView acceuil(HttpServletRequest request) throws IOException{
		/**
		 *  ModelAndView  envoie des objets a la vue et nous permet egalement 
		 *  d'y acceder accueil est la page jsp pour l'accueil On va lui transmettre des objets
		 */
		
		ModelAndView view= new ModelAndView("accueil");
		
	    /**
	     * Les paramètres de base
	     * Dans la recherche on affiche les enseignements dans la base de donnees
	     * et les annees entre l'annee actuelle et 50 ans en arriere
	     */
	    EnseignementDao enseignementDao= new EnseignementDao();
	    List <Enseignement> enseignements=enseignementDao.getEnseignements();
	    int year = Calendar.getInstance().get(Calendar.YEAR);
	    view.addObject("currentYear", year);
	    view.addObject("startYear", year-50);
	    view.addObject("enseignements",enseignements);
	    
	    /**
	     * Si nous venons d'atterir sur la page sans avoir rien cherché nous affichons les Trois derniers
	     * Projets insérés dans la base
	     */
	    if( request.getParameter("research")==null && request.getParameter("annee")==null  && request.getParameter("enseignement")==null)
		{
	    		/**
	    		 * Data access object qui permet de prendre une liste de sujet limité 
	    		 * à un int (ici 10)
	    		 */
			 SujetDao sujetDao = new SujetDao();
			 List <Sujet> sujets = (List < Sujet>)sujetDao.getSujets(10);
			 
			 /**
			  * Boucle permettant de rajouter a la classe sujet les attributs comme nom de l'enseignement
			  * l'annee et le nom de l'encadrant
			  */
		     for(Sujet sujet:sujets)
		     {		    		
		    		sujet.setNomEnseignement(sujetDao.getNomEnseignementSujet(sujet.getIdEnseignement()));
		    		sujet.setAnnee(sujetDao.getAnneeEnseignementSujet(sujet.getIdEnseignement()));
		    		int idEnseignant=enseignementDao.getIdEnseignant(sujet.getIdEnseignement()); 
		    		sujet.setNomEnseignant(enseignementDao.getNomEncadrant(idEnseignant));
		     }
		     
		     // nous renvoyons cela a la vue
		     view.addObject("sujets", sujets);
		}
		else
		{
			/**
		     * Si nous avons fais une recherche 
		     * Nous décorticons la requête et renvoyons
		     * La liste des sujets limités à 20
		     */
			String sujet="";
			int annee=0;
			String enseignement="";
			/**
			 * Recuperation des parametres non null
			 */
			if(request.getParameter("research")!=null)
			{
				if(!request.getParameter("research").trim().isEmpty())
				{
					sujet=HtmlUtils.htmlEscape(request.getParameter("research").trim());
				}
				
			}
			if(request.getParameter("annee")!=null)
			{
				if(!request.getParameter("annee").equals("annee"))
				{
					annee=Integer.valueOf(request.getParameter("annee").trim());
				}
			}
			if(request.getParameter("enseignement")!=null)
			{
				if(!request.getParameter("enseignement").equals("enseignement"))
				{
					enseignement=HtmlUtils.htmlEscape(request.getParameter("enseignement").trim());
				}
				 
			}
			
			/**
			 * Si la recherche se fait avec un sujet
			 */
			if(!sujet.isEmpty())
			{
				/**
				 * Si l'enseignement est vide et l'année également
				 */
				if(enseignement.equals("") && annee==0)
				{
					 SujetDao sujetDao = new SujetDao();
					 List <Sujet> sujets = (List < Sujet>)sujetDao.getSujets(sujet);
				     for(Sujet suj:sujets)
				     {		    		
				    		suj.setNomEnseignement(sujetDao.getNomEnseignementSujet(suj.getIdEnseignement()));
				    		suj.setAnnee(sujetDao.getAnneeEnseignementSujet(suj.getIdEnseignement()));
				    		int idEnseignant=enseignementDao.getIdEnseignant(suj.getIdEnseignement()); 
				    		suj.setNomEnseignant(enseignementDao.getNomEncadrant(idEnseignant));
				     }
				     view.addObject("sujets", sujets);
				}
				else if(!enseignement.equals("") && annee==0)
				{
					
					/**
					 * Si l'enseignement est renseigné et l'année vide
					 */
					 int id=enseignementDao.getIdEnseignement(enseignement, annee);
					 SujetDao sujetDao = new SujetDao();
					 if(id!=-1)
					 {
						 List <Sujet> sujets = (List < Sujet>)sujetDao.getSujetsEnseignementDescription(id,sujet);
					     for(Sujet suj:sujets)
					     {		    		
					    		suj.setNomEnseignement(sujetDao.getNomEnseignementSujet(suj.getIdEnseignement()));
					    		suj.setAnnee(sujetDao.getAnneeEnseignementSujet(suj.getIdEnseignement()));
					    		int idEnseignant=enseignementDao.getIdEnseignant(suj.getIdEnseignement()); 
					    		suj.setNomEnseignant(enseignementDao.getNomEncadrant(idEnseignant));
					     }
					     view.addObject("sujets", sujets);
					 }
					 else
					 {
						 
						 view.addObject("sujets",new ArrayList<Sujet>());
					 }
				     
				}
				else
				{
					/**
					  * Si l'enseignement est vide et l'année renseignée
					  */
					List<Integer> id=enseignementDao.getIdEnseignement( annee);
					 SujetDao sujetDao = new SujetDao();
					 if(!id.isEmpty())
					 {
						 List <Sujet> sujets= new ArrayList<Sujet>();
						 for(Integer i:id)
						 {
							 sujets.addAll((List < Sujet>)sujetDao.getSujetsEnseignementDescription(i,sujet));
							 
						 }
						 for(Sujet suj:sujets)
					     {		    		
					    		suj.setNomEnseignement(sujetDao.getNomEnseignementSujet(suj.getIdEnseignement()));
					    		suj.setAnnee(sujetDao.getAnneeEnseignementSujet(suj.getIdEnseignement()));
					    		int idEnseignant=enseignementDao.getIdEnseignant(suj.getIdEnseignement()); 
					    		suj.setNomEnseignant(enseignementDao.getNomEncadrant(idEnseignant));
					     }
						 
					     view.addObject("sujets", sujets);
					 }
					 else
					 {
						 view.addObject("sujets",new ArrayList<Sujet>());
					 }
				}
			}
			else
			{
				/**
				 * Si la recherche se fait sans sujet
				 */
				
				/**
				 * Si tout est vide
				 */
				if(enseignement.equals("") && annee==0)
				{
					 SujetDao sujetDao = new SujetDao();
					 List <Sujet> sujets = (List < Sujet>)sujetDao.getSujets();
				     for(Sujet suj:sujets)
				     {		    		
				    		suj.setNomEnseignement(sujetDao.getNomEnseignementSujet(suj.getIdEnseignement()));
				    		suj.setAnnee(sujetDao.getAnneeEnseignementSujet(suj.getIdEnseignement()));
				    		int idEnseignant=enseignementDao.getIdEnseignant(suj.getIdEnseignement()); 
				    		suj.setNomEnseignant(enseignementDao.getNomEncadrant(idEnseignant));
				     }
				     view.addObject("sujets", sujets);
				}
				/**
				 * Si l'enseignement est renseignée et l'année est vide 
				 */
				else if(!enseignement.equals(""))
				{
					
					 int id=enseignementDao.getIdEnseignement(enseignement, annee);
					 SujetDao sujetDao = new SujetDao();
					 if(id!=-1)
					 {
						 List <Sujet> sujets = (List < Sujet>)sujetDao.getSujetsEnseignement(id);
					     for(Sujet suj:sujets)
					     {		    		
					    		suj.setNomEnseignement(sujetDao.getNomEnseignementSujet(suj.getIdEnseignement()));
					    		suj.setAnnee(sujetDao.getAnneeEnseignementSujet(suj.getIdEnseignement()));
					    		int idEnseignant=enseignementDao.getIdEnseignant(suj.getIdEnseignement()); 
					    		suj.setNomEnseignant(enseignementDao.getNomEncadrant(idEnseignant));
					     }
					     view.addObject("sujets", sujets);
					 }
					 else
					 {
						 view.addObject("sujets",new ArrayList<Sujet>());
					 }
				     
				}
				else
				{
					/**
					 * Si l'enseignement est vide et l'année est renseignée 
					 */
					 List<Integer> id=enseignementDao.getIdEnseignement(annee);
					 SujetDao sujetDao = new SujetDao();
					 if(id.isEmpty())
					 {
						 view.addObject("sujets",new ArrayList<Sujet>());
					 }
					 else
					 {
						 List <Sujet> sujets= new ArrayList<Sujet>();
						 for(Integer i:id)
						 {
							 sujets.addAll((List < Sujet>)sujetDao.getSujetsEnseignement(i));
							 
						 }
						 for(Sujet suj:sujets)
					     {		    		
					    		suj.setNomEnseignement(sujetDao.getNomEnseignementSujet(suj.getIdEnseignement()));
					    		suj.setAnnee(sujetDao.getAnneeEnseignementSujet(suj.getIdEnseignement()));
					    		int idEnseignant=enseignementDao.getIdEnseignant(suj.getIdEnseignement()); 
					    		suj.setNomEnseignant(enseignementDao.getNomEncadrant(idEnseignant));
					     }
						 view.addObject("sujets", sujets);
					 }
					     
					     
					 }
				 }
			}
			
			
			
		
		return view;
	}
	
}
