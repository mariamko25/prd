package prd.outilsAffectationProjet.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.HtmlUtils;

import prd.outilsAffectationProjet.dao.*;
import prd.outilsAffectationProjet.model.*;

/**
 * Controller pour la vue Enseignant Elle met en place tout ce qui est gestion
 * de projet Insertion de sujet, création de formulaire etc..
 * 
 * @author MariamKonate
 *
 */
@Controller
public class EnseignantController {

	
	
	/**
	 * Deconnexion de la page  pour l'enseignant
	 * @param request requete http
	 * @return vue acceuil
	 * @throws IOException une exception
	 */
	@RequestMapping("/enseignantDeconnexion")
	public ModelAndView enseignantDeconnexion(HttpServletRequest request) throws IOException {
		
		
		request.getSession().setAttribute("enseignantConnecte", null); 
		request.getSession().setAttribute("email", null); 

		
		return new AccueilController().acceuil(request);
	}
	/**
	 * Affichage de la page  pour l'enseignant
	 * @param request requete http
	 * @return vue enseignant
	 * @throws IOException une exception
	 */
	@RequestMapping("/enseignant")
	public ModelAndView enseignant(HttpServletRequest request) throws IOException {
		/**
		 * ModelAndView envoie des objets a la vue et nous permet egalement d'y acceder
		 * enseignant est la page jsp pour la partie enseignant On va lui transmettre
		 * des objets
		 */
		ModelAndView view = new ModelAndView("enseignant");

		/**
		 * Après authentification on essaie d'obtenir les enseignements correspondant à
		 * l'enseignant
		 */
		String email = "";
		if (request.getParameter("email") == null) {
			email = "";
		} else {
			email = HtmlUtils.htmlEscape(request.getParameter("email").trim());

		}

		if (request.getSession().getAttribute("email") != null) {

			email = HtmlUtils.htmlEscape(((String) request.getSession().getAttribute("email")).trim());
		}
		request.getSession().setAttribute("enseignantConnecte", true); 
		/** Enregistrement de la session de l'utilisateur */
		HttpSession session = request.getSession();
		session.setAttribute("email", email);

		/** Obtenir la liste des enseignements */
		EnseignantDao enseignantDao = new EnseignantDao();
		int idEnseignant = enseignantDao.getIdEnseignant(email);
		EnseignementDao enseignementDao = new EnseignementDao();
		List<Enseignement> enseignements = enseignementDao.getEnseignements();

		/** Envoyer cette liste à la vue */
		view.addObject("enseignements", enseignements);
		view.addObject("idEnseignant", idEnseignant);
		return view;

	}

	/**
	 * Affichage de la page de recherche d'enseignements
	 * @param request requete http 
	 * @return vue enseignant
	 * @throws IOException une exception
	 */
	@RequestMapping("/enseignantRecherche")
	public ModelAndView enseignantRecherche(HttpServletRequest request) throws IOException {
		/**
		 * ModelAndView envoie des objets a la vue et nous permet egalement d'y acceder
		 * enseignant est la page jsp pour la partie enseignantRecherche On va lui transmettre
		 * des objets
		 */

		ModelAndView view = new ModelAndView("enseignant");
		String email = HtmlUtils.htmlEscape(((String) request.getSession().getAttribute("email")).trim());
		EnseignantDao enseignantDao = new EnseignantDao();
		int idEnseignant = enseignantDao.getIdEnseignant(email);
		EnseignementDao enseignementDao = new EnseignementDao();

		if (request.getParameter("research") != null && !request.getParameter("research").trim().isEmpty()) {
			/** Obtenir la liste des enseignements like */

			List<Enseignement> enseignements = enseignementDao.getEnseignementsLike(HtmlUtils.htmlEscape(request.getParameter("research").trim()));

			/** Envoyer cette liste à la vue */
			view.addObject("enseignements", enseignements);
		} else {
			/** Obtenir la liste des enseignements */

			List<Enseignement> enseignements = enseignementDao.getEnseignements();

			/** Envoyer cette liste à la vue */
			view.addObject("enseignements", enseignements);
		}

		view.addObject("idEnseignant", idEnseignant);

		return view;

	}

	/**
	 * Controller pour la page souhaits
	 * 
	 * @param request requete http
	 * @return vue souhaits
	 * @throws IOException une exception
	 */
	@RequestMapping("/souhaits")
	public ModelAndView enseignantEnseignement(HttpServletRequest request) throws IOException {

		/**
		 * ModelAndView envoie des objets a la vue et nous permet egalement d'y acceder
		 * enseignement est la page jsp pour la partie enseignement On va lui
		 * transmettre des objets
		 */

		ModelAndView view = new ModelAndView("souhaits");
		/** Obtenir l'idEnseignement **/
		if (request.getSession().getAttribute("idEnseignement") != null) {
			int id = (Integer) request.getSession().getAttribute("idEnseignement");
			EnseignementDao dao = new EnseignementDao();
			/** Obtenir l'enseignement **/
			Enseignement enseignement = dao.getEnseignement(id);
			view.addObject("enseignement", enseignement);

			EnseignantDao endao = new EnseignantDao();
			SouhaitDao sdao = new SouhaitDao();
			/** L'enseignement nous permet d'obtenir la liste des souhaits des etudiants **/
			List<Souhait> souhaits = sdao.getSouhaits(id);
			if (!souhaits.isEmpty()) {
				for (Souhait sht : souhaits) {
					if (endao.getEnseignantSujet(sht.getIdSujet()) != null) {
						/** On rajoute le nom de l'encadrant **/
						sht.setNomEncadrant(endao.getEnseignantSujet(sht.getIdSujet()).getNom());

					}
				}
			}
			/** On renvoie cet objet a la vue **/
			view.addObject("souhaits", souhaits);
		}
		return view;
	}

	/**
	 * Affichage des sujets pour un enseignement
	 * @param request requete http 
	 * @return vue enseignementSujets
	 * @throws IOException une exception
	 */
	@RequestMapping("/enseignementSujets")
	public ModelAndView enseignementSujets(HttpServletRequest request) throws IOException {

		/**
		 * ModelAndView envoie des objets a la vue et nous permet egalement d'y acceder
		 * enseignementSujets est la page jsp pour la partie enseignementSujets On va
		 * lui transmettre des objets
		 */
		int idEnseignement;
		ModelAndView view = new ModelAndView("enseignementSujets");

		/** Si l'on atterit sur enseignementSujets pour la premiere fois **/
		if (request.getParameter("ouvrir") != null) {
			String str = request.getParameter("idEnseignement");
			idEnseignement = Integer.valueOf(str);

			/** On enregistre l'identifiant de l'enseignement en session **/
			request.getSession().setAttribute("idEnseignement", idEnseignement);
		} else {
			/** On recupere l'identifiant de l'enseignement depuis la session **/
			idEnseignement = (Integer) request.getSession().getAttribute("idEnseignement");
		}
		EnseignementDao enDao = new EnseignementDao();
		/** identifiant de l'enseignant encadrant **/
		int idEnseignant = enDao.getIdEnseignant(idEnseignement);

		/**
		 * Si l'on a un parametre idEnseignant pour l'enseignant qui est connecte on
		 * enregistre en session
		 **/
		if (request.getParameter("idEnseignant") != null) {
			request.getSession().setAttribute("idEnseignant", Integer.valueOf(request.getParameter("idEnseignant")));
		}

		/** On verifie si l'encadrant n'est pas l'enseignant connecte **/
		if (idEnseignant != (Integer) request.getSession().getAttribute("idEnseignant")) {
			SujetDao dao = new SujetDao();
			List<Sujet> sujets = dao.getSujetsEnseignementEnseignant(idEnseignement,
					(Integer) (request.getSession().getAttribute("idEnseignant")));
			view.addObject("sujets", sujets);
			view.addObject("idEnseignement", idEnseignement);
			view.addObject("idEnseignant", (Integer) request.getSession().getAttribute("idEnseignant"));
			/** On ne lui affiche pas le menu liste souhait **/
			view.addObject("showMenu", false);
		} else if (idEnseignant == (Integer) request.getSession().getAttribute("idEnseignant")) {
			/** S'il s'agit de l'encadrant qui est connecté on lui affiche tout **/
			SujetDao dao = new SujetDao();
			List<Sujet> sujets = dao.getSujetsEnseignement(idEnseignement);
			view.addObject("sujets", sujets);
			view.addObject("idEnseignement", idEnseignement);
			view.addObject("idEnseignant", (Integer) request.getSession().getAttribute("idEnseignant"));
			view.addObject("showMenu", true);

		}
		/** On verifie si le formulaire est actif ie les etudiants l'utilisent **/
		EnseignementDao enseignementDao = new EnseignementDao();
		Enseignement enseignement = enseignementDao.getEnseignement(idEnseignement);
		int formulaire = enseignement.getFormulaire();
		/** On renvoie cette information a la vue **/
		if (formulaire == 0) {
			view.addObject("formulaire", false);
		} else {
			view.addObject("formulaire", true);

		}

		return view;
	}

	/**
	 * Methode permettant de upload un fichier de voeux
	 * @param file fichier d'entrée
	 * @param request requete http
	 * @param idEnseignement l'id de l'enseignement
	 * @return vue enseignant
	 * @throws IOException une exception
	 */
	@RequestMapping("/uploadFile")
	public ModelAndView submit(@RequestParam("file") MultipartFile file, HttpServletRequest request,
			@RequestParam("idEnseignement") int idEnseignement) throws IOException {

		try {

			/**
			 * Récupération du fichier et conversion en une liste de souhaits
			 */
			File fichier = new File("souhait.csv");
			file.transferTo(fichier);
			BufferedReader reader = new BufferedReader(new FileReader(fichier.getAbsolutePath()));
			String line = null;
			List<String> linesouhaits = new ArrayList<String>();

			// Récupération des lignes du fichier
			while ((line = reader.readLine()) != null) {
				linesouhaits.add(line);

			}
			reader.close();

			/** Tant que la liste des souhaits n'est pas vide **/
			while (!linesouhaits.isEmpty()) {
				String[] stringSouhait = linesouhaits.get(0).split(",");
				/**
				 * La première case c'est la liste des mails des etudiants 
				 * 
				 */
				String emails[] = stringSouhait[0].trim().split(";");
				/**
				 * La deuxième case c'est l'intitule du sujet
				 */
				String intitule = stringSouhait[1].trim();
				/**
				 * La troisième case c'est le score
				 */
				int score = Integer.valueOf(stringSouhait[2].trim());
				String emailsProf = stringSouhait[3].trim();
				System.out.println(emailsProf);
				/**
				 * Création du groupe
				 */
				GroupeDao groupeDao = new GroupeDao();
				Groupe groupe = new Groupe();
				groupeDao.enregistrerGroupe(groupe);
				/** Je recupere le dernier groupe cree **/
				groupe = groupeDao.getGroupes().get(groupeDao.getGroupes().size() - 1);
				/**
				 * Insertion dans la base de données inclus création d'étudiants ou de sujet
				 */
				EtudiantDao etuDao = new EtudiantDao();
				for (int i = 0; i < emails.length; i++) {
					/** Si l'etudiant n'existe pas on le cree **/
					if (etuDao.getEtudiant(emails[i]) == null) {
						Etudiant etu = new Etudiant();
						etu.setEmail(emails[i]);
						etuDao.enregistrerEtudiant(etu);
					}
					/** On recupere l'etudiant ayant l'adresse mail **/
					Etudiant etudiant = etuDao.getEtudiant(emails[i]);

					/** On l'enregistre en tant que membre du groupe **/
					MembreDuGroupeDao mdgDao = new MembreDuGroupeDao();
					MembreDuGroupe mdg = new MembreDuGroupe();
					mdg.setIdEtudiant(etudiant.getIdEtudiant());
					mdg.setIdGroupe(groupe.getIdGroupe());
					mdgDao.enregistrerMembreDuGroupe(mdg);
				}
				/** On cree un souhait **/
				Souhait sht = new Souhait();
				/** On lui donne comme idGroupe **/
				sht.setIdGroupe(groupe.getIdGroupe());
				SujetDao sjDao = new SujetDao();
				Sujet sujet = new Sujet();
				/** On cherche l'identifiant du sujet et on verifie s'il n'existe pas **/
				if (sjDao.getSujetIntituleEnseignement(intitule, idEnseignement) == null) {

					sujet.setIdEnseignement(idEnseignement);
					sujet.setIntitule(intitule);
					sujet.setNbCopie(1);
					sjDao.enregistrerSujet(sujet);
					sujet = sjDao.getSujetIntituleEnseignement(intitule, idEnseignement);

					/** On crée l'encadrant s'il n'existe pas **/
					Enseignant enseign = new Enseignant();

					if (new EnseignantDao().getEnseignant(new EnseignantDao().getIdEnseignant(emailsProf)) == null) {

						enseign.setEmail(emailsProf);
						new EnseignantDao().enregistrerEnseignant(enseign);
					}

					if (new EncadrantDao().getEncadrant(sujet.getIdSujet()) == null) {
						enseign = new EnseignantDao().getEnseignant(new EnseignantDao().getIdEnseignant(emailsProf));
						Encadrant encadr = new Encadrant();
						encadr.setIdEnseignant(enseign.getIdEnseignant());
						encadr.setIdSujet(sujet.getIdSujet());
						new EncadrantDao().enregistrerEncadrant(encadr);
					}
				}

				/** On le recupere **/
				sujet = sjDao.getSujetIntituleEnseignement(intitule, idEnseignement);

				

				/** On donne idSujet et score et on enregistre le souhait **/
				sht.setIdSujet(sujet.getIdSujet());
				sht.setScore(score);
				SouhaitDao shtDao = new SouhaitDao();
				shtDao.enregistrerSouhait(sht);
				/** Apres avoir enregistrer le souhait on supprime la ligne **/
				linesouhaits.remove(0);
				/**
				 * On cherche dans la liste de souhaits le groupe pour lequel on a enregistré un
				 * souhait
				 **/
				for (int i = 0; i < linesouhaits.size(); i++) {
					if (stringSouhait[0].equals(linesouhaits.get(i).split(",")[0])) {
						/**
						 * Si on le trouve on enregiste d'autres souhaits pour ce groupe et on supprime
						 **/
						Sujet sj = new Sujet();
						sj.setIdEnseignement(idEnseignement);
						String strIntitule = linesouhaits.get(i).split(",")[1];
						emailsProf=linesouhaits.get(i).split(",")[3].trim();
						int scor = Integer.valueOf(linesouhaits.get(i).split(",")[2]);
						if (sjDao.getSujetIntituleEnseignement(strIntitule, idEnseignement) == null) {
							sj.setIntitule(strIntitule);
							sj.setNbCopie(1);
							sjDao.enregistrerSujet(sj);
							sj = sjDao.getSujetIntituleEnseignement(strIntitule, idEnseignement);
							/**
							 * On enregistre l'encadrant
							 */
							Enseignant enseignant = new Enseignant();
							if (new EnseignantDao()
									.getEnseignant(new EnseignantDao().getIdEnseignant(emailsProf)) == null) {

								enseignant.setEmail(emailsProf);
								new EnseignantDao().enregistrerEnseignant(enseignant);
							}
							enseignant = new EnseignantDao().getEnseignant(new EnseignantDao().getIdEnseignant(emailsProf));
							Encadrant encadrant = new Encadrant();
							encadrant.setIdEnseignant(enseignant.getIdEnseignant());
							encadrant.setIdSujet(sj.getIdSujet());
							new EncadrantDao().enregistrerEncadrant(encadrant);
						}
						sj = sjDao.getSujetIntituleEnseignement(strIntitule, idEnseignement);
						
						Souhait st = new Souhait();
						st.setIdGroupe(groupe.getIdGroupe());
						st.setIdSujet(sj.getIdSujet());
						st.setScore(scor);
						shtDao.enregistrerSouhait(st);
						linesouhaits.remove(i);
					}
				}

			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			return enseignantEnseignement(request).addObject("failure", true);
		}
		return enseignantEnseignement(request).addObject("success", true);
	}

	/**
	 * Controller pour ajouter un sujet à un enseignement
	 * @param request requete http
	 * @return vue ajout Sujet
	 * @throws IOException une exception
	 */
	@RequestMapping("/ajoutSujet")
	public ModelAndView ajoutSujet(HttpServletRequest request) throws IOException {
		/**
		 * ModelAndView envoie des objets a la vue et nous permet egalement d'y acceder,
		 * ajoutSujet est la page jsp pour la partie ajoutSujet On va lui transmettre
		 * des objets
		 */
		ModelAndView view = new ModelAndView("ajoutSujet");
		try {

			/** On recupere l'enseignement et l'enseignant en session **/
			int idEnseignement = (Integer) request.getSession().getAttribute("idEnseignement");
			int idEnseignant = (Integer) request.getSession().getAttribute("idEnseignant");
			/** Si l'on a déjà indiqué les attributs du sujet **/
			if (request.getParameter("intitule") != null && request.getParameter("description") != null) {
				/** On cree un nouveau sujet a partir des renseignement du form **/
				Sujet sujet = new Sujet();
				sujet.setIntitule(HtmlUtils.htmlEscape(request.getParameter("intitule").trim()));
				sujet.setDescription(HtmlUtils.htmlEscape(request.getParameter("description").trim()));
				sujet.setNbCopie(Integer.valueOf(request.getParameter("nbCopie")));
				sujet.setIdEnseignement(idEnseignement);
				/** On enregistre le sujet dans la bdd **/
				SujetDao dao = new SujetDao();
				dao.enregistrerSujet(sujet);
				/** On indique que cet enseignant est l'encadrant du sujet enregistré **/
				List<Sujet> sujets = dao.getSujets();
				Encadrant encadrant = new Encadrant();
				encadrant.setIdEnseignant(idEnseignant);
				encadrant.setIdSujet(sujets.get(sujets.size() - 1).getIdSujet());
				EncadrantDao endao = new EncadrantDao();
				endao.enregistrerEncadrant(encadrant);
				view.addObject("success", true);
				view.addObject("failure", false);
			}
			EnseignementDao enseignantDao = new EnseignementDao();
			int idEnseignantEnseignement = enseignantDao.getIdEnseignant(idEnseignement);
			/**
			 * Si l'enseignant est l'encadrant on montre tout le menu
			 */
			if (idEnseignantEnseignement == idEnseignant) {
				view.addObject("idEnseignement", idEnseignement);
				view.addObject("idEnseignant", idEnseignant);
				view.addObject("showMenu", true);
			} else {
				/** Sinon on cache le menu **/
				view.addObject("idEnseignement", idEnseignement);
				view.addObject("idEnseignant", idEnseignant);
				view.addObject("showMenu", false);
			}

			/**
			 * On indique egalement si le formulaire est actif ou non
			 */
			EnseignementDao enseignementDao = new EnseignementDao();
			Enseignement enseignement = enseignementDao.getEnseignement(idEnseignement);
			int formulaire = enseignement.getFormulaire();
			if (formulaire == 0) {
				view.addObject("formulaire", false);
			} else {
				view.addObject("formulaire", true);

			}

			return view;
		} catch (Exception e) {
			view.addObject("success", false);
			view.addObject("failure", true);
			return view;
		}

	}

	/**
	 * Controller pour modifier le sujet d'un enseignement
	 * @param request requete http
	 * @return vue modifierSujet
	 * @throws IOException une exception
	 */
	@RequestMapping("/modifierSujet")
	public ModelAndView modifierSujet(HttpServletRequest request) throws IOException {

		/**
		 * Si l'on atterit sur la page pour la premiere fois
		 */
		if (request.getParameter("enregistrer") == null && request.getParameter("supprimer") == null) {
			/**
			 * ModelAndView envoie des objets a la vue et nous permet egalement d'y acceder
			 * modifierSujet est la page jsp pour la partie modifierSujet On va lui
			 * transmettre des objets
			 */
			ModelAndView view = new ModelAndView("modifierSujet");

			/**
			 * On recupere le sujet
			 */
			
				String str = request.getParameter("idSujet");
				int idSujet = Integer.valueOf(str);
		
			SujetDao dao = new SujetDao();
			Sujet sujet = dao.getSujet(idSujet);

			/**
			 * On renvoie les objet sujet, idEnseignement et idEnseignant a la vue
			 */
			view.addObject("sujet", sujet);
			view.addObject("idEnseignement", Integer.valueOf(request.getParameter("idEnseignement")));
			view.addObject("idEnseignant", Integer.valueOf(request.getParameter("idEnseignant")));
			Enseignement enseignement = dao
					.getEnseignementSujet(Integer.valueOf(request.getParameter("idEnseignement")));
			/**
			 * On lui indique egalement si le formulaire de l'enseignement est actif ou non
			 */
			int formulaire = enseignement.getFormulaire();
			if (formulaire == 0) {
				view.addObject("formulaire", false);
			} else {
				view.addObject("formulaire", true);

			}
			return view;
		}

		else if (request.getParameter("enregistrer") != null) {
			/**
			 * S'il s'agit d'enregistrer les informations saisies
			 */

			/**
			 * ModelAndView envoie des objets a la vue et nous permet egalement d'y acceder
			 * enseignementSujets est la page jsp pour la partie enseignementSujets On va
			 * lui transmettre des objets
			 */
			ModelAndView view = new ModelAndView("enseignementSujets");
			try {
				/**
				 * On recupere les identifiant de l'enseignant et de l'enseignement
				 */
				int idEnseignement = Integer.valueOf(request.getParameter("idEnseignement"));
				int idEnseignant = Integer.valueOf(request.getParameter("idEnseignant"));
				EnseignementDao enDao = new EnseignementDao();
				int idEncadrant = enDao.getIdEnseignant(idEnseignement);
				request.getSession().setAttribute("idEnseignement", idEnseignement);

				/**
				 * On update le sujet
				 */
				int idSujet = Integer.valueOf(request.getParameter("idSujet"));
				SujetDao dao = new SujetDao();
				Sujet sujet = dao.getSujet(idSujet);
				sujet.setIntitule(HtmlUtils.htmlEscape(request.getParameter("intitule").trim()));
				sujet.setDescription(HtmlUtils.htmlEscape(request.getParameter("description").trim()));
				sujet.setNbCopie(Integer.valueOf(request.getParameter("nbCopie")));
				dao.updateSujet(idSujet, sujet);

				/**
				 * On verifie que l'encadrant est bien l'enseignant connecté
				 */
				if (idEncadrant == idEnseignant) {
					/**
					 * On recupere la liste de tous les sujets de l'enseignement
					 */
					List<Sujet> sujets = dao.getSujetsEnseignement(idEnseignement);
					/**
					 * On envoie a la vue les objets sujets, si le menu doit etre montré,
					 * idEnseignement et idEnseignant
					 */
					view.addObject("sujets", sujets);
					view.addObject("showMenu", true);
					view.addObject("idEnseignement", Integer.valueOf(request.getParameter("idEnseignement")));
					view.addObject("idEnseignant", Integer.valueOf(request.getParameter("idEnseignant")));

				} else {

					/**
					 * On recupere la liste de tous les sujets de l'enseignement pour l'enseignant
					 * actuel
					 */
					List<Sujet> sujets = dao.getSujetsEnseignementEnseignant(idEnseignement, idEnseignant);
					/**
					 * On envoie a la vue les objets sujets, si le menu doit etre montré,
					 * idEnseignement et idEnseignant
					 */
					view.addObject("sujets", sujets);
					view.addObject("showMenu", false);
					view.addObject("idEnseignement", Integer.valueOf(request.getParameter("idEnseignement")));
					view.addObject("idEnseignant", Integer.valueOf(request.getParameter("idEnseignant")));

				}
				/**
				 * Verifie si le formulaire est actif ou non On envoie cette information a la
				 * vue
				 */
				EnseignementDao enseignementDao = new EnseignementDao();
				Enseignement enseignement = enseignementDao.getEnseignement(idEnseignement);
				int formulaire = enseignement.getFormulaire();
				if (formulaire == 0) {
					view.addObject("formulaire", false);
				} else {
					view.addObject("formulaire", true);

				}
				view.addObject("success", true).addObject("failure", false);
			} catch (Exception e) {
				view.addObject("success", true).addObject("failure", false);
				return view;
			}

			return view;
		} else if (request.getParameter("supprimer") != null) {
			/**
			 * Si l'on doit supprimer le sujet
			 */

			/**
			 * ModelAndView envoie des objets a la vue et nous permet egalement d'y acceder
			 * enseignementSujets est la page jsp pour la partie enseignementSujets On va
			 * lui transmettre des objets
			 */
			ModelAndView view = new ModelAndView("enseignementSujets");
			try {
				/**
				 * On recupere les identifiants de l'enseignant et l'encadrant
				 */
				int idEnseignement = Integer.valueOf(request.getParameter("idEnseignement"));
				int idEnseignant = Integer.valueOf(request.getParameter("idEnseignant"));
				EnseignementDao enDao = new EnseignementDao();
				int idEncadrant = enDao.getIdEnseignant(idEnseignement);
				request.getSession().setAttribute("idEnseignement", idEnseignement);
				int id = Integer.valueOf(request.getParameter("idSujet"));
				SujetDao dao = new SujetDao();
				dao.deleteSujet(id);
				if (idEncadrant == idEnseignant) {

					List<Sujet> sujets = dao.getSujetsEnseignement(idEnseignement);
					view.addObject("sujets", sujets);
					view.addObject("showMenu", true);
				} else {

					List<Sujet> sujets = dao.getSujetsEnseignementEnseignant(idEnseignement, idEnseignant);
					view.addObject("sujets", sujets);
					view.addObject("showMenu", false);
				}
				view.addObject("idEnseignement", Integer.valueOf(request.getParameter("idEnseignement")));
				view.addObject("idEnseignant", Integer.valueOf(request.getParameter("idEnseignant")));

				/**
				 * On verifie si le formulaire est actif ou non
				 */
				EnseignementDao enseignementDao = new EnseignementDao();
				Enseignement enseignement = enseignementDao.getEnseignement(idEnseignement);
				int formulaire = enseignement.getFormulaire();
				if (formulaire == 0) {
					view.addObject("formulaire", false);
				} else {
					view.addObject("formulaire", true);

				}
				return view.addObject("success", true).addObject("failure", false);

			} catch (Exception e) {
				return view.addObject("success", false).addObject("failure", true);
			}

		}
		return new ModelAndView("Default");
	}

	/**
	 * Controller pour activer/desactivier un formulaire
	 * @param request requete http 
	 * @return vue enseignementSujets
	 * @throws IOException une exception
	 */
	@RequestMapping("/activerFormulaire")
	public ModelAndView enseignantActiverFormulaire(HttpServletRequest request) throws IOException {

		/**
		 * ModelAndView envoie des objets a la vue et nous permet egalement d'y acceder
		 * enseignementSujets est la page jsp pour la partie enseignementSujets On va
		 * lui transmettre des objets
		 */
		ModelAndView view = new ModelAndView("enseignementSujets");

		/**
		 * On recupere les identifiants de l'enseignant et de l'encadrant
		 */
		int idEnseignement = Integer.valueOf(request.getParameter("idEnseignement"));
		int idEnseignant = Integer.valueOf(request.getParameter("idEnseignant"));
		EnseignementDao enDao = new EnseignementDao();
		int idEncadrant = enDao.getIdEnseignant(idEnseignement);
		request.getSession().setAttribute("idEnseignement", idEnseignement);
		/**
		 * On verifie qu'il s'agit bien de l'encadrant
		 */

		if (idEncadrant == idEnseignant) {
			/**
			 * Il a toujours acces aux menus
			 */
			SujetDao dao = new SujetDao();
			List<Sujet> sujets = dao.getSujetsEnseignement(idEnseignement);
			view.addObject("sujets", sujets);
			view.addObject("showMenu", true);
			view.addObject("idEnseignement", Integer.valueOf(request.getParameter("idEnseignement")));
			view.addObject("idEnseignant", Integer.valueOf(request.getParameter("idEnseignant")));

		} else {
			/**
			 * Sinon il n'a acces qu'a ces sujets en mode edit sans ajout
			 */
			SujetDao dao = new SujetDao();
			List<Sujet> sujets = dao.getSujetsEnseignementEnseignant(idEnseignement, idEnseignant);
			view.addObject("sujets", sujets);
			view.addObject("showMenu", false);
			view.addObject("idEnseignement", Integer.valueOf(request.getParameter("idEnseignement")));
			view.addObject("idEnseignant", Integer.valueOf(request.getParameter("idEnseignant")));

		}
		EnseignementDao enseignementDao = new EnseignementDao();
		Enseignement enseignement = enseignementDao.getEnseignement(idEnseignement);
		if (request.getParameter("desactiver") != null) {
			/**
			 * Si doit être desactiver On desactive le formulaire de l'enseignement
			 */

			enseignement.setFormulaire(0);
		} else {
			/**
			 * Si doit être activer On active le formulaire de l'enseignement
			 */
			enseignement.setFormulaire(1);

		}
		/** Etat formulaire **/
		int formulaire = enseignement.getFormulaire();

		/**
		 * On indique cela a la vue
		 */
		if (formulaire == 0) {
			view.addObject("formulaire", false);
		} else {
			view.addObject("formulaire", true);

		}
		/**
		 * On met a jour l'enseignement
		 */
		enseignementDao.updateEnseignement(idEnseignement, enseignement);
		return view;
	}

	/**
	 * Controller pour modifier un enseignement
	 * @param request requete http 
	 * @return vue modifierEnseignement
	 * @throws IOException une exception
	 */
	@RequestMapping("/modifierEnseignement")
	public ModelAndView modifierEnseignement(HttpServletRequest request) throws IOException {
		/**
		 * Si l'on accede a la page pour la premiere fois
		 */
		if (request.getParameter("enregistrer") == null) {
			ModelAndView view = new ModelAndView("modifierEnseignement");
			/**
			 * On recupere l'identfiant en session
			 */
			int idEnseignement = (Integer) request.getSession().getAttribute("idEnseignement");
			EnseignementDao enseignementDao = new EnseignementDao();
			Enseignement enseignement = enseignementDao.getEnseignement(idEnseignement);
			/**
			 * On renvoie les informations de l'enseignement a la vue
			 */
			view.addObject("enseignement", enseignement);
			return view;
		} else {
			/**
			 * Si les informations sont indiquées on retourne sur la page enseignant
			 */
			ModelAndView view = new ModelAndView("enseignant");

			try {
				/**
				 * On recupere l'enseignement en sesssion
				 */
				int idEnseignement = (Integer) request.getSession().getAttribute("idEnseignement");
				EnseignementDao enseignementDao = new EnseignementDao();
				Enseignement enseignementA = enseignementDao.getEnseignement(idEnseignement);

				/**
				 * On le modifie
				 */
				enseignementA.setCle(HtmlUtils.htmlEscape(request.getParameter("cle").trim()));
				enseignementA.setDescription(HtmlUtils.htmlEscape(request.getParameter("description").trim()));
				enseignementA.setFormulaire(Integer.valueOf(request.getParameter("formulaire")));
				enseignementA.setNbMaxScore(Integer.valueOf(request.getParameter("nbMaxScore")));
				enseignementA.setNbMaxTotal(Integer.valueOf(request.getParameter("nbMaxTotal")));
				enseignementA.setNom(HtmlUtils.htmlEscape(request.getParameter("nom").trim()));
				enseignementA.setTypeOptimisation(HtmlUtils.htmlEscape(request.getParameter("typeOptimisation").trim()));
				enseignementDao.updateEnseignement(idEnseignement, enseignementA);
				String email = "";
				if (request.getSession().getAttribute("email") != null) {

					email = HtmlUtils.htmlEscape(((String) request.getSession().getAttribute("email")).trim());
				}

				/** Enregistrement de la session de l'utilisateur */
				HttpSession session = request.getSession();
				session.setAttribute("email", email);

				/** Obtenir la liste de ses enseignements */
				EnseignantDao enseignantDao = new EnseignantDao();
				int idEnseignant = enseignantDao.getIdEnseignant(email);
				List<Enseignement> enseignements = enseignementDao.getEnseignements();

				/** Envoyer cette liste à la vue */
				view.addObject("enseignements", enseignements);
				view.addObject("idEnseignant", idEnseignant);
				view.addObject("success", true);
				view.addObject("failure", false);
			} catch (Exception e) {
				view.addObject("success", false);
				view.addObject("failure", true);
				return view;
			}

			return view;
		}

	}

	/**
	 * Controller pour créer un groupe
	 * @param request requete http
	 * @return vue enseignement
	 * @throws IOException une exception
	 */
	@RequestMapping("/creationGroupe")
	public ModelAndView creationGroupe(HttpServletRequest request) throws IOException {

		try {
			/** On recupere l'idEnseignement et on verifie le type de creation de groupe **/
			int idEnseignement = Integer.valueOf(request.getParameter("idEnseignement"));
			request.getSession().setAttribute("idEnseignement", idEnseignement);
			String typeCreation = request.getParameter("typeCreation");
			if (typeCreation.equals("Aucun")) {
				return enseignantEnseignement(request);

			} else if (typeCreation.equals("Choix")) {
				/**
				 * On recupere la liste des groupes ayant fait des souhaits
				 */
				List<Integer> groupes = new SouhaitDao().getSouhaitsGroupe(idEnseignement);
				int tailleMin = Integer.valueOf(request.getParameter("tailleMinGroupe"));
				int tailleMax = Integer.valueOf(request.getParameter("tailleMaxGroupe"));
				/**
				 * On cree un tableau de taille nbGroupe dont la premiere case sera l'id du
				 * groupe et la seconde sa taille
				 */
				int[][] tailleGroupes = new int[groupes.size()][2];
				for (int indGroupes = 0; indGroupes < groupes.size(); indGroupes++) {
					tailleGroupes[indGroupes][0] = groupes.get(indGroupes);
					tailleGroupes[indGroupes][1] = new MembreDuGroupeDao().getNbMembreGroupe(groupes.get(indGroupes));

				}
				/**
				 * On cree les groupe tant qu'il existe des groupes dont la taille est
				 * inferieure a taille Min
				 */
				boolean fini = false;
				while (fini == false) {
					fini = true;
					for (int indGroupes = 0; indGroupes < groupes.size(); indGroupes++) {

						if (tailleGroupes[indGroupes][1] < tailleMin) {
							/** On crée une liste de poid par groupes */
							List<Integer> poidsGroupes = new ArrayList<Integer>();
							fini = false;
							/**
							 * On cree une liste de groupes dans lesquels on pourra envoyer notre groupe
							 **/
							List<Integer> groupesPossibles = new ArrayList<Integer>();
							for (int indGroupes2 = 0; indGroupes2 < groupes.size(); indGroupes2++) {
								if (indGroupes != indGroupes2) {

									if ((tailleGroupes[indGroupes][1] + tailleGroupes[indGroupes2][1]) <= tailleMax) {
										groupesPossibles.add(tailleGroupes[indGroupes2][0]);
									}

								}
							}
							/** Si la liste n'est pas vide on choisit le premier **/
							if (!groupesPossibles.isEmpty()) {
								/**
								 * On recupere la liste des souhaits du groupe actuel
								 */
								List<Souhait> souhaitsGroupeActuel = new SouhaitDao().getSouhaits(idEnseignement,
										tailleGroupes[indGroupes][0]);
								/**
								 * On va comparer cette liste a la liste des souhaits pour chaque groupe
								 * possible et affecte un poid
								 **/
								for (int indGroupesPossible = 0; indGroupesPossible < groupesPossibles
										.size(); indGroupesPossible++) {
									List<Souhait> souhaitsGroupePossible = new SouhaitDao().getSouhaits(idEnseignement,
											groupesPossibles.get(indGroupesPossible));
									int dist = 0;
									if (!souhaitsGroupeActuel.isEmpty() && !souhaitsGroupePossible.isEmpty()) {
										for (int indice1 = 0; indice1 < souhaitsGroupeActuel.size(); indice1++) {
											int indSujet = souhaitsGroupeActuel.get(indice1).getIdSujet();
											boolean distIncrease = false;
											for (int indice2 = 0; indice2 < souhaitsGroupePossible.size(); indice2++) {
												if (indSujet == souhaitsGroupePossible.get(indice2).getIdSujet()) {
													distIncrease = true;
													dist = dist + Math.abs((souhaitsGroupeActuel.get(indice1).getScore()
															- souhaitsGroupePossible.get(indice2).getScore()));
												}
											}
											if (distIncrease == false) {
												dist = dist + souhaitsGroupeActuel.get(indice1).getScore();
											}
										}
										poidsGroupes.add(dist);
									} else {
										poidsGroupes.add(-1);
									}
								}
								/** On selectionne l'indice ayant le poid minimum */
								int MinPoid = 0;
								int indMinPoid = 0;
								for (int indpoid = 0; indpoid < poidsGroupes.size(); indpoid++) {
									if (poidsGroupes.get(indpoid) < MinPoid && poidsGroupes.get(indpoid) != -1) {
										MinPoid = poidsGroupes.get(indpoid);
										indMinPoid = indpoid;
									}
								}
								int groupeAffecte = groupesPossibles.get(indMinPoid);

								List<Integer> etudiants = new MembreDuGroupeDao()
										.etudiantsGroupe(tailleGroupes[indGroupes][0]);
								if (!etudiants.isEmpty()) {
									for (int idEtudiant : etudiants) {
										MembreDuGroupe mdg = new MembreDuGroupe();
										mdg.setIdEtudiant(idEtudiant);
										mdg.setIdGroupe(groupeAffecte);
										System.out
												.println("L'idEtudiant: " + idEtudiant + " idGroupe: " + groupeAffecte);
										new MembreDuGroupeDao().enregistrerMembreDuGroupe(mdg);
									}
									/**
									 * On supprime le groupe actuel et on met a jour la taille des groupes restant
									 **/
									new GroupeDao().deleteGroup(tailleGroupes[indGroupes][0]);
									System.out.println("Groupe delete: " + tailleGroupes[indGroupes][0]);
									groupes = new SouhaitDao().getSouhaitsGroupe(idEnseignement);
									tailleGroupes = new int[groupes.size()][2];
									for (int ind = 0; ind < groupes.size(); ind++) {
										tailleGroupes[ind][0] = groupes.get(ind);
										tailleGroupes[ind][1] = new MembreDuGroupeDao()
												.getNbMembreGroupe(groupes.get(ind));

									}
								} else {
									/** S'il n'existe pas de groupe auquel on peut affecte un membre on arrete **/
									fini = true;
								}
							}

						}

					}
				}
			} else if (typeCreation.equals("Hasard")) {

				/**
				 * On recupere la liste des groupes ayant fait des souhaits
				 */
				List<Integer> groupes = new SouhaitDao().getSouhaitsGroupe(idEnseignement);
				int tailleMin = Integer.valueOf(request.getParameter("tailleMinGroupe"));
				int tailleMax = Integer.valueOf(request.getParameter("tailleMaxGroupe"));

				/**
				 * On cree un tableau de taille nbGroupe dont la premiere case sera l'id du
				 * groupe et la seconde sa taille
				 */
				int[][] tailleGroupes = new int[groupes.size()][2];
				for (int indGroupes = 0; indGroupes < groupes.size(); indGroupes++) {
					tailleGroupes[indGroupes][0] = groupes.get(indGroupes);
					tailleGroupes[indGroupes][1] = new MembreDuGroupeDao().getNbMembreGroupe(groupes.get(indGroupes));

				}

				/**
				 * On cree les groupe tant qu'il existe des groupes dont la taille est
				 * inferieure a taille Min
				 */
				boolean fini = false;
				while (fini == false) {
					fini = true;
					for (int indGroupes = 0; indGroupes < groupes.size(); indGroupes++) {
						if (fini == true) {
							if (tailleGroupes[indGroupes][1] < tailleMin) {
								fini = false;
								/**
								 * On cree une liste de groupes dans lesquels on pourra envoyer notre groupe
								 **/
								List<Integer> groupesPossibles = new ArrayList<Integer>();
								for (int indGroupes2 = 0; indGroupes2 < groupes.size(); indGroupes2++) {
									if (indGroupes != indGroupes2) {

										if ((tailleGroupes[indGroupes][1]
												+ tailleGroupes[indGroupes2][1]) <= tailleMax) {
											groupesPossibles.add(tailleGroupes[indGroupes2][0]);
										}

									}
								}
								/** Si la liste n'est pas vide on choisit le premier **/
								if (!groupesPossibles.isEmpty()) {
									int groupeAffecte = groupesPossibles.get(0);
									/**
									 * On affecte tous les etudiants du groupe actuel a ce groupe
									 */
									List<Integer> etudiants = new MembreDuGroupeDao()
											.etudiantsGroupe(tailleGroupes[indGroupes][0]);
									if (!etudiants.isEmpty()) {
										for (int idEtudiant : etudiants) {
											MembreDuGroupe mdg = new MembreDuGroupe();
											mdg.setIdEtudiant(idEtudiant);
											mdg.setIdGroupe(groupeAffecte);
											System.out.println(
													"L'idEtudiant: " + idEtudiant + " idGroupe: " + groupeAffecte);
											new MembreDuGroupeDao().enregistrerMembreDuGroupe(mdg);
										}
										/**
										 * On supprime le groupe actuel et on met a jour la taille des groupes restant
										 **/
										new GroupeDao().deleteGroup(tailleGroupes[indGroupes][0]);
										System.out.println("Groupe delete: " + tailleGroupes[indGroupes][0]);
										groupes = new SouhaitDao().getSouhaitsGroupe(idEnseignement);
										tailleGroupes = new int[groupes.size()][2];
										for (int ind = 0; ind < groupes.size(); ind++) {
											tailleGroupes[ind][0] = groupes.get(ind);
											tailleGroupes[ind][1] = new MembreDuGroupeDao()
													.getNbMembreGroupe(groupes.get(ind));

										}
									}
								} else {
									/** S'il n'existe pas de groupe auquel on peut affecte un membre on arrete **/
									fini = true;
								}

							}
						}

					}
				}
			}

			return enseignantEnseignement(request).addObject("success", true);
		} catch (Exception e) {
			return enseignantEnseignement(request).addObject("failure", true);
		}

	}
}
