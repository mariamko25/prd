package prd.outilsAffectationProjet.controller;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.HtmlUtils;

import prd.outilsAffectationProjet.dao.*;

import prd.outilsAffectationProjet.model.*;

/**
 * Class gerant la notification
 * @author MariamKonate
 *
 */
@Controller
public class NotificationController {
	
	

	final String username = "outilsAffectation@gmail.com";    
      
    final String password = "@outilsAffectation1"; 
      
    final String host = "localhost"; 
	/**
	 * Methode permettant de notifier les etudiants
	 * @param request requete http
	 * @return vue notification 
	 * @throws IOException une exception
	 */
	@RequestMapping("/notification")
	public ModelAndView notification(HttpServletRequest request) throws IOException{
		
		Enseignement enseignement=new EnseignementDao().getEnseignement(Integer.valueOf(request.getParameter("idEnseignement")));
		Enseignant enseignant= new EnseignantDao().getEnseignant(enseignement.getIdEnseignant());
		Properties props = new Properties();
        // enable authentication 
        props.put("mail.smtp.auth", true); 

        // enable STARTTLS 
        props.put("mail.smtp.starttls.enable", "true"); 
        
        // Setup mail server 
        props.put("mail.smtp.host", "smtp.gmail.com");   
	   
        // TLS Port 
        props.put("mail.smtp.port", "587");  
        Session session = Session.getInstance(props, 
                new javax.mail.Authenticator() {                  
                  //override the getPasswordAuthentication method 
                  protected PasswordAuthentication  
                                 getPasswordAuthentication() { 
                                               
                      return new PasswordAuthentication(username,  
                                                       password); 
                  } 
                }); 

       
		try {
			String  listAffectationGroupeSujet= HtmlUtils.htmlEscape(request.getParameter("listAffectationGroupeSujet").trim());
			listAffectationGroupeSujet=listAffectationGroupeSujet.replaceAll("[{}]", " ");
			String affectations[]=listAffectationGroupeSujet.split(",");
			for(int i=0;i<affectations.length;i++)
			{
				List<Integer> idMembresGroupe=new MembreDuGroupeDao().etudiantsGroupe(Integer.valueOf(affectations[i].trim().split("=")[0]));
				for(Integer idEtudiant:idMembresGroupe)
				{
					Etudiant etudiant= new EtudiantDao().getEtudiant(idEtudiant);
					//String sujetEmail="Affectation";
					String messageEmail="Bonjour ceci est un mail automatique depuis l'outils affectation.Vous avez eu le sujet: "+new SujetDao().getSujet(Integer.valueOf(affectations[i].trim().split("=")[1])).getIntitule();
					//String destinataire=etudiant.getEmail();
					
					String toAddr = etudiant.getEmail();
			 
					// email subject
					String subject = enseignement.getNom();
			 
					// email body
					 // compose the message 
		            // javax.mail.internet.MimeMessage class is  
		            // mostly used for abstraction. 
		            Message message = new MimeMessage(session);     
		              
		            // header field of the header. 
		              
		            message.setRecipients(Message.RecipientType.TO, 
		                InternetAddress.parse(toAddr)); 
		            message.setSubject(subject); 
		            message.setText(messageEmail); 
		  
		            Transport.send(message);         //send Message 
		  
		            System.out.println("Done"); 
					
					

				}
				
			}
			ModelAndView modelViewObj = new ModelAndView("accueil").addObject("success",true);
		        return  modelViewObj;   
		}
		catch(Exception e)
		{
			ModelAndView modelViewObj = new ModelAndView("notification").addObject("failure", true);
			e.printStackTrace();
			System.out.println(e.getMessage());
	        return  modelViewObj;   

		}
	}

}
