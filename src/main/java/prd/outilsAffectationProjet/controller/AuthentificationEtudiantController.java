package prd.outilsAffectationProjet.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.HtmlUtils;

import prd.outilsAffectationProjet.dao.EnseignantDao;
import prd.outilsAffectationProjet.dao.EtudiantDao;
import prd.outilsAffectationProjet.model.Enseignant;
import prd.outilsAffectationProjet.model.Etudiant;

/**
 * Controller pour la vue authentification etudiant 
 * Elle met en place tout ce qui connection de l'etudiant
 * @author MariamKonate
 *
 */
@Controller
public class AuthentificationEtudiantController {
	
	
	
	/**
	 * Méthode pour afficher la page de connexion etudiant
	 * @param request requete http
	 * @return vue etudiant
	 * @throws IOException une exception
	 */	
	@RequestMapping("/authentificationEtudiant")
	public ModelAndView authentificationEtudiant(HttpServletRequest request) throws IOException {
		
		if(request.getParameter("email")==null && request.getParameter("motDePasse")==null && request.getSession().getAttribute("etudiantConnecte")==null)
		{
			ModelAndView view= new ModelAndView("connexionEtudiant");
			return view;
		}
		else
		{
			if(request.getSession().getAttribute("etudiantConnecte")!=null && request.getSession().getAttribute("etudiantConnecte").equals(true)) {
				return new EtudiantController().etudiant(request);
			}
			else if(authentification(request.getParameter("email"),request.getParameter("motDePasse")))
			{
				Etudiant etu=new EtudiantDao().getEtudiant( HtmlUtils.htmlEscape(request.getParameter("email").trim()));
				if(etu==null) {
					etu=new Etudiant();
					etu.setEmail(HtmlUtils.htmlEscape(request.getParameter("email").trim()));
					new	EtudiantDao().enregistrerEtudiant(etu);
				}
				return new EtudiantController().etudiant(request).addObject("success", true);
			}
			else
			{
				ModelAndView view= new ModelAndView("connexionEtudiant");
				return view.addObject("failure", true);
			}
		}
		
	}
	
	/**
	 * Méthode pour s'authentifier avec l'active directory
	 * @param email l'adresse mail de l'etudiant
	 * @param motDePasse mot de passe de l'etudiant
	 * @return vrai ou faux
	 */
	public boolean authentification(String email, String motDePasse)
	{
		if(email.equals("etu1") && motDePasse.equals("Pop2212!"))
		{
			return true;
		}
		else if(email.equals("etu2") && motDePasse.equals("Pop2212!"))
		{
			return true;
		}
		else if(email.equals("mariam.konate-2@etu.univ-tours.fr") && motDePasse.equals("Pop2212!"))
		{
			return true;
		}
		else if(email.equals("michelorel.ngatimo@etu.univ-tours.fr") && motDePasse.equals("Pop2212!"))
		{
			return true;
		}
		return false;
		
	}

}
