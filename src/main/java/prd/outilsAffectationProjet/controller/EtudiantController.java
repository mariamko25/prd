package prd.outilsAffectationProjet.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.HtmlUtils;

import prd.outilsAffectationProjet.dao.*;
import prd.outilsAffectationProjet.model.*;


/**
 * Class pour l'etudiant
 * @author MariamKonate
 *
 */
@Controller
public class EtudiantController {

	
	
	
	/**
	 * Deconnexion de la page  pour l'etudiant
	 * @param request requete http
	 * @return vue accueil
	 * @throws IOException une exception
	 */
	@RequestMapping("/etudiantDeconnexion")
	public ModelAndView etudiantDeconnexion(HttpServletRequest request) throws IOException {
		
		
		request.getSession().setAttribute("etudiantConnecte", null); 
		request.getSession().setAttribute("email", null); 

		
		return new AccueilController().acceuil(request);
	}
	
	/**
	 * Controller pour la page etudiant
	 * @param request requete http
	 * @return vue etudiant
	 * @throws IOException une exception
	 */
	@RequestMapping("/etudiant")
	public ModelAndView etudiant(HttpServletRequest request) throws IOException {

		/**
		 * ModelAndView envoie des objets a la vue et nous permet egalement d'y acceder
		 * etudiant est la page jsp pour la partie etudiant On va lui transmettre des
		 * objets
		 */

		ModelAndView view = new ModelAndView("etudiant");

		/**
		 * Après authentification on essaie d'obtenir les enseignements correspondant à
		 * l'enseignant
		 */
		String email;
		if (request.getParameter("email") == null && request.getSession().getAttribute("email")==null) {
			email = "";
		} else if(request.getSession().getAttribute("email")!=null){

			email = HtmlUtils.htmlEscape(((String)request.getSession().getAttribute("email")).trim());

		}
		else
		{
			email = HtmlUtils.htmlEscape(request.getParameter("email").trim());
		}

		request.getSession().setAttribute("etudiantConnecte", true); 

		/** Enregistrement de la session de l'utilisateur */
		HttpSession session = request.getSession();
		session.setAttribute("email", email);

		/** Obtenir la liste des enseignements ayant un formulaire */

		EnseignementDao enseignementDao = new EnseignementDao();
		List<Enseignement> enseignements = enseignementDao.getEnseignementsFormulaire();

		/** Envoyer cette liste à la vue */
		view.addObject("enseignements", enseignements);
		return view;
	}
	
	/**
	 * Affichage des différents enseignements pour l'étudiant
	 * @param request requete http
	 * @return vue formulaire voeux
	 * @throws IOException une exception
	 */
	@RequestMapping("/etudiantEnseignement")
	public ModelAndView etudiantEnseignement(HttpServletRequest request) throws IOException {

		/** Récupération de l'identifiant de l'enseignement */
		int idEnseignement = Integer.valueOf(request.getParameter("idEnseignement"));

		/** Récupération de la liste de sujets de cet enseignement */
		SujetDao dao = new SujetDao();
		List<Sujet> sujets = dao.getSujetsEnseignement(idEnseignement);
		EnseignementDao enDao = new EnseignementDao();
		Enseignement enseignement = enDao.getEnseignement(idEnseignement);
		String cleDonnee =HtmlUtils.htmlEscape( request.getParameter("cle").trim());
		if (enseignement.getCle().trim().equals(cleDonnee) || enseignement.getCle()==null) {
			/**
			 * ModelAndView envoie des objets a la vue et nous permet egalement d'y acceder
			 * formulaireVoeux est la page jsp pour la partie formulaire On va lui
			 * transmettre des objets
			 */
			ModelAndView view = new ModelAndView("formulaireVoeux");
			view.addObject("sujets", sujets);
			view.addObject("enseignement", enseignement);
			view.addObject("idEnseignement", idEnseignement);

			return view.addObject("success", true).addObject("failure", false);
		}
		return etudiantCleEnseignement(request).addObject("success", false).addObject("failure", true);

	}

	/**
	 * Controller pour ouvrir un enseignement l'étudiant renseigne 
	 * la clé
	 * @param request requete http
	 * @return vue cleEnseignement
	 * @throws IOException une exception
	 */
	@RequestMapping("/cleEnseignement")
	public ModelAndView etudiantCleEnseignement(HttpServletRequest request) throws IOException {

		/**
		 * ModelAndView envoie des objets a la vue et nous permet egalement d'y acceder
		 * cleEnseignement est la page jsp pour la partie cleEnseignement On va lui
		 * transmettre des objets
		 */
		ModelAndView view = new ModelAndView("cleEnseignement");
		int idEnseignement = Integer.valueOf(request.getParameter("idEnseignement"));
		view.addObject("idEnseignement", idEnseignement);

		return view;
	}
	
	/**
	 * Controller permettant à un étudiant d'émettre des souhaits
	 * @param request requete http
	 * @return vue etudiant
	 * @throws IOException une exception
	 */
	@RequestMapping("/etudiantSouhait")
	public ModelAndView etudiantSouhait(HttpServletRequest request) throws IOException {
		try {
				if(request.getParameter("groupeEmails")==null || request.getParameter("groupeEmails").isEmpty())
				{
					throw new Exception("Pas d'adresse mail");
				}
		
				/** On recupere l'identifiant de l'enseignement */
				int idEnseignement = Integer.valueOf(request.getParameter("idEnseignement"));	
				Enseignement ens=new EnseignementDao().getEnseignement(idEnseignement);
				/** nous recuperons la liste des sujets*/
				List<Sujet> sujets= new SujetDao().getSujetsEnseignement(idEnseignement);
				/** Nous avons une liste de score pour les enseignements*/
				List<Integer> listScore= new ArrayList<Integer>();
				/** On verifie si l'etudiant n'a pas dépassé le max de score*/
				int maxScoreTotal=0;
				for(Integer i:listScore)
				{
					maxScoreTotal=maxScoreTotal+i;
				}
				if(maxScoreTotal>ens.getNbMaxTotal())
				{
					throw new Exception("Trop de points sur les enseignement");
				}
				for(int indice=0;indice<sujets.size();indice++)
				{
					listScore.add(Integer.valueOf(request.getParameter("score-"+indice)));
				}
		
				/** Nous créons un groupe pour cet etudiant puis on l'enregistre*/
				Groupe groupeEtudiant= new Groupe();
				GroupeDao groupeDao=new GroupeDao();
				groupeDao.enregistrerGroupe(groupeEtudiant);
				groupeEtudiant = groupeDao.getGroupes().get(groupeDao.getGroupes().size() - 1);
				String emails=HtmlUtils.htmlEscape(request.getParameter("groupeEmails"));
				emails=emails.replace('[', ' ');
				emails=emails.replace('"', ' ');
				emails=emails.replace(']', ' ');
				emails=emails.trim();
				String[] emailsGroupe=emails.split(",");
				/**
				 * Insertion dans la base de données inclus création d'étudiants ou de sujet
				 */
				EtudiantDao etuDao = new EtudiantDao();
				for (int i = 0; i < emailsGroupe.length; i++) {
					/** Si l'etudiant n'existe pas on le cree **/
					if (etuDao.getEtudiant(emailsGroupe[i]) == null) {
						Etudiant etu = new Etudiant();
						etu.setEmail(emailsGroupe[i]);
						etuDao.enregistrerEtudiant(etu);
					}
					/** On recupere l'etudiant ayant l'adresse mail **/
					Etudiant etudiant = etuDao.getEtudiant(emailsGroupe[i]);
		
					/** On l'enregistre en tant que membre du groupe **/
					MembreDuGroupeDao mdgDao = new MembreDuGroupeDao();
					MembreDuGroupe mdg = new MembreDuGroupe();
					mdg.setIdEtudiant(etudiant.getIdEtudiant());
					mdg.setIdGroupe(groupeEtudiant.getIdGroupe());
					mdgDao.enregistrerMembreDuGroupe(mdg);
			
					for(Sujet sujet:sujets)
					{
						/** On cree un souhait **/
						Souhait sht = new Souhait();
						/** On lui donne comme idGroupe **/
						sht.setIdGroupe(groupeEtudiant.getIdGroupe());
						sht.setIdSujet(sujet.getIdSujet());
						sht.setScore(listScore.get(0));
						listScore.remove(0);
						new SouhaitDao().enregistrerSouhait(sht);
					}
		
		
			}	
			return etudiant(request).addObject("success", true).addObject("failure",false);
	}catch(Exception e)
	{
		/** On recupere l'identifiant de l'enseignement */
		int idEnseignement = Integer.valueOf(request.getParameter("idEnseignement"));	
		/** nous recuperons la liste des sujets*/
		List<Sujet> sujets= new SujetDao().getSujetsEnseignement(idEnseignement);
		Enseignement enseignement= new EnseignementDao().getEnseignement(idEnseignement);
		/**
		 * ModelAndView envoie des objets a la vue et nous permet egalement d'y acceder
		 * formulaireVoeux est la page jsp pour la partie formulaire On va lui
		 * transmettre des objets
		 */
		ModelAndView view = new ModelAndView("formulaireVoeux");
		view.addObject("failure",true);
		view.addObject("success",false);
		view.addObject("sujets", sujets);
		view.addObject("enseignement", enseignement);
		view.addObject("idEnseignement", idEnseignement);
		return view.addObject("success", false).addObject("failure",true);
	}
	
	}
}
