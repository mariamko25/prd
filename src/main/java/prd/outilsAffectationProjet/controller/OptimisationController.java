package prd.outilsAffectationProjet.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.HtmlUtils;

import com.quantego.clp.CLP;
import com.quantego.clp.CLPExpression;
import com.quantego.clp.CLPVariable;

import prd.outilsAffectationProjet.dao.*;

import prd.outilsAffectationProjet.model.*;
import com.google.gson.Gson;

/**
 * Class gerant la partie optimisation
 * @author MariamKonate
 *
 */
@Controller
public class OptimisationController {
	
	/**
	 * Methode permettant d'afficher la page optimisation
	 * @param request requete http
	 * @return vue optimisation
	 * @throws IOException une exception
	 */
	@RequestMapping("/optimisation")
	public ModelAndView optimisation(HttpServletRequest request) throws IOException{
		
		/**
		 *  ModelAndView  envoie des objets a la vue et nous permet egalement 
		 *  d'y acceder optimisation est la page jsp pour la partie enseignement On va lui transmettre des objets
		 */
		ModelAndView view= new ModelAndView("optimisation");
		
		/** On envoie les infos sur l'enseignement a la vue*/
		int idEnseignement=Integer.valueOf(request.getParameter("idEnseignement"));
		request.getSession().setAttribute("idEnseignement", idEnseignement);
		EnseignementDao dao=new EnseignementDao();
		Enseignement enseignement=dao.getEnseignement(Integer.valueOf(idEnseignement));
		view.addObject("enseignement",enseignement);
		
		/** On recupere des informations sur les encadrants de sujet de cet enseignement */
		List<Encadrant> encadrants=new EncadrantDao().getEncadrantEnseignement(idEnseignement);
		List<Enseignant> enseignants= new ArrayList<Enseignant>();
		/** On essaie de savoir pour chaque enseignant le nombre max de sujet qu'il peut encadrer*/
		List<Integer> nbMaxSujet= new ArrayList<Integer>();
		List<Integer> nbMinSujet= new ArrayList<Integer>();
		boolean skip=false;
		for(int ind=0;ind<encadrants.size();ind++)
		{
			skip=false;
			int idEncadrant=encadrants.get(ind).getIdEnseignant();
			for(Enseignant ens:enseignants)
			{
				if(ens.getIdEnseignant()==idEncadrant)
				{
					skip=true;
				}
			}
			if(skip!=true)
			{
				Enseignant enseignant= new EnseignantDao().getEnseignant(idEncadrant);
				enseignants.add(enseignant);
				if(new EnseignantNbMaxSujetsDao().getEnseignantNbMaxSujets(enseignant.getIdEnseignant(), idEnseignement)==null)
				{
					EnseignantNbMaxSujets enseignantNbMaxSujets= new EnseignantNbMaxSujets();
					enseignantNbMaxSujets.setIdEnseignant(enseignant.getIdEnseignant());
					enseignantNbMaxSujets.setIdEnseignement(idEnseignement);
					enseignantNbMaxSujets.setNombreSujetMax(10);
					enseignantNbMaxSujets.setNombreSujetMin(1);
					new EnseignantNbMaxSujetsDao().enregistrerEnseignantNbMaxSujets(enseignantNbMaxSujets);
				}
				nbMaxSujet.add(new EnseignantNbMaxSujetsDao().getEnseignantNbMaxSujets(enseignant.getIdEnseignant(), idEnseignement).getNombreSujetMax());
				nbMinSujet.add(new EnseignantNbMaxSujetsDao().getEnseignantNbMaxSujets(enseignant.getIdEnseignant(), idEnseignement).getNombreSujetMin());

			}
			
		}
		view.addObject("enseignants",enseignants);
		view.addObject("nbMaxSujet",nbMaxSujet);
		view.addObject("nbMinSujet",nbMinSujet);
		return view;
		
	}

	
	/**
	 * Methode permettant d'afficher la page validerOptimisation
	 * @param request requete http
	 * @return vue notification
	 * @throws IOException une exception
	 */
	@RequestMapping("/validerOptimisation")
	public ModelAndView validerOptimisation(HttpServletRequest request) throws IOException{
		Enseignement enseignement=new EnseignementDao().getEnseignement(Integer.valueOf(request.getParameter("idEnseignement")));
		String  listAffectationGroupeSujet= HtmlUtils.htmlEscape(request.getParameter("listSolution").trim());

		try {
			ModelAndView view= new ModelAndView("notification");
			listAffectationGroupeSujet=listAffectationGroupeSujet.replaceAll("[{}]", " ");
			String affectations[]=listAffectationGroupeSujet.split(",");
			for(int i=0;i<affectations.length;i++)
			{
				Affectation affectation=new Affectation();
				affectation.setIdGroupe(Integer.valueOf(affectations[i].trim().split("=")[0]));
				affectation.setIdSujet(Integer.valueOf(affectations[i].trim().split("=")[1]));
				String nomEncadrant =new EnseignantDao().getEnseignantSujet(Integer.valueOf(affectations[i].trim().split("=")[1])).getNom();
				affectation.setNomEncadrant(nomEncadrant);
				new AffectationDao().enregistrerAffectation(affectation);
			}
			List<Affectation> affectationsVue=new AffectationDao().getAffectations(enseignement.getIdEnseignement());
			view.addObject("success",true);
			view.addObject("enseignement",enseignement);
			view.addObject("affectations",affectationsVue);
			view.addObject("listAffectationGroupeSujet",listAffectationGroupeSujet);
			return view;
		}
		catch(Exception e)
		{
			ModelAndView view= new ModelAndView("notification");
			view.addObject("failure",true);
			List<Affectation> affectationsVue=new AffectationDao().getAffectations(enseignement.getIdEnseignement());
			view.addObject("enseignement",enseignement);
			view.addObject("affectations",affectationsVue);
			view.addObject("listAffectationGroupeSujet",listAffectationGroupeSujet);
			
			return view;
		}
		
		
	}
	/**
	 * Methode permettant de modifier le nombre max de sujet qu'un 
	 * enseignant peut encadrer
	 * @param request requete http
	 * @return vue optimisation
	 * @throws IOException une exception
	 */
	@RequestMapping("/modifierEnseignantNbMaxMinSujets")
	public ModelAndView modifierEnseignantNbMaxMinSujets(HttpServletRequest request) throws IOException{
		
		try {
			/** On requiert les informations necessaire pour update le nbMaxSujets*/
			int idEnseignant= Integer.valueOf(request.getParameter("idEnseignant"));
			int idEnseignement=Integer.valueOf(request.getParameter("idEnseignement"));
			int nbSujetMax=Integer.valueOf(request.getParameter("nbSujetMax"));
			int nbSujetMin=Integer.valueOf(request.getParameter("nbSujetMin"));
			EnseignantNbMaxSujets enseignantNbMaxSujets= new EnseignantNbMaxSujets();
			enseignantNbMaxSujets.setIdEnseignant(idEnseignant);
			enseignantNbMaxSujets.setIdEnseignement(idEnseignement);
			enseignantNbMaxSujets.setNombreSujetMax(nbSujetMax);
			enseignantNbMaxSujets.setNombreSujetMin(nbSujetMin);
			new EnseignantNbMaxSujetsDao().updateEnseignantNbMaxSujets(enseignantNbMaxSujets);

			return optimisation(request).addObject("success", true);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
			return optimisation(request).addObject("failure", true);
		}
		
		

		
	}
	
		
	/**
	 * MEthode permettant d'automatiser l'affectation	
	 * @param request requete http
	 * @return vue resultats
	 * @throws IOException une exception
	 */
	@RequestMapping("/resultats")
	public ModelAndView optimisationResultats(HttpServletRequest request) throws IOException{
		  String dataPoints="";
		  String dataPoints2="";
	  try {
		  	//La pire affectation
			int Bmin=Integer.valueOf(request.getParameter("pireAffectation"));
			//L'identifiant de l'enseignement
			int idEnseignement= Integer.valueOf(request.getParameter("idEnseignement"));
			//Librairie pour résoudre le problème 
			CLP model = new CLP();
			//Récupération de la liste des souhaits, de la liste des groupes, des sujets et des encadrants
			List<Souhait> listSouhaits=new SouhaitDao().getSouhaits(idEnseignement);
			List<Integer> listIdGroupes=new SouhaitDao().getSouhaitsGroupe(idEnseignement);
			List<Sujet> listSujets= new SujetDao().getSujetsEnseignement(idEnseignement);
			List<Encadrant> listEncadrants= new EncadrantDao().getEncadrantEnseignement(idEnseignement);
			//Obtenir le nombre de groupe et le nombre de sujets
			int nbGroupe = listIdGroupes.size();
			int nbSujet = listSujets.size();
			//Tableau dans lequel on stocke les scores donnés à chaque sujet
			double [][] valeurs= new double[nbGroupe][nbSujet];
			
			
			/** Renseignement des scores pour chaque souhaits*/
			for (int i=0; i<nbGroupe; i++) {
				for (int j=0; j<nbSujet; j++)
				{
					for(int ind=0;ind<listSouhaits.size();ind++)
					{
						if(listSouhaits.get(ind).getIdGroupe()==listIdGroupes.get(i) && listSouhaits.get(ind).getIdSujet()==listSujets.get(j).getIdSujet()) {
							valeurs[i][j]=listSouhaits.get(ind).getScore();
						}
					}
					
				}

			}
			
			/** Declaration variables avec les coeffiscients obj*/
			
			CLPVariable[][] assignment = new CLPVariable[nbGroupe][nbSujet];
			for (int i=0; i<nbGroupe; i++) {
				for (int j=0; j<nbSujet; j++)
				{
					assignment[i][j] = model.addVariable().bounds(0.0, 1.0).obj(valeurs[i][j]);
				}
			}
			
			/**
			*	Declaration contraintes
			*/

			/** Chaque groupe est affecté à un seul sujet */
			for (int i=0; i<nbGroupe; i++) {
				CLPExpression expression=model.createExpression();
				for (int j=0; j<nbSujet; j++)
				{
					expression.add(assignment[i][j]);
				}
				expression.eq(1.0);
			}
			
			/** Chaque groupe a Bmin comme score min */
			for (int i=0; i<nbGroupe; i++) {
				CLPExpression expression=model.createExpression();
				for (int j=0; j<nbSujet; j++)
				{
					expression.add(valeurs[i][j],assignment[i][j]);
				}
				expression.geq(Bmin);
			}
			/** Chaque sujet peut etre pris au max nbCopie de fois*/
			for (int i=0; i<nbSujet; i++) {
				CLPExpression expression=model.createExpression();
				for (int j=0; j<nbGroupe; j++)
				{
					expression.add(assignment[j][i]);
				}
				
					expression.leq(listSujets.get(i).getNbCopie());
				
				
			}

			boolean gotVar=false;
			/** Chaque enseignant peut encadrer que nbMaxSujets */
			while(!listEncadrants.isEmpty())
			{
				int idEncadrant=listEncadrants.get(0).getIdEnseignant();
				CLPExpression expression=model.createExpression();
				gotVar=false;
				for (int i=0; i<nbSujet; i++) {
					if(new EncadrantDao().getEncadrant(listSujets.get(i).getIdSujet())!=null)
					{
						if(new EncadrantDao(). getEncadrant(listSujets.get(i).getIdSujet()).getIdEnseignant()==idEncadrant)
						{
							for (int j=0; j<nbGroupe; j++)
							{
								gotVar=true;
								expression.add(assignment[j][i]);
							}
						}
					}
					
								
				}
				
				if(gotVar)
				{
					EnseignantNbMaxSujets enseignantNbMaxSujets=new EnseignantNbMaxSujetsDao().getEnseignantNbMaxSujets(idEncadrant,idEnseignement);
					if(enseignantNbMaxSujets!=null)
					{
						expression.leq(enseignantNbMaxSujets.getNombreSujetMax());
					}
					else
					{
						expression.leq(10);
					}
					
				}
				
				listEncadrants.remove(0);

			}
			
			listEncadrants= new EncadrantDao().getEncadrantEnseignement(idEnseignement);
			gotVar=false;
			/** Chaque enseignant peut encadrer au minimum nbMinSujets */
			while(!listEncadrants.isEmpty())
			{
				int idEncadrant=listEncadrants.get(0).getIdEnseignant();
				CLPExpression expression=model.createExpression();
				gotVar=false;
				for (int i=0; i<nbSujet; i++) {
					if(new EncadrantDao().getEncadrant(listSujets.get(i).getIdSujet())!=null)
					{
						if(new EncadrantDao(). getEncadrant(listSujets.get(i).getIdSujet()).getIdEnseignant()==idEncadrant)
						{
							for (int j=0; j<nbGroupe; j++)
							{
								gotVar=true;
								expression.add(assignment[j][i]);
							}
						}
					}
					
								
				}
				
				if(gotVar)
				{
					EnseignantNbMaxSujets enseignantNbMaxSujets=new EnseignantNbMaxSujetsDao().getEnseignantNbMaxSujets(idEncadrant,idEnseignement);
					if(enseignantNbMaxSujets!=null)
					{
						expression.geq(enseignantNbMaxSujets.getNombreSujetMin());
					}
					else
					{
						expression.geq(0);
					}
					
				}
				
				listEncadrants.remove(0);

			}

			/** Resolution */
			CLP.STATUS ret=model.maximize();
			
			/** Affichage */
		    System.out.println(model.toString());
		    System.out.println(ret);
		    System.out.println(model.getObjectiveValue());
		    for(int i=0;i<nbGroupe;i++) 
		    {
			    	for(int j=0;j<nbSujet;j++) 
			    	{
				   System.out.println("solution: "+i+"-"+j+" "+model.getSolution(assignment[i][j]));
			    	}

		    }
		
		    HashMap<Integer,Integer>  listAffectationGroupeSujet=new  HashMap<Integer,Integer>();

			
			//Classement voeux
			double [][] classeVoeux=new double[nbGroupe][nbSujet];
			
			//Je parcours chaque groupe
			for(int i=0;i<nbGroupe;i++) {
				double rang=1;
				int indice=0;
				//Tableau indiquant quelle case est triée
				boolean [] trie= new boolean[nbSujet];
				Arrays.fill(trie, false);
				boolean end=false;
				//Tant que les case du groupe ne sont pas triée
				while(end==false) {
					//Je met le score max à 0
					double max= 0;
					end=true;
					//Je parcours tous les sujets
					for(int j=0;j<nbSujet;j++) {
						//Si le sujet n'est pas trié
						if(trie[j]!=true) {
							//Je verifie si son score est Supérieu ou égal à max
							if(valeurs[i][j]>=max) {
								//Je change max et je prend le nouvel indice 
								max=valeurs[i][j];
								indice=j;
							}
						}						
					}
					//Je déclare que l'indice de ce sujet est trié
					trie[indice]=true;
					//je met le rang
					classeVoeux[i][indice]=rang;
					// Je met tous les sujets ayant le meme rang
					for(int j=0;j<nbSujet;j++) {
						if(trie[j]!=true) {
							if(valeurs[i][j]==max) {
								classeVoeux[i][j]=rang;
								trie[j]=true;
							}
						}
					}
					
					//J'incrémente le rang
					rang++;
					//Je verifie que c'est pas trié
					for(int ind=0;ind<nbSujet;ind++) {
						if(trie[ind]!=true) {
							end=false;
						}
					}
				}
				
						
			}
			 int [] rangs= new int[nbSujet];
			 int [] occSujets= new int[nbSujet];
			 Arrays.fill(rangs, 0);
			 Arrays.fill(occSujets, 0);
			//Calcul des occurrences
			 for(int i=0;i<nbGroupe;i++) {
				 for(int j=0;j<nbSujet;j++) {
					
		    			 if((int)model.getSolution(assignment[i][j])==1)
					 {
		    				 rangs[(int) (classeVoeux[i][j]-1)]=rangs[(int) (classeVoeux[i][j]-1)]+1;
		    				 occSujets[j]=occSujets[j]+1;
						 listAffectationGroupeSujet.put(listIdGroupes.get(i), listSujets.get(j).getIdSujet());


					 }
				 }
			 }
				   
			
		   
		   
		    listSujets= new SujetDao().getSujetsEnseignement(idEnseignement);
			listEncadrants= new EncadrantDao().getEncadrantEnseignement(idEnseignement);
		    //Affichage histogramme enseignants
			Gson gsonObj = new Gson();
			Map<Object,Object> map = null;
			List<Map<Object,Object>> list = new ArrayList<Map<Object,Object>>();
			
			for (int i=0; i<nbSujet;i++) {
			    
	    			Enseignant ens=new EnseignantDao().getEnseignantSujet(listSujets.get(i).getIdSujet());
	    			String nomEncadrant="";
	    			if(ens!=null) {
	    				nomEncadrant=ens.getNom();
	    			}
	    			Map<Object,Object> map2 = new HashMap<Object,Object>();
	    			map2.put("label", nomEncadrant); 
	    			map2.put("y", occSujets[i]); 
	    			list.add(map2);
			    		
			    		
			}
			 
		    dataPoints = gsonObj.toJson(list);
		   
		    //Affichage histogramme etudiants
			List<Map<Object,Object>> list2 = new ArrayList<Map<Object,Object>>();
			 gsonObj = new Gson();
			 map = null;
			for(int j=0;j<nbSujet;j++)
			{
				map = new HashMap<Object,Object>();
				map.put("label", "Voeux "+(j+1)); 
				map.put("y", rangs[j]); 
				list2.add(map);
			}
			dataPoints2 = gsonObj.toJson(list2);
			Enseignement enseignement= new EnseignementDao().getEnseignement(idEnseignement);
			ModelAndView view=new ModelAndView("affichage");
			view.addObject("dataPoints", dataPoints);
			view.addObject("dataPoints2", dataPoints2);
			view.addObject("nomEnseignement", enseignement.getNom());
			view.addObject("pireAffectation",Bmin);
			view.addObject("idEnseignement", idEnseignement);
			if(ret==CLP.STATUS.OPTIMAL)
			{
				view.addObject("solution", "Solution Optimale");
				view.addObject("ok",true);
				view.addObject("listSolution", listAffectationGroupeSujet);
				System.out.println(listAffectationGroupeSujet);
					return view;

			}
		    return view.addObject("solution", "Solution Impossible");

		}
		catch(Exception e)
		{
			System.out.println("blabla");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return new ModelAndView("affichage").addObject("dataPoints", dataPoints);

		}
		
		
	}
	
	/**
	 * Methode permettant de calculer le nombre d'occurence 
	 * d'un nombre dans une liste
	 * @param arr un array de doubles
	 * @param x un entier 
	 * @return le nombre d'occurence
	 */
	public int countOccurrences(List<Double> arr,int x) 
    { 
        int res = 0; 
        for (int i=0; i<arr.size(); i++) 
            if (arr.get(i)== x) 
            {
                res++; 
            }
        return res; 
    } 
}
