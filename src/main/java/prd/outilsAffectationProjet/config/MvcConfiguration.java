package prd.outilsAffectationProjet.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
/**
 * Classe de configuration pour les vues et les ressources
 * Nous indiquons ou se trouvent les dossiers afin qu'ils soient cherchés
 * @author MariamKonate
 *
 */
@Configuration
@ComponentScan(basePackages="prd.outilsAffectationProjet")
@EnableWebMvc
public class MvcConfiguration extends WebMvcConfigurerAdapter{

	//Dossier des vues
	@Bean
	public ViewResolver getViewResolver(){
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		return resolver;
	}
	
	//Dossier ressource ou l'on retrouve les jar comme bootstrap jquery etc..
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
		registry
        .addResourceHandler("/webjars/**")
        .addResourceLocations("/webjars/");
	}
	
	@Bean(name = "multipartResolver")
	public CommonsMultipartResolver multipartResolver() {
	    CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
	    multipartResolver.setMaxUploadSize(100000);
	    return multipartResolver;
	}
	
	
}
