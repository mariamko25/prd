package prd.outilsAffectationProjet.config;

import java.util.Properties;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import prd.outilsAffectationProjet.model.*;


/**
 * Classe de configuration pour hibernate
 * Ici on stocke les classes liées à la base de données
 * Les classe sont liée à l'aide d'annotations
 * @author MariamKonate
 *
 */
public class HibernateConfiguration {
	
	private static SessionFactory sessionFactory;
    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                Configuration configuration = new Configuration();
                
                // Hibernate settings equivalent à hibernate.cfg.xml's properties
                Properties settings = new Properties();
                //Tous les paramètres mdp user etc..
                settings.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
                settings.put(Environment.URL, "jdbc:mysql://localhost:8889/PRD");
                settings.put(Environment.USER, "root");
                settings.put(Environment.PASS, "root");
                settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
                settings.put(Environment.SHOW_SQL, "true");
                settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
                settings.put(Environment.HBM2DDL_AUTO, "update");
                
                //Les classes de mapping à la bdd
                configuration.setProperties(settings);
                configuration.addAnnotatedClass(Etudiant.class);
                configuration.addAnnotatedClass(Sujet.class);
                configuration.addAnnotatedClass(Enseignement.class);
                configuration.addAnnotatedClass(Enseignant.class);
                configuration.addAnnotatedClass(Souhait.class);
                configuration.addAnnotatedClass(Encadrant.class);
                configuration.addAnnotatedClass(Groupe.class);
                configuration.addAnnotatedClass(MembreDuGroupe.class);
                configuration.addAnnotatedClass(Affectation.class);
                configuration.addAnnotatedClass(EnseignantNbMaxSujets.class);
                //Unique session factory
                ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties()).build();
                sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sessionFactory;
    }

}
