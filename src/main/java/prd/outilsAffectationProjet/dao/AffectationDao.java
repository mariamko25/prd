package prd.outilsAffectationProjet.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import prd.outilsAffectationProjet.config.HibernateConfiguration;
import prd.outilsAffectationProjet.model.Affectation;
import prd.outilsAffectationProjet.model.Souhait;

/**
 * Classe data access object
 * Pour Affectation
 * @author MariamKonate
 *
 */
public class AffectationDao {
	
	
	/**
	 * Enregistrer une affectation dans la base
	 * @param affectation un objet affectation
	 */
	public void enregistrerAffectation(Affectation affectation) {
        Transaction transaction = null;
        Session session = HibernateConfiguration.getSessionFactory().openSession();
            
        		// start a transaction
            transaction = session.beginTransaction();
            // save the membregroupe object
            
            session.save(affectation);
            // commit transaction
            transaction.commit();
        
    }
	/**
	 * Supprimer la liste des affectations
	 */
    public void deleteAffectations() {
    	Transaction transaction = null;
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Affectation> affectations = session.createQuery("from Affectation", Affectation.class).list();
		if(!affectations.isEmpty())
		{
			for (Affectation affectation : affectations) {
				transaction = session.beginTransaction();
				session.delete(affectation);
				transaction.commit();
			}
		}       
    }
    
    
    /**
	 * Obtenir la liste des affectations
	 * @return liste affectation
	 */
    public List < Affectation > getAffectations() {
    	Session session=HibernateConfiguration.getSessionFactory().openSession();
    	return session.createQuery("from Affectation", Affectation.class).list();        
    }
    
    /**
   	 * Obtenir la liste des affectations
   	 * pour un enseignement
   	 * @param idEnseignement id de l'enseignement
   	 * @return la liste des affectations de cet enseignement
   	 */
       public List < Affectation > getAffectations(int idEnseignement) {
       	Session session=HibernateConfiguration.getSessionFactory().openSession();
       	return session.createQuery("from Affectation where idSujet in(select idSujet from Sujet where idEnseignement = :idEnseignement)", Affectation.class).setParameter("idEnseignement", idEnseignement).list();        
       }
}
