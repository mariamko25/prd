package prd.outilsAffectationProjet.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import prd.outilsAffectationProjet.config.HibernateConfiguration;
import prd.outilsAffectationProjet.model.*;

/**
 * Classe data access object
 * Pour Enseignant
 * @author MariamKonate
 *
 */
public class EnseignantDao {
	
	/**
	 * Enregistrer enseignant dans la base
	 * @param enseignant un objet Enseignant
	 */
	public void enregistrerEnseignant(Enseignant enseignant) {
        Transaction transaction = null;
        Session session = HibernateConfiguration.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the enseignant object
            session.save(enseignant);
            // commit transaction
            transaction.commit();
        
    }
	
	/**
	 * Obtenir l'identifiant d'un enseignant a partir de son adresse
	 * mail
	 * @param email l'adresse email de l'enseignant
	 * @return l'identifiant de l'enseignant
	 */
	public int getIdEnseignant(String email)
	{
		int id=-1;
		Session session=HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> list= session.createQuery("select E.idEnseignant from Enseignant E where lower(E.email) = :email", Integer.class).setParameter("email", email.trim().toLowerCase()).list();
    		if(!list.isEmpty()) {
    			id=list.get(0);
    		}
		return id;
	}
	
	/**
	 * Obtenir la liste des enseignants
	 * @return la liste des enseignants
	 */
    public List < Enseignant > getEnseignants() {
    	
    	Session session=HibernateConfiguration.getSessionFactory().openSession();
    	return session.createQuery("from Enseignant", Enseignant.class).list();
        
    }
    
    /**
     * Obtenir la liste des enseignants en limitant 
     * par l'entier limit
     * @param limit la taille limite de la liste d'enseignant
     * @return la liste d'enseignant limitée par limit
     */
	public List < Enseignant > getEnseignants(int limit) {
	    	
	    	Session session=HibernateConfiguration.getSessionFactory().openSession();
	    	return session.createQuery("from Enseignant order by idEnseignant desc", Enseignant.class).setMaxResults(limit).list();
	        
	    }
	
	 /**
     * Obtenir un enseignant a partir de son id
     * 
     * @param idEnseignant l'id de l'enseignant
     * @return  l'enseignant portant l'id idEnseignant
     */
	public  Enseignant  getEnseignant(int idEnseignant) {
	    	
	    	Session session=HibernateConfiguration.getSessionFactory().openSession();
	    	 List<Enseignant> enseignants=session.createQuery("from Enseignant where idEnseignant = :idEnseignant order by idEnseignant desc", Enseignant.class).setParameter("idEnseignant", idEnseignant).list();
	    	 if(enseignants.isEmpty())
	    	 {
	    		 return null;
	    	 }
	    	return enseignants.get(0);
	    }
	
	 /**
     * Obtenir un enseignant a partir de adresseMail
     * 
     * @param email l'email de l'enseignant
     * @return  l'enseignant portant l'email
     */
	public  Enseignant  getEnseignant(String email) {
	    	
	    	Session session=HibernateConfiguration.getSessionFactory().openSession();
	    	 List<Enseignant> enseignants=session.createQuery("from Enseignant where lower(email) = :email order by idEnseignant desc", Enseignant.class).setParameter("email", email.toLowerCase().trim()).list();
	    	 if(enseignants.isEmpty())
	    	 {
	    		 return null;
	    	 }
	    	return enseignants.get(0);
	    }
	
	/**
	 * Obtenir l'enseignant responsable du sujet dont l'id est idSujet
	 * @param idSujet l'id du sujet
	 * @return l'enseignant encadrant ce sujet
	 */
	public Enseignant getEnseignantSujet(int idSujet)
    {
     	Session session=HibernateConfiguration.getSessionFactory().openSession();
     	List<Enseignant> list=session.createQuery("from Enseignant where idEnseignant in(select idEnseignant from Encadrant where idSujet = :idSujet)", Enseignant.class).setParameter("idSujet", idSujet).list();           	
     	if(list.isEmpty())
     	{     	
     		return null;          	
     		
     	}
     	
     	return list.get(0);
    }
	/**
	 * Supprimer la liste des enseignants
	 */
    public void deleteEnseignants() {
    	Transaction transaction = null;
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Enseignant> enseignants = session.createQuery("from Enseignant", Enseignant.class).list();
		if(!enseignants.isEmpty())
		{
			for (Enseignant enseignant : enseignants) {
				transaction = session.beginTransaction();
				session.delete(enseignant);
				transaction.commit();
			}
		}      
    }

}
