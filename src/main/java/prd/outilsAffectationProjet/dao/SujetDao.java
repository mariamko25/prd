package prd.outilsAffectationProjet.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import prd.outilsAffectationProjet.config.HibernateConfiguration;
import prd.outilsAffectationProjet.model.*;

/**
 * Classe data access object Pour Sujet
 * 
 * @author MariamKonate
 *
 */
public class SujetDao {

	/**
	 * Enregistrer un sujet dans la base
	 * 
	 * @param sujet objet Sujet
	 * 
	 */
	public void enregistrerSujet(Sujet sujet) {
		Transaction transaction = null;
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		// start a transaction
		transaction = session.beginTransaction();
		// save the sujet object
		session.save(sujet);
		// commit transaction
		transaction.commit();

	}

	/**
	 * Obtenir un sujet
	 * @param id l'id du sujet
	 * @return objet Sujet 
	 */
	public Sujet getSujet(int id) {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		String query= "from Sujet where idSujet = :idSujet";
		List<Sujet>list=session.createQuery(query, Sujet.class).setParameter("idSujet", id).list();
		if(list.isEmpty())
		{
			return null;
		}
		return list.get(0);
	}

	/**
	 * Supprimer un sujet
	 * 
	 * @param id l'id du sujet
	 */
	public void deleteSujet(int id) {
		Transaction transaction = null;
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		transaction = session.beginTransaction();
		Sujet sujet = session.load(Sujet.class, id);
		session.delete(sujet);

		transaction.commit();
	}

	/**
	 * mette à jour un sujet
	 * 
	 * @param id l'id du sujet
	 * @param sujetB objet Sujet
	 */
	public void updateSujet(int id,Sujet sujetB) {
		Transaction transaction = null;
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		// start a transaction
		transaction = session.beginTransaction();
		// save the sujet object
		Sujet sujetA = session.load(Sujet.class, id);
		sujetA.setIntitule(sujetB.getIntitule());
		sujetA.setDescription(sujetB.getDescription());
		sujetA.setNbCopie(sujetB.getNbCopie());
		// commit transaction
		transaction.commit();
	}

	/**
	 * Obtenir la liste des sujets
	 * 
	 * @return List Sujet
	 */
	public List<Sujet> getSujets() {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		return session.createQuery("from Sujet", Sujet.class).list();
	}

	/**
	 * Supprimer la liste des sujets
	 */
	public void deleteSujets() {
		Transaction transaction = null;
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Sujet> sujets = session.createQuery("from Sujet", Sujet.class).list();
		if(!sujets.isEmpty())
		{
			for (Sujet sujet : sujets) {
				transaction = session.beginTransaction();
				session.delete(sujet);
				transaction.commit();
			}
		}
		

	}

	/**
	 * Obtenir la liste des sujets contenant le mot like
	 * 
	 * @param like une string 
	 * @return List Sujet
	 */
	public List<Sujet> getSujets(String like) {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		String query="from Sujet S where lower(S.description) like :chaine  or S.intitule like :chaine  order by idSujet desc";
		return session.createQuery(query, Sujet.class).setParameter("chaine","%"+like.trim().toLowerCase()+"%").list();
	}

	/**
	 * Obtenir la liste des sujets limitée par limit
	 * 
	 * @param limit la taille limite de la liste
	 * @return List Sujet avec taille max limit
	 */
	public List<Sujet> getSujets(int limit) {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		
		return session.createQuery("from Sujet order by idSujet desc", Sujet.class).setMaxResults(limit).list();
	}

	/**
	 * Obtenir la liste des sujets en fonction de l'idEnseignement
	 * 
	 * @param idEnseignement l'id de l'enseignement
	 * @return List Sujet
	 */
	public List<Sujet> getSujetsEnseignement(int idEnseignement) {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		String query="from Sujet S where S.idEnseignement = :idEnseignement";

		return session.createQuery(query, Sujet.class).setParameter("idEnseignement",idEnseignement)
				.setMaxResults(20).list();
	}

	/**
	 * Obtenir la liste des sujets en fonction de l'idEnseignement et l'enseignant
	 * 
	 * @param idEnseignement l'id de l'enseignement
	 * @param idEnseignant l'id de l'enseignant
	 * @return List Sujet
	 */
	public List<Sujet> getSujetsEnseignementEnseignant(int idEnseignement, int idEnseignant) {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		String query= "from Sujet as S where S.idEnseignement = :idEnseignement  and S.idSujet in (select E.idSujet from Encadrant as E where E.idEnseignant = :idEnseignant )";
		return session.createQuery(query, Sujet.class).setParameter("idEnseignement", idEnseignement).setParameter("idEnseignant", idEnseignant).setMaxResults(20).list();
	}

	/**
	 * Obtenir La liste des sujets ayant l'identifiant idEnseignement et comme
	 * description description
	 * 
	 * @param idEnseignement l'id de l'enseignement
	 * @param description la chaine de description
	 * @return List Sujet
	 */
	public List<Sujet> getSujetsEnseignementDescription(int idEnseignement, String description) {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		String query= "from Sujet S where S.idEnseignement = :idEnseignement and lower(S.description) like :chaine order by idSujet desc";	
		return session.createQuery(query	,Sujet.class).setParameter("idEnseignement", idEnseignement).setParameter("chaine", "%"+description.trim().toLowerCase()+"%").setMaxResults(20).list();
	}

	/**
	 * Obtenir le nom de l'enseignement avec l'identifiant
	 * 
	 * @param idEnseignement l'id de l'enseignement
	 * @return Le nom de l'enseignement
	 */
	public String getNomEnseignementSujet(int idEnseignement) {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		String query= "select E.nom from Enseignement E where E.idEnseignement = :idEnseignement";				
		String nom = session.createQuery(query,String.class).setParameter("idEnseignement", idEnseignement).list().get(0);
		return nom;
	}

	/**
	 * Obtenir l'enseignement avec l'identifiant
	 * 
	 * @param idEnseignement l'identifiant de l'enseignement
	 * @return Enseignement
	 */
	public Enseignement getEnseignementSujet(int idEnseignement) {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		String query=" from Enseignement E where E.idEnseignement = :idEnseignement";
		Enseignement enseignement = session
				.createQuery(query, Enseignement.class).setParameter("idEnseignement", idEnseignement)
				.list().get(0);
		return enseignement;
	}

	
	/**
	 * Obtenir l'annee de création de l'enseignement portant l'identifiant
	 * idEnseignement
	 * 
	 * @param idEnseignement l'id de l'enseignement
	 * @return l'annee de l'enseignement
	 */
	public int getAnneeEnseignementSujet(int idEnseignement) {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		int annee = -1;
		String query= "select E.annee from Enseignement E where E.idEnseignement = :idEnseignement";
		
		List<Integer> list=session.createQuery(query,Integer.class).setParameter("idEnseignement", idEnseignement).list();
		if (!list.isEmpty()) {
			annee = list.get(0);
		}
		return annee;
	}

	/**
	 * Obtenir nom de l'enseignement portant l'idEnseignement avec l'annee
	 * 
	 * @param idEnseignement l'id de l'enseignement
	 * @param annee de l'enseignement
	 * @return nom de l'enseignement
	 */
	public String getNomEnseignementSujet(int idEnseignement, int annee) {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		String nom="";
		String query="select E.nom from Enseignement E where E.idEnseignement = :idEnseignement and E.annee= :annee";
		List<String> noms= session.createQuery(query,String.class).setParameter("idEnseignement", idEnseignement).setParameter("annee", annee).list();
		if(!noms.isEmpty()) {
			nom=noms.get(0);
		}
		 
		return nom;
	}

	/**
	 * Obtenir le sujet a partir de son intitule
	 * 
	 * @param intitule l'intitule du sujet
	 * @return Sujet 
	 */
	public Sujet getSujetIntitule(String intitule) {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		String query="from Sujet S where lower(S.intitule) = :chaine order by S.idSujet desc";
		List<Sujet> sujets = session
				.createQuery(query,Sujet.class).setParameter("chaine", intitule.trim().toLowerCase())
				.list();
		if (sujets.isEmpty()) {
			return null;
		}
		return sujets.get(0);
	}

	/**
	 * Obtenir le sujet a partir de l'intitule et de l'idEnseignement
	 * 
	 * @param intitule l'intitule du sujet
	 * @param idEnseignement l'id de l'enseignement
	 * @return objet Sujet
	 */
	public Sujet getSujetIntituleEnseignement(String intitule, int idEnseignement) {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		String query="from Sujet S where lower(S.intitule) = :chaine and idEnseignement = :idEnseignement order by S.idSujet desc";
		List<Sujet> sujets = session.createQuery(query, Sujet.class).setParameter("chaine", intitule.trim().toLowerCase()).setParameter("idEnseignement", idEnseignement).list();
		if (sujets.isEmpty()) {
			return null;
		}
		return sujets.get(0);
	}

}
