package prd.outilsAffectationProjet.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import prd.outilsAffectationProjet.config.HibernateConfiguration;
import prd.outilsAffectationProjet.model.*;

/**
 * Classe data access object Pour EnseignantNbMaxSujets
 * 
 * @author MariamKonate
 *
 */
public class EnseignantNbMaxSujetsDao {
	/**
	 * Enregistrer un EnseignantNbMaxSujets dans la base
	 * 
	 * @param enseignantNbMaxSujets object EnseignantNbMaxSujets
	 */
	public void enregistrerEnseignantNbMaxSujets(EnseignantNbMaxSujets enseignantNbMaxSujets) {
		Transaction transaction = null;
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		// start a transaction
		transaction = session.beginTransaction();
		// save the enseignant object
		session.save(enseignantNbMaxSujets);
		// commit transaction
		transaction.commit();

	}
	
	
	/**
	 * Obtenir EnseignantNbMaxSujets a partir de l'id enseignant 
	 * et l'id enseignement
	 * @param idEnseignant l'id de l'enseignant
	 * @param idEnseignement l'id de l'enseignement
	 * @return EnseignantNbMaxSujets
	 */
	public EnseignantNbMaxSujets getEnseignantNbMaxSujets(int idEnseignant,int idEnseignement) {

		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<EnseignantNbMaxSujets> enseignantNbMaxSujets = session.createQuery("from EnseignantNbMaxSujets where idEnseignant = :idEnseignant and idEnseignement = :idEnseignement", EnseignantNbMaxSujets.class)
				.setParameter("idEnseignant", idEnseignant).setParameter("idEnseignement", idEnseignement).list();
		if (enseignantNbMaxSujets.isEmpty()) {
			return null;
		}
		return enseignantNbMaxSujets.get(0);

	}
	
	/**
	 * Obtenir EnseignantNbMaxSujets a partir de l'id enseignement
	 * @param idEnseignement l'idEnseignement 
	 * @return EnseignantNbMaxSujets
	 */
	public List<EnseignantNbMaxSujets> getEnseignantNbMaxSujets(int idEnseignement) {

		Session session = HibernateConfiguration.getSessionFactory().openSession();
		return session.createQuery("from EnseignantNbMaxSujets where  idEnseignement = :idEnseignement", EnseignantNbMaxSujets.class).setParameter("idEnseignement", idEnseignement)
				.list();
		

	}
	
	/**
	 * mette à jour un EnseignantNbMaxSujets
	 * 
	 * @param enseignantNbMaxSujetsB objet EnseignantNbMaxSujets
	 */
	public void updateEnseignantNbMaxSujets(EnseignantNbMaxSujets enseignantNbMaxSujetsB) {
		Transaction transaction = null;
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		// start a transaction
		int idEnseignant=enseignantNbMaxSujetsB.getIdEnseignant();
		int idEnseignement= enseignantNbMaxSujetsB.getIdEnseignement();
		transaction = session.beginTransaction();
		List<EnseignantNbMaxSujets> enseignantNbMaxSujets = session.createQuery("from EnseignantNbMaxSujets where idEnseignant = :idEnseignant and idEnseignement = :idEnseignement", EnseignantNbMaxSujets.class)
				.setParameter("idEnseignant", idEnseignant).setParameter("idEnseignement", idEnseignement).list();
		EnseignantNbMaxSujets enseignantNbMaxSujetsA=new EnseignantNbMaxSujets();
		if(!enseignantNbMaxSujets.isEmpty())
		{
			enseignantNbMaxSujetsA=enseignantNbMaxSujets.get(0);
		}
		enseignantNbMaxSujetsA.setNombreSujetMax(enseignantNbMaxSujetsB.getNombreSujetMax());
		enseignantNbMaxSujetsA.setNombreSujetMin(enseignantNbMaxSujetsB.getNombreSujetMin());
		// commit transaction
		transaction.commit();
	}


}
