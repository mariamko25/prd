package prd.outilsAffectationProjet.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import prd.outilsAffectationProjet.config.HibernateConfiguration;
import prd.outilsAffectationProjet.model.*;

/**
 * Classe data access object
 * Pour MembreDuGroupeDao
 * @author MariamKonate
 *
 */
public class MembreDuGroupeDao {
	
	/**
	 * Enregistrer un membre du groupe dans la base
	 * @param membregroupe objet  MembreDuGroupe
	 */
	public void enregistrerMembreDuGroupe(MembreDuGroupe membregroupe) {
        Transaction transaction = null;
        Session session = HibernateConfiguration.getSessionFactory().openSession();
            
        		// start a transaction
            transaction = session.beginTransaction();
            // save the membregroupe object
            
            session.save(membregroupe);
            // commit transaction
            transaction.commit();
        
    }
	
	
	/**
	 * Obtenir tous els membres de tous les groupes
	 * @return liste MembreDuGroupe
	 */
    public List < MembreDuGroupe > getMembreDesGroupes() {
    	Session session=HibernateConfiguration.getSessionFactory().openSession();
    	return session.createQuery("from MembreDuGroupe", MembreDuGroupe.class).list();        
    }
    
    /**
	 * Obtenir  la taille d'un groupe
	 * @param idGroupe l'id du groupe
	 * @return  nombre  de Membre Du Groupe
	 */
    public int getNbMembreGroupe(int idGroupe) {
    	Session session=HibernateConfiguration.getSessionFactory().openSession();
    	List<MembreDuGroupe>membres= session.createQuery("from MembreDuGroupe where idGroupe = :idGroupe", MembreDuGroupe.class).setParameter("idGroupe", idGroupe).list();  
    	if(!membres.isEmpty())
    	{
    		return membres.size();
    	}
    return 0;
    }
    
    /**
     * Etudiants membres d'un groupe
     * @param idGroupe id du Groupe 
     * @return liste idEtudiants 
     */
    public List<Integer> etudiantsGroupe(int idGroupe)
    {
    		Session session=HibernateConfiguration.getSessionFactory().openSession();
    		return session.createQuery("select idEtudiant from MembreDuGroupe where idGroupe = :idGroupe", Integer.class).setParameter("idGroupe", idGroupe).list();  
    }
    /**
	 * Supprimer la liste des MembreDuGroupe
	 */
    public void deleteMembreDuGroupes() {
    	Transaction transaction = null;
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<MembreDuGroupe> membreDuGroupes = session.createQuery("from MembreDuGroupe", MembreDuGroupe.class).list();
		if(!membreDuGroupes.isEmpty())
		{
			for (MembreDuGroupe membreDuGroupe : membreDuGroupes) {
				transaction = session.beginTransaction();
				session.delete(membreDuGroupe);
				transaction.commit();
			}
		}          
    }
    
}
