package prd.outilsAffectationProjet.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import prd.outilsAffectationProjet.config.*;


import prd.outilsAffectationProjet.model.*;

/**
 * Classe data access object
 * Pour Etudiant
 * @author MariamKonate
 *
 */
public class EtudiantDao {
	
	/**
	 * Enregistrer etudiant dans la base
	 * @param etudiant objet Etudiant
	 */
	public void enregistrerEtudiant(Etudiant etudiant) {
        Transaction transaction = null;
        Session session = HibernateConfiguration.getSessionFactory().openSession();
            // start a transaction
            transaction = session.beginTransaction();
            // save the student object
            session.save(etudiant);
            // commit transaction
            transaction.commit();
        
    }
	
	/**
	 * Obtenir etudiant a partir de son adresse mail
	 * @param email l'adresse mail
	 * @return Etudiant
	 */
	public Etudiant getEtudiant(String email)
	{
		Session session=HibernateConfiguration.getSessionFactory().openSession();
		List<Etudiant> etu=session.createQuery("from Etudiant where lower(email) = :email", Etudiant.class).setParameter("email", email.toLowerCase().trim()).list();
		if(!etu.isEmpty())
		{
			return etu.get(0);
		}
		return null;
	}
	/**
	 * Obtenir la liste des etudiants
	 * @return liste Etudiant
	 */
    public List < Etudiant > getStudents() {
    	
    	Session session=HibernateConfiguration.getSessionFactory().openSession();
    	return session.createQuery("from Etudiant", Etudiant.class).list();
        
    }
	
    /**
   	 * Obtenir la liste des etudiants
   	 * @return liste Etudiant
   	 */
       public List < Etudiant > getEtudiants() {
       	
       	Session session=HibernateConfiguration.getSessionFactory().openSession();
       	return session.createQuery("from Etudiant", Etudiant.class).list();
           
       }
    /**
     * Obtenir la liste des etudiants limitée par limit
     * @param limit la taille max de la liste 
     * @return liste Etudiant avec taille max limit
     */
    public List < Etudiant > getStudents(int limit) {
    	
    	Session session=HibernateConfiguration.getSessionFactory().openSession();
    	return session.createQuery("from Etudiant order by idEtudiant desc", Etudiant.class).setMaxResults(limit).list();
        
    }
    
    /**
	 * Supprimer la liste des etudiants
	 */
    public void deleteEtudiants() {
    	Transaction transaction = null;
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Etudiant> etudiants = session.createQuery("from Etudiant", Etudiant.class).list();
		if(!etudiants.isEmpty())
		{
			for (Etudiant etudiant : etudiants) {
				transaction = session.beginTransaction();
				session.delete(etudiant);
				transaction.commit();
			}
		}       
    }

	/**
	 * Obtenir etudiant a partir de son idEtudiant
	 * @param idEtudiant l'id de l'etudiant 
	 * @return Etudiant objet
	 */
	public Etudiant getEtudiant(int idEtudiant)
	{
		Session session=HibernateConfiguration.getSessionFactory().openSession();
		List<Etudiant> etu=session.createQuery("from Etudiant where idEtudiant = :idEtudiant", Etudiant.class).setParameter("idEtudiant", idEtudiant).list();
		if(!etu.isEmpty())
		{
			return etu.get(0);
		}
		return null;
	}
}
