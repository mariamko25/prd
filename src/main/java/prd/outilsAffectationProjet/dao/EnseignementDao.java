package prd.outilsAffectationProjet.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import prd.outilsAffectationProjet.config.HibernateConfiguration;
import prd.outilsAffectationProjet.model.*;

/**
 * Classe data access object Pour Enseignement
 * 
 * @author MariamKonate
 *
 */
public class EnseignementDao {

	/**
	 * Enregistrer enseignement dans la base
	 * 
	 * @param enseignement objet Enseignement
	 */
	public void enregistrerEnseignement(Enseignement enseignement) {
		Transaction transaction = null;
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		// start a transaction
		transaction = session.beginTransaction();
		// save the enseignement object
		session.save(enseignement);
		// commit transaction
		transaction.commit();

	}

	/**
	 * Supprime un enseignement portant l'id idEnseignement
	 * @param idEnseignement l'id de l'enseignement
	 */
	public void supprimerEnseignement(int idEnseignement) {
		Transaction transaction = null;
	    	Session session=HibernateConfiguration.getSessionFactory().openSession();
	    	transaction = session.beginTransaction();
	    	Enseignement enseignement=session.load(Enseignement.class, idEnseignement);
	    	session.delete(enseignement);
	    	transaction.commit();
	}

	/**
	 * mette à jour un enseignement
	 * 
	 * @param idEnseignement l'id de l'enseignement a modifier
	 * @param enseignementB objet enseignement
	 */
	public void updateEnseignement(int idEnseignement, Enseignement enseignementB) {
		Transaction transaction = null;
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		// start a transaction
		transaction = session.beginTransaction();
		// save the sujet object
		Enseignement enseignementA = session.load(Enseignement.class, idEnseignement);
		enseignementA.setAnnee(enseignementB.getAnnee());
		enseignementA.setCle(enseignementB.getCle());
		enseignementA.setDescription(enseignementB.getDescription());
		enseignementA.setFormulaire(enseignementB.getFormulaire());
		enseignementA.setIdEnseignant(enseignementB.getIdEnseignant());
		enseignementA.setNbMaxScore(enseignementB.getNbMaxScore());
		enseignementA.setNbMaxTotal(enseignementB.getNbMaxTotal());
		enseignementA.setNom(enseignementB.getNom());
		enseignementA.setTypeOptimisation(enseignementB.getTypeOptimisation());
		// commit transaction
		transaction.commit();
	}

	/**
	 * Obtenir un enseignement à partir de son identifiant
	 * 
	 * @param idEnseignement l'id de l'enseignement
	 * @return Enseignement
	 */
	public Enseignement getEnseignement(int idEnseignement) {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		return session.createQuery("from Enseignement E where E.idEnseignement = :idEnseignement", Enseignement.class)
				.setParameter("idEnseignement", idEnseignement).list().get(0);
	}

	/**
	 * Récupérer la liste des enseignements
	 * 
	 * @return liste Enseignement
	 */
	public List<Enseignement> getEnseignements() {

		Session session = HibernateConfiguration.getSessionFactory().openSession();
		return session.createQuery("from Enseignement ", Enseignement.class).list();

	}

	/**
	 * Récupérer la liste des enseignements de l'enseignant dont la description est
	 * comme "like"
	 * @param like une string 
	 * @return liste Enseignement
	 */
	public List<Enseignement> getEnseignementsLike(String like) {

		Session session = HibernateConfiguration.getSessionFactory().openSession();
		
		String query="from Enseignement E where  lower(E.description) like :chaine or lower(E.nom) like :chaine order by idEnseignement desc";
		return session.createQuery(query,Enseignement.class).setParameter("chaine", "%"+like.trim().toLowerCase()+"%").list();

	}

	/**
	 * Récupérer la liste des enseignements d'un enseignant
	 * 
	 * @param idEnseignant id de l'enseignement
	 * @return liste Enseignement
	 */
	public List<Enseignement> getEnseignementsEnseignant(int idEnseignant) {

		Session session = HibernateConfiguration.getSessionFactory().openSession();
		String query= "from Enseignement E where E.idEnseignant = :idEnseignant order by idEnseignement desc";
		return session.createQuery(query,Enseignement.class).setParameter("idEnseignant", idEnseignant).list();

	}

	/**
	 * Récupérer la liste des enseignements limités
	 * 
	 * @param limit la taille max de la liste
	 * @return list Enseignement avec taille  max limit
	 */
	public List<Enseignement> getEnseignements(int limit) {

		Session session = HibernateConfiguration.getSessionFactory().openSession();
		return session.createQuery("from Enseignement order by idEnseignement desc", Enseignement.class)
				.setMaxResults(limit).list();

	}

	/**
	 * Récupérer l'id de l'enseignant qui encadre l'enseignement portant comme
	 * identifiant idEnseignement
	 * 
	 * @param idEnseignement l'id de l'enseignement
	 * @return id Enseignement
	 */
	public int getIdEnseignant(int idEnseignement) {
		int id = -1;
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> list=session.createQuery("select E.idEnseignant from Enseignement E where E.idEnseignement =  :idEnseignement",
				Integer.class).setParameter("idEnseignement", idEnseignement).list();
		if(!list.isEmpty()) {
			id=list.get(0);
		}
		
		return id;
	}

	/**
	 * Récupérer l'id de l'enseignement qui se nomme nomEnseignement et qui a été
	 * créé en annee
	 * 
	 * @param nomEnseignement nom de l'enseignement
	 * @param annee annee de l'enseignement
	 * @return id de l'enseignement
	 */
	public int getIdEnseignement(String nomEnseignement, int annee) {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> list = session.createQuery(
				"select E.idEnseignement from Enseignement E where lower(E.nom) = :nom and E.annee = :annee", 
						Integer.class).setParameter("nom", nomEnseignement.toLowerCase().trim()).setParameter("annee", annee).list();
		if(list.isEmpty()) {
			return -1;
		}
		return list.get(0);
		
		
	}

	/**
	 * Récupérer la liste des enseignements de l'annee
	 * 
	 * @param annee  annee de l'enseignement
	 * @return liste des id des enseignement de l'annee
	 */
	public List<Integer> getIdEnseignement(int annee) {
		Session session = HibernateConfiguration.getSessionFactory().openSession();

		List<Integer> list = session
				.createQuery("select E.idEnseignement from Enseignement E where E.annee = :annee", Integer.class).setParameter("annee", annee)
				.list();
		return list;
	}

	/**
	 * Récupérer le nom de l'enseignant ayant pour identifiant idEnseignant
	 * 
	 * @param idEnseignant l'id de l'enseignement
	 * @return nom de l'encadrant 
	 */
	public String getNomEncadrant(int idEnseignant) {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		String nom ="";
		List<String> noms= session.createQuery("select E.nom from Enseignant E where E.id = :id", String.class)
				.setParameter("id", idEnseignant).list();
		if(!noms.isEmpty()) {
			nom=noms.get(0);
		}
		return nom;
	}

	/**
	 * Liste Enseignement ayant un formulaire ouvert
	 * 
	 * @return Liste Enseignement
	 */
	public List<Enseignement> getEnseignementsFormulaire() {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		return session.createQuery("from Enseignement where formulaire = 1", Enseignement.class).list();

	}

	/**
	 * Supprimer la liste des enseignements
	 */
    public void deleteEnseignements() {
    	Transaction transaction = null;
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Enseignement> enseignements = session.createQuery("from Enseignement", Enseignement.class).list();
		if(!enseignements.isEmpty())
		{
			for (Enseignement enseignement : enseignements) {
				transaction = session.beginTransaction();
				session.delete(enseignement);
				transaction.commit();
			}
		}             
    }
}
