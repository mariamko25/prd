package prd.outilsAffectationProjet.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import prd.outilsAffectationProjet.config.HibernateConfiguration;
import prd.outilsAffectationProjet.model.*;

/**
 * Classe data access object
 * Pour Groupe
 * @author MariamKonate
 *
 */
public class GroupeDao {

	/**
	 * Enregistrer un groupe dans la base
	 * 
	 * @param groupe objet Groupe
	 */
	public void enregistrerGroupe(Groupe groupe) {
		Transaction transaction = null;
		Session session = HibernateConfiguration.getSessionFactory().openSession();

		// start a transaction
		transaction = session.beginTransaction();
		// save the groupe object

		session.save(groupe);
		// commit transaction
		transaction.commit();

	}

	/**
	 * Obtenir la liste des groupes
	 * 
	 * @return liste  Groupe
	 */
	public List<Groupe> getGroupes() {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		return session.createQuery("from Groupe", Groupe.class).list();
	}

	/**
	 * Obtenir un groupe a partir de son identifiant
	 * 
	 * @param idGroupe l'id du groupe
	 * @return Groupe le groupe ayant idGroupe comme id
	 */
	public Groupe getGroupe(int idGroupe) {
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Groupe> list = session.createQuery("from Groupe where idGroupe = :idGroupe", Groupe.class).setParameter("idGroupe", idGroupe).list();
		if (list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}

	/**
	 * Supprimer le groupe avec id idGroupe
	 * 
	 * @param idGroupe l'id du groupe
	 */
	public void deleteGroup(int idGroupe) {
		Transaction transaction = null;
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		transaction = session.beginTransaction();
		Groupe group = session.load(Groupe.class, idGroupe);
		session.delete(group);
		transaction.commit();
	}

	 /**
		 * Supprimer la liste des Groupes
		 */
	    public void deleteGroupes() {
	    	Transaction transaction = null;
			Session session = HibernateConfiguration.getSessionFactory().openSession();
			List<Groupe> groupes = session.createQuery("from Groupe", Groupe.class).list();
			if(!groupes.isEmpty())
			{
				for (Groupe groupe : groupes) {
					transaction = session.beginTransaction();
					session.delete(groupe);
					transaction.commit();
				}
			}        
	    }
}
