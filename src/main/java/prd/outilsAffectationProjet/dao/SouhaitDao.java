package prd.outilsAffectationProjet.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import prd.outilsAffectationProjet.config.HibernateConfiguration;
import prd.outilsAffectationProjet.model.*;


/**
 * Classe data access object
 * Pour Souhait
 * @author MariamKonate
 *
 */
public class SouhaitDao {
	/**
	 * Enregistrer un souhait dans la base
	 * @param souhait objet Souhait
	 */
	public void enregistrerSouhait(Souhait souhait) {
        Transaction transaction = null;
        Session session = HibernateConfiguration.getSessionFactory().openSession();
            
        		// start a transaction
            transaction = session.beginTransaction();
            // save the souhait object
            
            session.save(souhait);
            // commit transaction
            transaction.commit();
        
    }
	
	/**
	 * Enregistrer une liste de souhait pour un enseignement
	 * @param souhaits liste de souhaits
	 * @param idEnseignement l'identifiant de l'enseignement
	 */
	public void enregistrerSouhaits(List<Souhait>souhaits,int idEnseignement)
	{
		Transaction transaction = null;
        Session session = HibernateConfiguration.getSessionFactory().openSession();
        List<Souhait> souhaitDelete= getSouhaits(idEnseignement);
        for(Souhait sht:souhaitDelete)
        {
        		// start a transaction
            transaction = session.beginTransaction();
            // delete the souhait object
            session.delete(sht);
            // commit transaction
            transaction.commit();
        }
        
        for(Souhait sht:souhaits)
        {
        		// start a transaction
        		transaction = session.beginTransaction();
            // save the souhait object
            session.save(sht);
            // commit transaction
            transaction.commit();
        }
        
            
	}
	
	/**
	 * Obtenir la liste des souhaits
	 * @return liste Souhait
	 */
    public List < Souhait > getSouhaits() {
    	Session session=HibernateConfiguration.getSessionFactory().openSession();
    	return session.createQuery("from Souhait", Souhait.class).list();        
    }
    
    
    /**
	 * Obtenir la liste des souhaits
	 * pour un enseignement
	 * @param idEnseignement l'id de l'enseignement
	 * @return liste Souhait
	 */
    public List < Souhait > getSouhaits(int idEnseignement) {
    	Session session=HibernateConfiguration.getSessionFactory().openSession();
    	String query="from Souhait where idSujet in(select idSujet from Sujet where idEnseignement = :idEnseignement)";
    	return session.createQuery(query, Souhait.class).setParameter("idEnseignement", idEnseignement).list();        
    }
    
    
    /**
	 * Obtenir la liste des groupes des souhaits
	 * pour un enseignement
	 * @param idEnseignement l'id de l'Enseignement
	 * @return liste des idGroupes
	 */
    public List < Integer > getSouhaitsGroupe(int idEnseignement) {
    	Session session=HibernateConfiguration.getSessionFactory().openSession();
    
    	return session.createQuery("select distinct idGroupe from Souhait where idSujet in(select idSujet from Sujet where idEnseignement = :idEnseignement) order by idGroupe asc", Integer.class).setParameter("idEnseignement", idEnseignement).list();        
    }
    
    
    /**
	 * Obtenir la liste des sujets des souhaits
	 * pour un enseignement
	 * @param idEnseignement liste des id des enseignement
	 * @return liste des idSujets
	 */
    public List < Integer > getSouhaitsSujet(int idEnseignement) {
    	Session session=HibernateConfiguration.getSessionFactory().openSession();
    
    	return session.createQuery("select distinct idSujet from Souhait where idSujet in(select idSujet from Sujet where idEnseignement = :idEnseignement)", Integer.class).setParameter("idEnseignement", idEnseignement).list();        
    }
    
    /**
	 * Obtenir la liste des souhaits d'un groupe 
	 * pour un enseignement
	 * @param idEnseignement l'id de l'enseignement
	 * @param idGroupe l'id du groupe
	 * @return  Liste Souhait
	 */
    public List < Souhait > getSouhaits(int idEnseignement,int idGroupe) {
    	Session session=HibernateConfiguration.getSessionFactory().openSession();
    	return session.createQuery("from Souhait where idSujet in(select idSujet from Sujet where idEnseignement = :idEnseignement) and idGroupe = :idGroupe", Souhait.class).setParameter("idEnseignement", idEnseignement).setParameter("idGroupe", idGroupe).list();        
    }
    
    /**
	 * Supprimer la liste des souhaits
	 */
    public void deleteSouhaits() {
    	Transaction transaction = null;
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Souhait> souhaits = session.createQuery("from Souhait", Souhait.class).list();
		if(!souhaits.isEmpty())
		{
			for (Souhait souhait : souhaits) {
				transaction = session.beginTransaction();
				session.delete(souhait);
				transaction.commit();
			}
		}      
    }
}
