package prd.outilsAffectationProjet.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import prd.outilsAffectationProjet.config.HibernateConfiguration;
import prd.outilsAffectationProjet.model.Encadrant;

/**
 * Classe data access object Pour Encadrant
 * 
 * @author MariamKonate
 *
 */
public class EncadrantDao {

	/**
	 * Enregistrer un encadrant dans la base
	 * 
	 * @param encadrant un objet Encadrant
	 */
	public void enregistrerEncadrant(Encadrant encadrant) {
		Transaction transaction = null;
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		// start a transaction
		transaction = session.beginTransaction();
		// save the enseignant object
		session.save(encadrant);
		// commit transaction
		transaction.commit();

	}

	/**
	 * Obtenir l'identifiant de l'encadrant du sujet ayant pour id idSujet
	 * 
	 * @param idSujet l'id du sujet
	 * @return l'encadrant du sujet
	 */
	public int getIdEncadrant(int idSujet) {
		int id = -1;
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Integer> list= session.createQuery("select E.idEnseignant from Encadrant E where E.idSujet = :idSujet", Integer.class)
				.setParameter("idSujet", idSujet).list();		
		if(!list.isEmpty()) {
			id=list.get(0);
		}
		return id;
	}

	/**
	 * Obtenir la liste des encadrants
	 * 
	 * @return liste des encadrants
	 */
	public List<Encadrant> getEncadrant() {

		Session session = HibernateConfiguration.getSessionFactory().openSession();
		return session.createQuery("from Encadrant", Encadrant.class).list();

	}
	
	/**
	 * Obtenir la liste des encadrants pour un enseignement
	 * ayant pour id idEnseignement
	 * @param idEnseignement  l'id de l'enseignement
	 * @return les encadrants de sujets d'un enseignement
	 */
	public List<Encadrant> getEncadrantEnseignement(int idEnseignement) {

		Session session = HibernateConfiguration.getSessionFactory().openSession();
		return session.createQuery("from Encadrant where idSujet in (Select S.idSujet from Sujet S where idEnseignement = :idEnseignement)", Encadrant.class).setParameter("idEnseignement", idEnseignement).list();

	}

	/**
	 * Obtenir encadrant a partir de l'id sujet
	 * @param idSujet l'id du sujet
	 * @return l'encadrant du sujet
	 */
	public Encadrant getEncadrant(int idSujet) {

		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Encadrant> encadrants = session.createQuery("from Encadrant where idSujet = :idSujet", Encadrant.class).setParameter("idSujet", idSujet)
				.list();
		if (encadrants.isEmpty()) {
			return null;
		}
		return encadrants.get(0);

	}

	/**
	 * Obtenir la liste des encadrants en limitant par l'entier limit
	 * 
	 * @param limit la taille limite de la liste
	 * @return la liste des encadrants ayant la taille limite limit
	 */
	public List<Encadrant> getEncadrants(int limit) {

		Session session = HibernateConfiguration.getSessionFactory().openSession();
		return session.createQuery("from Encadrant order by idEnseignant desc", Encadrant.class).setMaxResults(limit)
				.list();

	}

	/**
	 * Supprimer la liste des encadrants
	 */
	public void deleteEncadrants() {
		Transaction transaction = null;
		Session session = HibernateConfiguration.getSessionFactory().openSession();
		List<Encadrant> encadrants = session.createQuery("from Encadrant", Encadrant.class).list();
		if (!encadrants.isEmpty()) {
			for (Encadrant encadrant : encadrants) {
				transaction = session.beginTransaction();
				session.delete(encadrant);
				transaction.commit();
			}
		}
	}

}
