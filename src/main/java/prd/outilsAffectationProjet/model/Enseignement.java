package prd.outilsAffectationProjet.model;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "Enseignement")
public class Enseignement implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idEnseignement")
	int idEnseignement;
	
	@Column(name = "nom")
	String nom;
	
	@Column(name = "annee")
	int annee;
	
	@Column(name = "description")
	String description;
	
	@Column(name = "typeOptimisation")
	String typeOptimisation;
	
	@Column(name = "idEnseignant")
	int idEnseignant;
	
	@Column(name = "nbMaxScore")
	int nbMaxScore;
	
	@Column(name = "nbMaxTotal")
	int nbMaxTotal;
	
	@Column(name = "formulaire")
	int formulaire;
	
	@Column(name = "cle")
	String cle;

	/**
	 * Constructeur par défaut
	 */
	public Enseignement()
    {
    	
    }
	
	
	
	/**
	 * @return the nbMaxTotal
	 */
	public int getNbMaxTotal() {
		return nbMaxTotal;
	}



	/**
	 * @param nbMaxTotal the nbMaxTotal to set
	 */
	public void setNbMaxTotal(int nbMaxTotal) {
		this.nbMaxTotal = nbMaxTotal;
	}



	/**
	 * @return the nbMaxScore
	 */
	public int getNbMaxScore() {
		return nbMaxScore;
	}



	/**
	 * @param nbMaxScore the nbMaxScore to set
	 */
	public void setNbMaxScore(int nbMaxScore) {
		this.nbMaxScore = nbMaxScore;
	}



	/**
	 * @return the formulaire
	 */
	public int getFormulaire() {
		return formulaire;
	}



	/**
	 * @param formulaire the formulaire to set
	 */
	public void setFormulaire(int formulaire) {
		this.formulaire = formulaire;
	}



	/**
	 * @return the cle
	 */
	public String getCle() {
		return cle;
	}



	/**
	 * @param cle the cle to set
	 */
	public void setCle(String cle) {
		this.cle = cle;
	}



	/**
	 * @return the idEnseignement
	 */
	public int getIdEnseignement() {
		return idEnseignement;
	}

	/**
	 * @param idEnseignement the idEnseignement to set
	 */
	public void setIdEnseignement(int idEnseignement) {
		this.idEnseignement = idEnseignement;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the annee
	 */
	public int getAnnee() {
		return annee;
	}

	/**
	 * @param annee the annee to set
	 */
	public void setAnnee(int annee) {
		this.annee = annee;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the typeOptimisation
	 */
	public String getTypeOptimisation() {
		return typeOptimisation;
	}

	/**
	 * @param typeOptimisation the typeOptimisation to set
	 */
	public void setTypeOptimisation(String typeOptimisation) {
		this.typeOptimisation = typeOptimisation;
	}

	/**
	 * @return the idEnseignant
	 */
	public int getIdEnseignant() {
		return idEnseignant;
	}

	/**
	 * @param idEnseignant the idEnseignant to set
	 */
	public void setIdEnseignant(int idEnseignant) {
		this.idEnseignant = idEnseignant;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
