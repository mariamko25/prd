package prd.outilsAffectationProjet.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Encadrant")
public class Encadrant implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name = "idEnseignant")
	int idEnseignant;
	
	@Id
    @Column(name = "idSujet")
	int idSujet;

	/**
	 * @return the idEnseignant
	 */
	public int getIdEnseignant() {
		return idEnseignant;
	}

	/**
	 * @param idEnseignant the idEnseignant to set
	 */
	public void setIdEnseignant(int idEnseignant) {
		this.idEnseignant = idEnseignant;
	}

	/**
	 * @return the idSujet
	 */
	public int getIdSujet() {
		return idSujet;
	}

	/**
	 * @param idSujet the idSujet to set
	 */
	public void setIdSujet(int idSujet) {
		this.idSujet = idSujet;
	}
	
	

}
