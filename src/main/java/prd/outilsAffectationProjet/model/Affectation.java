package prd.outilsAffectationProjet.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Affectation")
public class Affectation implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name = "idGroupe")
	int idGroupe;
	
	@Id
    @Column(name = "idSujet")
	int idSujet;
	
	String nomEncadrant;

	/**
	 * @return the idGroupe
	 */
	public int getIdGroupe() {
		return idGroupe;
	}

	/**
	 * @param idGroupe the idGroupe to set
	 */
	public void setIdGroupe(int idGroupe) {
		this.idGroupe = idGroupe;
	}

	/**
	 * @return the idSujet
	 */
	public int getIdSujet() {
		return idSujet;
	}

	/**
	 * @param idSujet the idSujet to set
	 */
	public void setIdSujet(int idSujet) {
		this.idSujet = idSujet;
	}

	/**
	 * @return the nomEncadrant
	 */
	public String getNomEncadrant() {
		return nomEncadrant;
	}

	/**
	 * @param nomEncadrant the nomEncadrant to set
	 */
	public void setNomEncadrant(String nomEncadrant) {
		this.nomEncadrant = nomEncadrant;
	}
	

}
