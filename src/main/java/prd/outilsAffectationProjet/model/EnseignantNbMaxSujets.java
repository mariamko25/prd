package prd.outilsAffectationProjet.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "EnseignantNbMaxSujets")
public class EnseignantNbMaxSujets implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name = "idEnseignant")
	int idEnseignant;
	
	@Id
    @Column(name = "idEnseignement")
	int idEnseignement;
	
	
    @Column(name = "nombreSujetMax")
	int nombreSujetMax;
    
    @Column(name = "nombreSujetMin")
   	int nombreSujetMin;

    
	/**
	 * @return the nombreSujetMin
	 */
	public int getNombreSujetMin() {
		return nombreSujetMin;
	}

	/**
	 * @param nombreSujetMin the nombreSujetMin to set
	 */
	public void setNombreSujetMin(int nombreSujetMin) {
		this.nombreSujetMin = nombreSujetMin;
	}

	/**
	 * @return the idEnseignant
	 */
	public int getIdEnseignant() {
		return idEnseignant;
	}

	/**
	 * @param idEnseignant the idEnseignant to set
	 */
	public void setIdEnseignant(int idEnseignant) {
		this.idEnseignant = idEnseignant;
	}

	/**
	 * @return the idEnseignement
	 */
	public int getIdEnseignement() {
		return idEnseignement;
	}

	/**
	 * @param idEnseignement the idEnseignement to set
	 */
	public void setIdEnseignement(int idEnseignement) {
		this.idEnseignement = idEnseignement;
	}

	/**
	 * @return the nombreSujetMax
	 */
	public int getNombreSujetMax() {
		return nombreSujetMax;
	}

	/**
	 * @param nombreSujetMax the nombreSujetMax to set
	 */
	public void setNombreSujetMax(int nombreSujetMax) {
		this.nombreSujetMax = nombreSujetMax;
	}

	

}
