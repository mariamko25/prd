package prd.outilsAffectationProjet.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author MariamKonate
 *
 */
@Entity
@Table(name = "MembreDuGroupe")
public class MembreDuGroupe implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name = "idGroupe")
	int idGroupe;
	
	@Id
    @Column(name = "idEtudiant")
	int idEtudiant;

	/**
	 * @return the idGroupe
	 */
	public int getIdGroupe() {
		return idGroupe;
	}

	/**
	 * @param idGroupe the idGroupe to set
	 */
	public void setIdGroupe(int idGroupe) {
		this.idGroupe = idGroupe;
	}

	/**
	 * @return the idEtudiant
	 */
	public int getIdEtudiant() {
		return idEtudiant;
	}

	/**
	 * @param idEtudiant the idEtudiant to set
	 */
	public void setIdEtudiant(int idEtudiant) {
		this.idEtudiant = idEtudiant;
	}	
	
}
