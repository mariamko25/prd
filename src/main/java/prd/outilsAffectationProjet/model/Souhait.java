package prd.outilsAffectationProjet.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Souhait")
public class Souhait implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name = "idGroupe")
	int idGroupe;
	
	@Id
    @Column(name = "idSujet")
	int idSujet;
		
	@Column(name = "score")
	int score;

	String nomEncadrant;
	
	
	/**
	 * @return the nomEncadrant
	 */
	public String getNomEncadrant() {
		return nomEncadrant;
	}
	/**
	 * @param nomEncadrant the nomEncadrant to set
	 */
	public void setNomEncadrant(String nomEncadrant) {
		this.nomEncadrant = nomEncadrant;
	}
	/**
	 * Constructeur par défaut
	 */
	public Souhait()
	{
		
	}
	/**
	 * Constructeur de recopie
	 * @param idGroupe l'id du groupe
	 * @param idSujet l'id du sujet
	 * @param score le score
	 */
	public Souhait(int idGroupe, int idSujet,int score)
	{
		this.idGroupe=idGroupe;
		this.idSujet=idSujet;
		this.score=score;
	}
	
	
	/**
	 * @return the idSujet l'id du sujet
	 */
	public int getIdSujet() {
		return idSujet;
	}

	/**
	 * @param idSujet the idSujet to set
	 */
	public void setIdSujet(int idSujet) {
		this.idSujet = idSujet;
	}

	/**
	 * @return the idGroupe l'id du groupe
	 */
	public int getIdGroupe() {
		return idGroupe;
	}

	/**
	 * @param idGroupe the idGroupe to set
	 */
	public void setIdGroupe(int idGroupe) {
		this.idGroupe = idGroupe;
	}

	/**
	 * @return the score
	 */
	public int getScore() {
		return score;
	}

	/**
	 * @param score the score to set
	 */
	public void setScore(int score) {
		this.score = score;
	}
	
}
