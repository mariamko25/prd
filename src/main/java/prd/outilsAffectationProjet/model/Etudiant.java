package prd.outilsAffectationProjet.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "Etudiant")
public class Etudiant implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idEtudiant")
	int idEtudiant;
	
	
	
	@Column(name = "prenom")
	String prenom;
	
	@Column(name = "nom")
	String nom;
	
	@Column(name = "dateNaissance")
	Date dateNaissance;
	
	@Column(name = "email")
	String email;
	
	public Etudiant()
    {
    	
    }
	
	public int getIdEtudiant() {
		return idEtudiant;
	}
	public void setIdEtudiant(int idEtudiant) {
		this.idEtudiant = idEtudiant;
	}
	
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public Date getDateNaissance() {
		return dateNaissance;
	}
	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
    public String toString() {
        return "Student [id=" + idEtudiant + ", firstName=" + prenom + ", lastName=" + nom + ", email=" + email + "]";
    }
}
