package prd.outilsAffectationProjet.model;

import java.io.Serializable;

import javax.persistence.*;


@Entity
@Table(name = "Sujet")
public class Sujet implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idSujet")
	int idSujet;
	
	@Column(name = "intitule")
	String intitule;
	
	@Column(name = "description")
	String description;
	
	@Column(name = "nbCopie")
	int nbCopie;
	
	
	
    @Column(name="idEnseignement", nullable=false)
    private int idEnseignement;

    
    private String nomEnseignement;
    
    private int annee;
    private String nomEnseignant;
	
    public Sujet()
    {
    	
    }
    
    /**
	 * @return the idSujet
	 */
	public int getIdSujet() {
		return idSujet;
	}

	/**
	 * @param idSujet the idSujet to set
	 */
	public void setIdSujet(int idSujet) {
		this.idSujet = idSujet;
	}

	/**
	 * @return the intitule
	 */
	public String getIntitule() {
		return intitule;
	}

	/**
	 * @param intitule the intitule to set
	 */
	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the nbCopie
	 */
	public int getNbCopie() {
		return nbCopie;
	}

	/**
	 * @param nbCopie the nbCopie to set
	 */
	public void setNbCopie(int nbCopie) {
		this.nbCopie = nbCopie;
	}

	/**
	 * @return the enseignement
	 */
	public int getIdEnseignement() {
		return idEnseignement;
	}

	/**
	 * @param idEnseignement the idEnseignement to set
	 */
	public void setIdEnseignement(int idEnseignement) {
		this.idEnseignement = idEnseignement;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the nomEnseignement
	 */
	public String getNomEnseignement() {
		return nomEnseignement;
	}

	/**
	 * @param nomEnseignement the nomEnseignement to set
	 */
	public void setNomEnseignement(String nomEnseignement) {
		this.nomEnseignement = nomEnseignement;
	}

	/**
	 * @return the annee
	 */
	public int getAnnee() {
		return annee;
	}

	/**
	 * @param annee the annee to set
	 */
	public void setAnnee(int annee) {
		this.annee = annee;
	}

	/**
	 * @return the nomEnseignant
	 */
	public String getNomEnseignant() {
		return nomEnseignant;
	}

	/**
	 * @param nomEnseignant the nomEnseignant to set
	 */
	public void setNomEnseignant(String nomEnseignant) {
		this.nomEnseignant = nomEnseignant;
	}

}
