package prd.outilsAffectationProjet.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author MariamKonate
 *
 */
@Entity
@Table(name = "Groupe")
public class Groupe implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idGroupe")
	int idGroupe;
	/**
	 * @return the idGroupe
	 */
	public int getIdGroupe() {
		return idGroupe;
	}
	/**
	 * @param idGroupe the idGroupe to set
	 */
	public void setIdGroupe(int idGroupe) {
		this.idGroupe = idGroupe;
	}
	

}
