<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">		
	    <title>Formulaire de voeux </title>
	    <link rel="stylesheet" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css">
	    <script src="webjars/jquery/3.1.1/jquery.min.js"></script>
		<script src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
		<script type="text/javascript" src="webjars/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
		<link rel="stylesheet" href="webjars/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" type="text/css"/>
		<link rel="stylesheet" href="resources/style.css">
		<script src="resources/multiple-emails.js"></script>		
		<link type="text/css" rel="stylesheet" href="resources/multiple-emails.css" />
		 <script type="text/javascript">
				function JSalert(){
				var alertstr = "Opération réussie";
					    alert(alertstr);
				 
				}
				
				function JSalertFail(){
					 
					var alertstr = "Désolé(e) l'opération a échoué";
				    alert(alertstr);
					 
					}
			</script>
		
		
		<script type="text/javascript">
	
			$(function() {
				$('#groupeEmails').multiple_emails({position: "bottom"});
				
				$('#current_emailsBS').text($('#groupeEmails').val());
				$('#groupeEmails').change( function(){
					$('#groupeEmails').text($(this).val());
				});
			});
		
		
		</script>		   
	</head>
<body>
	<c:if test="${success}"><script> JSalert();</script></c:if>
	<c:if test="${failure}"><script> JSalertFail();</script></c:if>
	<!--       Barre de navigation         -->
			<nav class="navbar navbar-default">
				  <div class="container-fluid">			    
					    <ul class="nav navbar-nav">
						      <li><a href="index">ACCUEIL</a></li>
							      
							      <li class="active" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">PROJET<span class="caret"></span></a>
								        <ul id="drop" class="dropdown-menu" >
								          <li ><a href="authentificationEnseignant">ENSEIGNANT</a></li>
								          <li><a style="background-color:#00a99b !important"href="authentificationEtudiant">ETUDIANT</a></li>
								        </ul>
							      </li>
						      
						      <li><a href="authentificationAdministrateur">ADMINISTRATEUR</a></li>
					    </ul>
				  </div>
			</nav>
			<h2 style="text-align:center">Entrez les scores sans dépasser ${enseignement.nbMaxTotal} !!!</h2>
		  <div class="container" style="margin-top: 4%; margin-bottom:28%">
						<c:if test="${error}"> 
						<p style="color:red"> Une erreur s'est produite veuillez recommencer </p>
						</c:if>	            
					        <div class="row">
					        <div class="col-md-8 col-md-offset-2">   
							<form action="etudiantSouhait" method="post" >
							<div class='container'>
					<div class='row' style="margin-bottom:2%">
						<div class='form-group'>
							<div class='col-md-4 col-md-offset-2' style="margin-top:2%">
							<h4 for='groupeEmails' style="text-align:center">Adresses mails des membres du groupe</h4>
								<input type='text' id='groupeEmails' name='groupeEmails' class='form-control' >
								<input type="submit" name="enregistrer" value="enregistrer">
							</div>
						</div>
					</div>
				</div>
					
		            <table class="table table-list-search">
		                    <thead>
		                        <tr style="color:white; background-color:#00a99b">
		                            <th>Sujet</th>
		                            <th>Score</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    
		                    <c:forEach items="${sujets}" var="current" varStatus="loop">
										<c:if test="${loop.index mod 2 eq 0 }">
									        
										        <tr>
													
														<td> ${current.intitule} </td>
														
														<td> <select class="form-control" name="score-${loop.index}" id="score">
									                				<option selected="selected">0</option>
													        		
													        		<c:forEach var="i" begin="${1}" end="${enseignement.nbMaxScore}" varStatus="loop2">
														        		
																	   <option>${loop2.index}</option>
																</c:forEach>
													     	</select>	
								      					</td>
												</tr> 
											
									    </c:if>
									    <c:if test="${loop.index mod 2 ne 0 }">
									        <tr style="background-color:#e6fff9">
													<td> ${current.intitule} </td>
													<td> <select class="form-control" name="score-${loop.index}" id="score" >
									                				<option selected="selected">0</option>
													        		
													        		<c:forEach var="i" begin="${1}" end="${enseignement.nbMaxScore}" varStatus="loop2">
														        		
																	   <option>${loop2.index}</option>
																</c:forEach>
													     	</select>	
								      					</td>
													
											</tr>  
									    </c:if>
										
							</c:forEach>
							
		                    </tbody>
		                   
		                </table>
		                <div class="col-md-12 text-center"> 
						 <input type="submit" name="valider" value="valider" style="text-align: center"> 
						 <input type="hidden" name="nbSujets" value="${fn:length(sujets)}"> 
						 <input type="hidden" name="idEnseignement" value="${enseignement.idEnseignement}"> 
						
						</div>
						 
		                </form>
		                </div> 
				</div>
				</div>
	</body>
	<div class="row">
		<div class="col-md-12">            
			<footer>
				<img src="resources/footer.png" class="img-fluid" alt="Responsive image" style="width:100%; height:auto">		
			</footer>
		</div>
	</div>
</html>